;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(defpackage :krunch
  (:use #:common-lisp #+sbcl #:sb-mop)
  (:export #:*aggregate-length* #:*eos*
           #:aggregate-term-p
           #:ask #:answer
           #:capitalize #:compose
           #:exclaim #:expand-lf
           #:feature
           #:inconsistent-ψ-term
           #:missing-feature #:morphology
           #:normalize #:normalize-ψ-term
           #:parse-ψ-term #:preprocess-ψ-term
           #:punctuate
           #:realize #:rewrite
           #:say #:sentence
           #:with-ontology)
  (:documentation "KRUNCH implementation."))
