;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defclass concept (standard-class)
  ()
  (:documentation "A metaclass for TRIPS concepts."))

(defmethod validate-superclass ((class concept) (superclass standard-class))
  "Declare the concept metaclass to be compatible with standard classes."
  t)

(defmacro define-concept (name (&rest supers) &optional documentation)
  `(defclass ,name (,@supers)
     ()
     (:documentation ,documentation)
     (:metaclass concept)))

;; Undefined, non-ONT-named roots of the TRIPS concept hierarchy.
(define-concept abstr-obj () "Abstract objects.")
(define-concept phys-obj () "Physical objects.")
(define-concept situation () "Situations.")

(defun eval-concept (form)
  "Evaluate a TRIPS concept definition form."
  (ecase (car form)
    (provenance
     (when *load-verbose*
       (format t "; loading ~a concepts~%" (cadadr form))))
    (concept
     (destructuring-bind (name . options) (cdr form)
       (check-type name symbol "a valid concept name")
       (flet ((option (option) (cdr (find option options :key #'car))))
         (flet ((comment (&aux (comment (option 'comment)))
                  (etypecase comment
                    (null)
                    ((cons string null) (car comment))))
                (inherits (&aux (inherits (option 'inherit)))
                  (assert (every #'symbolp inherits) (inherits)
                          "Invalid inheritance list for concept ~a." name)
                  inherits))
           (ensure-class name
                         :direct-superclasses (inherits)
                         :documentation (comment)
                         :metaclass (find-class 'concept))))))))

(defun load-concepts (filename)
  "Read and evaluate TRIPS concept definitions from a file, and
automatically create missing packages encountered during reading."
  (with-open-file (file filename)
    (loop with *read-eval* = nil
          for position = 0 then (file-position file)
          for form = (loop
                       (handler-case (return (read file))
                         (end-of-file ()
                           (loop-finish))
                         (package-error (error)
                           (make-package (package-error-package error))
                           (unless (file-position file position)
                             (error error)))))
          when (with-simple-restart (continue "Skip this concept.")
                 (eval-concept form))
            collect it)))
