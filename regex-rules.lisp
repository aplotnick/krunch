;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Regular expression rewrite rules.

;; A regex rule, or rerule, rewrites strings. It must have exactly
;; one string as a LHS, and exactly one string as the RHS. The two
;; strings are used as the regex and replacement arguments to a call
;; to PPCRE:REGEX-REPLACE with the current string as target.

(defun parse-rerule (rerule)
  (destructuring-bind (lhs rhs) rerule
    (check-type lhs (or string function) "a regex")
    (check-type rhs string "a regex string")
    (parse-rule rerule 'regex-rewrite-rule)))

;; Emacs: (put 'define-rerule 'lisp-indent-function 0)
(defmacro define-rerule (regex replacement)
  `(define-rule ,(parse-rerule `(,regex ,replacement))))

(defclass regex-rewrite-rule (rewrite-rule)
  ((scanner :accessor lhs-scanner :initarg :scanner))
  (:default-initargs :scanner nil))

(defmethod shared-initialize :after ((instance regex-rewrite-rule) slot-names &key)
  "Cache the LHS regex scanner closure. We use a seperate slot from the LHS
so that instances print nicely."
  (setf (lhs-scanner instance) (ppcre:create-scanner (lhs instance))))

(defmethod apply-rule ((rule regex-rewrite-rule) (term string) bindings)
  (values (ppcre:regex-replace-all (lhs-scanner rule) term (rhs rule))
          bindings))
