;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH; Coding: utf-8 -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

;;; Utilities for Lisp data types.

(in-package :krunch)

;;; Conditions.

(defmacro ignore-error (typespec &body forms)
  "Execute FORMS ignoring errors that match TYPESPEC.
\(ignore-error nil . FORMS) ≡ (progn . FORMS)
\(ignore-error error . FORMS) ≡ (ignore-errors . FORMS)"
  `(handler-case (progn ,@forms)
     (,typespec (condition) (values nil condition))))

;;; Symbols.

(defun reintern (symbol package)
  "Intern the name of SYMBOL in PACKAGE and return the new symbol,
unless SYMBOL is uninterned."
  (if (and (symbolp symbol) (symbol-package symbol))
      (values (intern (symbol-name symbol) package))
      symbol))

;;; Conses.

(defun ensure-list (object)
  "Return the list designated by OBJECT."
  (if (listp object)
      object
      (list object)))

(defun designate-list (object)
  "Return a list designator for OBJECT."
  (if (and (listp object) (null (cdr object)))
      (car object)
      object))

(defun list-length* (list)
  "Return the length of LIST even if it is not a proper list.
Does not handle circular lists."
  (labels ((list-length* (list accumulator)
             (declare (fixnum accumulator) (optimize speed))
             (typecase list
               (null accumulator)
               (atom (the fixnum (1+ accumulator)))
               (cons (list-length* (cdr list) (1+ accumulator))))))
    (list-length* list 0)))

(defun recons (cons car cdr)
  "Create or reuse CONS of CAR and CDR."
  (if (and (eql car (car cons))
           (eql cdr (cdr cons)))
      cons
      (cons car cdr)))

(defun tail-cons (item list)
  "Destructively cons an item onto the end of a list."
  (if list
      (rplacd (last list) (cons item nil))
      (setq list (list item)))
  list)

(define-modify-macro appendf (&rest args) append "Append to list in a place.")
(define-modify-macro nconcf (&rest args) nconc "Destructively append to list.")
(define-modify-macro unionf (&rest args) union "Union with a list in a place.")

;; Property lists.

(defun reverse-plist (plist)
  (loop with reverse = '()
        for (tag value) on plist by #'cddr
        do (setq reverse (list* tag value reverse))
        finally (return reverse)))

(defun sans (plist &rest keys &aux sans)
  "Return a plist without the given keys.
By Erik Naggum."
  (loop
    (let ((tail (ignore-errors (nth-value 2 (get-properties plist keys)))))
      ;; this is how it ends
      (unless tail
        (return (nreconc sans plist)))
      ;; copy all the unmatched keys
      (loop until (eq plist tail)
            do (push (pop plist) sans)
               (push (pop plist) sans))
      ;; skip the matched key
      (setq plist (cddr plist)))))

(defun quote-every-other-one (list &optional (which :odd))
  "For hacking macros."
  (loop with quotep = (ecase which
                        (:even #'evenp)
                        (:odd #'oddp))
        initially (assert (evenp (length list)) (list) "Odd list length.")
        for item in list and index upfrom 0
        collect (if (funcall quotep index) `(quote ,item) item)))

;;; Sequences.

(defun choose (sequence &optional (random-state *random-state*))
  "Return a random element of the sequence."
  (elt sequence (random (length sequence) random-state)))

;;; Hash tables.

(defun copy-hash-table (hash-table &aux
                        (copy (make-hash-table
                               :test (hash-table-test hash-table)
                               :size (hash-table-size hash-table)
                               :rehash-size (hash-table-rehash-size hash-table)
                               :rehash-threshold (hash-table-rehash-threshold hash-table))))
  (maphash (lambda (key value) (setf (gethash key copy) value)) hash-table)
  copy)
