;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defgeneric name (object)
  (:documentation "Return the name of OBJECT.")
  (:method (object)
    "By default, objects name themselves."
    object)
  (:method ((object class))
    "Classes are named by symbols."
    (class-name object))
  (:method ((object package))
    "Packages are named by strings."
    (package-name object)))

(defgeneric constant-name (object)
  (:documentation "Return a constant that names OBJECT.")
  (:method (object &aux (name (name object)))
    (if (eq name object)
        object
        (constant-name name)))
  (:method ((object string))
    (values (intern (string-upcase object) "KEYWORD")))
  (:method ((object null)))
  (:method ((object symbol))
    (typecase object
      (keyword object)
      (t (values (intern (symbol-name object) "KEYWORD"))))))

(defclass named-object ()
  ((name :accessor name :initarg :name)))

(defun baptize (class name &rest args)
  "Make an individual and name it."
  (apply #'make-instance (or class 'named-object)
         :name name
         args))

(defmethod print-object ((object named-object) stream &aux
                         (type-name (ignore-errors (name (class-of object))))
                         (object-name (ignore-errors (name object))))
  (print-unreadable-object (object stream
                            :type (not type-name)
                            :identity (not object-name))
    (format stream "~@[~a~]~@[ ~a~]" type-name object-name)))
