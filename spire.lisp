;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

;;; KRUNCH interface to SPIRE.

(in-package :krunch)

;; KB::SET-FN, KB::LIST-FN, etc.
(unionf *aggregates* spire::*aggregate-predicates*)
