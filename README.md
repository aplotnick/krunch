# KRUNCH: a knowledge representation and natural language generation system.

This document provides a rough overview of the basic structure and operation
of KRUNCH, but the best overall demonstration of its capabilities is the
test suite in `test/krunch.lisp`. To load the system and run the test suite
in a Lisp REPL:
```
* (and (asdf:load-system :krunch)
       (in-package :krunch)
       (asdf:test-system :krunch))
Doing 195 pending tests of 195 tests total.
 IGNORE-ERROR REINTERN (REINTERN UNINTERNED) (ENSURE-LIST ATOM)
 (ENSURE-LIST NULL) (ENSURE-LIST CONS) (ENSURE-LIST LIST) LIST-LENGTH*
 RECONS TAIL-CONS (TAIL-CONS NIL) NCONCF REVERSE-PLIST SANS
 QUOTE-EVERY-OTHER-ONE CHOOSE COPY-HASH-TABLE GLB LUB IDENTITIES
 LISP-LATTICE FIND-CATEGORY (CATEGORY DOCUMENTATION) SUBCATP ITYPEP
 (ITYPEP SUBCAT) MAKE-INDIVIDUAL DELETE-INDIVIDUAL INDIVIDUAL-NAMED
 FIND-INDIVIDUAL (AÏT-KACI FIG1) (AÏT-KACI FIG4) (AÏT-KACI&AMIR FIG4)
 (AÏT-KACI&AMIR FIG5) (VON-BOMMEL&BECK FIG3) READ&PARSE-Ψ-TERM
 (HOOT FIG7) (NORMALIZE DUPLICATE FEATURES) TERM-SANS TERM-SANS-VALUES
 BIND* KEYS RULE-LESSP (REWRITE STATIC) (REWRITE VARIABLE)
 (REWRITE VARIABLE EQL) (REWRITE VARIABLE EQUAL) (REWRITE DOTTED)
 (REWRITE NESTED) (REWRITE CYCLE) (REWRITE PLIST)
 (REWRITE PLIST AND INDICATORS) (REWRITE PLIST DEFAULT)
 (REWRITE PLIST LOCATION) (REWRITE REGEX) (COMPOSE ⊤ ⊥)
 (COMPOSE CORULES) SUFFIXES COMBINE-WORDS (SAY AARDVARK) (SAY AARON)
 (SAY AARON AND PETER) (SAY CAT) (SAY QUICK BROWN FOX)
 (SAY BARBER SHAVES SELF) (SAY CAT LICKS SELF)
 (SAY CATS LICK THEMSELVES) (SAY PETER BOUGHT SELF BOOK)
 (SAY PETER BOUGHT BOOK ABOUT SIMON) (SAY PETER BOUGHT SIMON BOOK)
 (ASK WHY PETER BOUGHT SIMON BOOK) (SAY SIMON BOUGHT BOOK ABOUT SELF)
 (SAY SIMON WANTS BARBER TO SHAVE SELF)
 (SAY SIMON WANTS BARBER TO SHAVE HIM) (SAY SIMON LIKES HIS CAT)
 (SAY SIMON LIKES PETER’S CAT) (SAY I BOUGHT BOOK AND READ IT)
 (SAY DONKEY ANAPHORA) (SAY DRINK) (SAY DRINK MY MILK) (SAY I GO HOME)
 (SAY HE GOES HOME) (SAY WANT MILK) (SAY WANT TO BE HOME)
 (SAY WANT TO GO HOME) (SAY DRINK MILK) (SAY DRANK MILK)
 (SAY WILL DRINK MILK) (SAY HAVE DRUNK MILK) (SAY CAT IS ON MAT)
 (SAY CAT MAY BE ON MAT) (SAY FLUFFY CHASES MICE)
 (SAY FLUFFY IS CHASING MOUSE) (SAY FLUFFY CHASED MOUSE)
 (SAY FLUFFY ATE MOUSE) (ASK IF FLUFFY IS EATING)
 (ASK IF FLUFFY IS NOT EATING) (ASK WHAT FLUFFY MIGHT EAT)
 (ASK WHAT FLUFFY ATE) (ASK WHEN FLUFFY ATE)
 (ASK WHEN FLUFFY WILL BE EATING) (ASK WHICH MOUSE FLUFFY ATE)
 (SAY MOUSE WAS EATEN BY FLUFFY) (SAY THEY WERE IMPRESSED BY HIM)
 (SAY THEY WERE IMPRESSED BY HIS MANNER)
 (SAY HIS FRIENDS LAUGHED AT HIM) (SAY IT IS RAINING)
 (SAY LITTLE FLOWER GREW) (SAY BIG RED BLOCK) (SAY BLOCK ON TABLE)
 (SAY BIG RED BLOCK THAT IS ON TABLE) (SAY BLOCK NAME)
 (SAY BLOCK IS RED) (ASK IF BLOCK IS RED) (SAY YES THE BLOCK IS RED)
 (SAY NO THE BLOCK IS NOT RED) (ASK IF BLOCK IS EVER RED)
 (SAY NO THE BLOCK IS NEVER RED) (ASK WHAT NEVER) (SAY NO NEVER)
 (ASK IF THERE IS A BIG RED BLOCK) (SAY THERE IS A BIG RED BLOCK)
 (ASK IF THERE ARE ANY BIG RED BLOCKS)
 (SAY THERE ARE SOME BIG RED BLOCKS) (SAY PICK UP BIG RED BLOCK)
 (SAY NOW PUT IT DOWN) (ASK WHICH BLOCK) (ASK WHERE TO PUT IT)
 (ASK HOW TO PUT IT DOWN) (ASK PUT IT DOWN GENTLY)
 (SAY I WILL PUT IT DOWN GENTLY NOW)
 (SAY MOVE BLOCK FROM SHELF TO TABLE) (SAY ADD ANOTHER ONE)
 (SAY PUSH THEM TOGETHER) (SAY PUT RED BLOCK ON TABLE)
 (SAY PUT ANOTHER BLOCK NEXT TO IT)
 (SAY NOW ADD RED BLOCK NEXT TO THAT BLOCK)
 (SAY NOW PUT GREEN BLOCK ON FIRST BLOCK)
 (SAY PUT RED BLOCK ON BOTTOM MIDDLE GREEN BLOCK)
 (SAY MAKE ROW OF TWO GREEN BLOCKS) (SAY PUT RED BLOCK AT END)
 (SAY PUT ANOTHER AT END OF ROW ON LEFT) (SAY NOW PUT BLOCK ON IT)
 (SAY TOP BLOCK SHOULD BE RED) (SAY PUT ROW ON TABLE)
 (SAY PUT STACK ON TABLE) (SAY LET US BUILD THREE STEP STAIRCASE)
 (SAY NOT ENOUGH BLOCKS) (SAY MEK PHOSPHORYLATES ERK)
 (SAY ERK THAT MEK PHOSPHORYLATED)
 (SAY BRAF THAT IS PHOSPHORYLATED BY MEK)
 (SAY BRAF THAT IS PHOSPHORYLATED ON SERINE AND TYROSINE)
 (SAY AMOUNT OF PHOSPHORYLATED BRAF)
 (SAY CONCENTRATION BRAF-NRAS COMPLEX) (ASK IF COMPLEX IS SUSTAINED)
 (SAY EGFR BINDS EGF) (SAY TOTAL AMOUNT OF BRAF)
 (SAY ASSUME NO NRAS IN SYSTEM) (SAY DECREASE BINDING RATE)
 (SAY QUITE CERTAIN IT IS TRANSIENT IN TIME)
 (ASK WHAT GENES SRF REGULATES) (ASK WHICH PATHWAYS INVOLVE SRF)
 (SAY FIND DRUG THAT TARGETS KRAS) (SAY FIND DRUGS THAT TARGET KRAS)
 (SAY 88% OF PATIENTS) (SAY MUTATED KRAS CAN CAUSE LUNG CANCER)
 (SAY MUTATIONS IN BRAF) (SAY EFFECT OF RAF CAAX ON ASPP2)
 (SAY WANT TO FIND TREATMENT FOR PANCREATIC CANCER)
 (ASK WHAT DRUG I COULD USE)
 (ASK WHAT DRUG SHOULD I USE FOR PANCREATIC CANCER)
 (ASK WHAT PROTEINS MIGHT LEAD TO PANCREATIC CANCER)
 (ASK WHAT PROTEINS PLX-4720 TARGETS) (ASK WHAT ARE TARGETS OF PLX-4720)
 (ASK WHAT IS TARGET OF SELUMETINIB)
 (ASK WHAT PROTEINS VEMURAFENIB TARGETS)
 (ASK WHAT SELUMETINIB’S TARGETS ARE)
 (ASK IF SELUMETINIB INHIBITS MAP2K1) (ASK WHAT DRUGS INHIBIT MAP2K1)
 (ASK IF THERE ARE ANY DRUGS TARGETING KRAS)
 (ASK WHAT DRUG TARGETS BRAF) (ASK WHAT DRUGS TARGET BRAF)
 (ASK IF THERE ARE ANY DRUGS INHIBITING MEK)
 (ASK IF THERE ARE DRUGS THAT TREAT PANCREATIC CANCER)
 (ASK IF VEMURAFENIB INHIBITS BRAF)
 (ASK IF SELUMETINIB IS INHIBITOR OF MEK1)
 (ASK IF MEK2 IS INHIBITED BY SELUMETINIB)
 (ASK IF VEMURAFENIB IS AN INHIBITOR FOR BRAF)
 (ASK IF VEMURAFENIB TARGETS BRAF) (ASK IF THERE ARE ANY DRUGS FOR BRAF)
 (ASK IF YOU KNOW ANY DRUGS FOR BRAF) (ASK WHAT INHIBITS BRAF)
 (ASK IF THERE ARE ANY INHIBITORS FOR JAK1)
 (ASK WHAT ARE SOME JAK1 INHIBITORS)
 (ASK IF STAT3 IS ONE OF THE REGULATORS OF C-FOS)
No tests failed.
T
```

## Ontologies.

We assume that our knowledge about some domain or world may be captured
by typed, composable, descriptions. Types are arranged into an ontology,
which may in general be a DAG, but is more usually hierarchical. Base
types are called "categories", and our implementation of them is in
the file `categories.lisp`. The most important operation defined there
is the macro `defcat`, which is used in an ontology file to define
categories, e.g.,
```
(defcat attribute (individual))
(defcat color (attribute))
(defcat red (color))
```
defines three categories arranged in a trivial hierarchy.

An ontology is an object that stores a set of categories and related
information like rules, features, etc. See the file `ontologies.lisp`
for operations like `load-ontology` and `define-ontology`. Each ontology
is named by a symbol like `demo` or `blocks`. We maintain a notion of
"the current ontology", bound using the macro `with-ontology`.
For example, to use the `demo` ontology (see `ontology/demo.lisp`)
for a trivial demonstration of the generator, we might say
```(with-ontology demo (say '(aardvark)))```
to yield the string `"aardvark"`. In another ontology, `blocks`, say,
we would get an error that the `aardvark` category is not defined.

Instances of categories are called "individuals", and routines for
dealing with them are found in `individuals.lisp`. This functionality
is not usually needed, and may for the most part be ignored.

Specific ontologies are defined at the end of `ontologies.lisp`,
each of which loads its own file(s) from the `ontology` directory.
Ontologies are aggressively cached, but during development one often
wants to force a reload; to do so, use a form like this:
```(with-ontology clic (load-ontology 'clic :force t))```
Notice that the first `clic` (the ontology we want to reload here)
is unquoted, because `with-ontology` is a macro, but the second
occurrence is quoted, since it's an argument to the function `load-ontology`.

## ψ terms.

The basic knowledge representation structure in KRUNCH is the ψ term,
a kind of typed feature structure introduced by Hassan Aït-Kaci. The
implementation in `terms.lisp` is based on the "HOOT Language Specification"
(2014). Each ψ term has a type, represented by a category or class,
and zero or more features with type-restricted values. The ψ terms are
arranged in a lattice per `lattice.lisp`, which provides the basic
operations least upper bound (LUB) and greatest lower bound (GLB).
We take ψ terms to denote linguistic descriptions, and so LUB and GLB
become least common subsumer and most specific subsumee, respectively.

A simple example of a ψ term is `(name :first "John" :last "Doe")`,
which describes a name (a type that must exist in the current ontology)
with string-valued features `:first` and `:last`. A more complex example
```
!P : (person :id (name :first "John" :last !S : "Doe")
                       :address !A : location
                       :age 42
                       :spouse (person :id (name :first "Jane" :last !S)
                                       :address !A : location
                                       :spouse !P))
```
describes a person that might have that name, his address, and his spouse
Jane, who lives at the same address. The notation
```!X : (term ..)```
means to mark the term following the `:` with the label `!X`, which may
later be referred to as `!X`. It is analogous to the Lisp `#N=`/`#N#`
notation. One other piece of notation occasionally useful is that a
bare `ψ` stands for an anonymous type, normalized to `(ψ)`; it is
roughly analogous to `lambda` in Lisp.

The bulk of the logic in `terms.lisp` is devoted to the term normalization
algorithm described in the HOOT specification, which performs feature type
checks and normalizes types with respect to the current ontology. It also
defines the Lisp reader macros for the `!X` and `ψ` notations.

## Rewriting S-expressions.

To produce terms for the generator from other representations,
we use the rewrite engine defined in `rewrite.lisp`. It works
over S-expressions, not terms, and supports variable bindings,
plist handling, etc.

A rewrite rule is defined with `(define-rule lhs rhs)`, where
the left- and right-hand sides are S-expressions that may contain
variable bindings or references, marked with `?variable`.
Variables bound on the left-hand side may be used on the right:
e.g., `(define-rule (foo ?foo) (bar ?foo))` rewrites `(foo x)`
to `(bar x)`. A `*` matches any object, and keywords are matched
as such, not positionally. See `ontology/clic.lisp` for examples
of rules that convert interpretations of TRIPS logical forms into
the raw terms expected by the generator.

## Natural language generation.

The generator in `generator.lisp` takes a ψ term as input as produces
a sentence of english as a string. The ψ term must completely describe
both the syntax and semantics of the proposed utterance; see
`ontology/grammar.lisp` for linguistic features and rewrite rules.

The primary interface function is `say`, plus the wrappers `ask`, etc.
```
* (with-ontology demo (say '(be :subject fluffy :comp (eat :aspect progressive) :tense present)))
"Fluffy is eating"
* (with-ontology blocks (ask '(be :subject (the block) :comp (red :qualifier ever) :tense present)))
"Is the block ever red?"
```
See the test suite in `test/krunch.lisp` for many more examples.

Internally, the generator is based around the generic function `realize`,
which takes a morphology function designator (see `morphology.lisp`) and
a `thing` to realize. Simple methods on `realize` may be defined using
the `define-realization` macro. For complex terms, the generator works
in terms of "phrases", which are ψ terms taken to represent specific
kinds of linguistic phrases; see `define-phrase-type`. Phrases so defined
include `noun-phrase`, `aggregate-phrase`, `preposition-phrase`, `clause`,
`s-v-o`, etc. Phrase types are responsible for arranging their components
in a linear order and performing any associated lingusitic transformations,
e.g., pronominalization, elision, particle & dummy insertion, fronting, etc.
The phrase type for a ψ term is dynamically determined by the generic function
`phrase-type-p` (called by `compute-phrase-type`), methods for which are
generated by supplying the `:predicate` option to `define-phrase-type`.
This allows for extremely fine-grained and flexible phrase typing.
