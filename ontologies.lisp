;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Category & term lattice binding macros.

(defmacro with-categories ((&rest categories) &body body)
  "Execute BODY with a local category/term lattice.
CATEGORIES is a list of local category definitions
of the form (NAME SUPERS &REST SLOTS/OPTIONS).
Local categories are named by uninterned symbols,
but the original names are used for variables bound
to those categories and for local instantiation functions."
  (let ((local-names (loop with table = (make-hash-table)
                           for (name . rest) in categories
                           as local-name = (make-symbol (symbol-name name))
                           do (setf (gethash name table) local-name)
                           finally (return table))))
    (flet ((local-name (catname) (gethash catname local-names)))
      `(let* ((*categories* (copy-list *categories*))
              (*category-lattice* (copy-lattice *category-lattice*))
              (*term-lattice* (typecase *category-lattice*
                                (term-lattice *category-lattice*)
                                (t (make-instance 'term-lattice))))
              ,@(loop for (catname supers . slots/options) in categories
                      as catsym = (local-name catname)
                      as supersyms = (mapcar (lambda (super)
                                               (or (local-name super) super))
                                             supers)
                      collect `(,catname ; local variable bound to category
                                (defcat ,catsym ,supersyms ,@slots/options))))
         (declare (ignorable ,@(mapcar #'car categories))) ; local variables
         (unwind-protect
              (flet (,@(loop with common-lisp = (find-package "COMMON-LISP")
                             for (catname . nil) in categories
                             as catsym = (local-name catname)
                             as catcon = (intern (symbol-name catname) "KRUNCH")
                             unless (eq (symbol-package catcon) common-lisp)
                             collect `(,catcon (&rest initargs)
                                        ,(format nil "Make a new ~a." catname)
                                        (apply #'make-instance ',catsym
                                               initargs))))
                ,@body)
           ,@(loop for catsym being the hash-values of local-names
                   collect `(setf (find-class ',catsym) nil)))))))

(defmacro with-terms ((&key categories rules) &body body)
  `(with-categories (,@categories)
     (with-rewrite-rules (,@rules)
       ,@body)))

(defmacro with-ontology (ontology/options &body body)
  (destructuring-bind (&optional ontology &rest args) (ensure-list ontology/options)
   `(with-terms (,@args)
      (load-ontology ',ontology)
      (with-lattice-operators t
        ,@body))))

;;; Ontology definition & loading.

(defclass ontology ()
  ((categories :accessor ontology-categories :initarg :categories)
   (rules :accessor ontology-rewrite-rules :initarg :rules)
   (corules :accessor ontology-compose-rules :initarg :corules)
   (lattice :accessor ontology-lattice :initarg :lattice)
   (lattice-class :accessor ontology-lattice-class :initarg :lattice-class))
  (:default-initargs
   :categories nil :rules nil :corules nil :lattice nil :lattice-class nil)
  (:documentation "A related set of categories, rules, and features."))

(defgeneric ontology-files (ontology)
  (:method-combination append :most-specific-last)
  (:method append ((ontology ontology)))
  (:documentation "Construct a list of ontology files to load."))

(defmacro define-ontology (name (&rest supers) &rest options)
  (let* ((files (getf options :files))
         (options (sans options :files)))
    `(progn
       (defclass ,name (,@(or supers '(ontology)))
         ()
         (:default-initargs ,@options))
       (defmethod ontology-files append ((ontology ,name))
         ',(ensure-list files)))))

(defun make-ontology (name)
  (assert (subtypep name 'ontology) (name) "~a is not an ontology." name)
  (make-instance name))

(defparameter *ontologies* (make-hash-table))

(defun ontology (name)
  (check-type name symbol "an ontology name")
  (or (gethash name *ontologies*)
      (setf (ontology name) (make-ontology name))))

(defun (setf ontology) (ontology name)
  (check-type name symbol "an ontology name")
  (setf (gethash name *ontologies*) ontology))

(defgeneric load-ontology (ontology &key &allow-other-keys)
  (:documentation "Load a set of categories, terms, etc.")
  (:method ((ontology null) &key))
  (:method ((name symbol) &rest args)
    (apply #'load-ontology (ontology name) args))
  (:method ((ontology ontology) &key (load #'load) force
            (verbose *load-verbose*)
            (print *load-print*)
            (if-does-not-exist :error)
            (external-format :default))
    (if (and (not force)
             (ontology-categories ontology)
             (ontology-lattice ontology))
        (setq *rules* (copy-list (ontology-rewrite-rules ontology))
              *corules* (copy-list (ontology-compose-rules ontology))
              *categories* (copy-list (ontology-categories ontology))
              *term-lattice* (copy-lattice (ontology-lattice ontology))
              *category-lattice* *term-lattice*)
        (loop initially
                (setq *rules* nil *corules* nil)
                (assert (eq *term-lattice* *category-lattice*))
                (when (ontology-lattice-class ontology)
                  (change-class *term-lattice* (ontology-lattice-class ontology)))
              for file in (ontology-files ontology)
              do (funcall load file
                          :verbose verbose
                          :print print
                          :if-does-not-exist if-does-not-exist
                          :external-format external-format)
              finally
                (setf (ontology-rewrite-rules ontology) (copy-list *rules*)
                      (ontology-compose-rules ontology) (copy-list *corules*)
                      (ontology-categories ontology) (copy-list *categories*)
                      (ontology-lattice ontology) (copy-lattice *term-lattice*))))
    ontology))

;;; Ontologies.

(define-ontology events ()
  :files ("krunch:ontology;events.lisp"))

(define-ontology actors ()
  :files ("krunch:ontology;actors.lisp"))

(define-ontology grammar (actors events)
  :files ("krunch:ontology;grammar.lisp"))

(define-ontology animals (grammar)
  :files ("krunch:ontology;animals.lisp"))

(define-ontology people (grammar)
  :files ("krunch:ontology;people.lisp"))

(define-ontology demo (animals people)
  :files ("krunch:ontology;demo.lisp"))

(define-ontology blocks (grammar)
  :files ("krunch:ontology;blocks.lisp"))

(define-ontology biology (grammar)
  :files ("krunch:ontology;biology.lisp"))

(define-ontology trips (grammar)
  :lattice-class 'trips-term-lattice
  :files ("krunch:ontology;trips.lisp"))

(define-ontology clic (blocks biology trips)
  :lattice-class 'clic-term-lattice
  :files ("krunch:ontology;clic.lisp"))
