;;; -*- Mode: Lisp; Syntax: Common-Lisp; Package: KRUNCH; -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Hook into the Common Lisp class lattice.

(defclass lisp-lattice (lattice)
  ((representatives :accessor representatives
                    :initarg :representatives
                    :initform (make-hash-table))))

(defmethod copy-lattice ((lattice lisp-lattice) &rest initargs &key
                         (representatives
                          (copy-hash-table (representatives lattice))))
  (apply #'call-next-method lattice
         :representatives representatives
         initargs))

(defparameter *default-lisp-types*
  (remove-duplicates
   (append (class-precedence-list (find-class 'fixnum))
           (class-precedence-list (find-class 'bignum))
           (class-precedence-list (find-class 'single-float))
           (class-precedence-list (find-class 'double-float))
           (class-precedence-list (find-class 'symbol))
           (class-precedence-list (class-of "string")))))

(defmethod shared-initialize :after ((instance lisp-lattice) slot-names &key
                                     (lisp-types *default-lisp-types*))
  (declare (ignore slot-names))
  (dolist (type lisp-types (encode-all-types instance))
    (register-type type instance :encode-all-types nil)))

(defun lisp-instance-p (x lattice)
  (not (or (typep x 'class) (top x lattice) (bottom x lattice))))

(defmethod register-type :before (type (lattice lisp-lattice) &key)
  (when (lisp-instance-p type lattice)
    (pushnew type (gethash (class-of type) (representatives lattice)))))

(defmethod children ((x symbol) (lattice lisp-lattice) &aux
                     (class (find-class x nil)))
  (if class
      (children class lattice)
      (call-next-method)))

(defmethod children ((x class) (lattice lisp-lattice))
  (append (call-next-method) (gethash x (representatives lattice))))

(defmethod parents ((x symbol) (lattice lisp-lattice) &aux
                    (class (find-class x nil)))
  (if class
      (parents class lattice)
      (unless x (call-next-method))))

(defmethod parents :around (x (lattice lisp-lattice))
  (if (lisp-instance-p x lattice)
      (list (class-of x))
      (call-next-method)))
