;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH; -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defclass clic-lattice (lattice)
  ())

(defmethod children ((x symbol) (lattice clic-lattice)))
(defmethod parents ((x symbol) (lattice clic-lattice)))

(defclass clic-term-lattice (clic-lattice term-lattice)
  ())

(defparameter *raw-type-packages*
  '(nil :common-lisp :keyword :kb :ont :w))

(defmethod normalize-ψ-term-type ((raw-type symbol) (lattice clic-term-lattice))
  "Let concepts and words represent themselves."
  (if (member (symbol-package raw-type) *raw-type-packages* :key #'find-package)
      (register-type raw-type lattice)
      (call-next-method)))

(defmethod normalize-features ((term term) (lattice clic-term-lattice))
  "Do not normalize features in the CLiC lattice."
  term)
