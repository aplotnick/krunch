;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defpackage :eci
    (:use)
    (:export :eci :def-eci :def-role :def-instance :def-instances)
    (:documentation "Elementary Composable Ideas")))

(defvar *eci-package*
  (or (find-package :eci)
      (error "Can't find the ECI package."))
  "The ECI package.")

(defun export-eci (name)
  (when (eq (symbol-package name) *eci-package*)
    (export name *eci-package*))
  name)

(defclass eci:eci (standard-class)
  ((properties :initform nil :accessor eci-properties :initarg :properties)
   (attributes :initform nil :accessor eci-attributes :initarg :attributes)
   (states :initform nil :accessor eci-states :initarg :states)
   (args :initform nil :accessor eci-args :initarg :args)
   (const :initform nil :accessor eci-const :initarg :const)
   (components :initform nil :accessor eci-components :initarg :components)
   (constraints :initform nil :accessor eci-constraints :initarg :constraints)
   (habitats :initform nil :accessor eci-habitats :initarg :habitats)
   (affordances :initform nil :accessor eci-affordances :initarg :affordances))
  (:documentation "An Elementary Composable Idea (ECI)."))

(defmethod validate-superclass ((class eci:eci) (superclass standard-class))
  "Declare the ECI metaclass to be compatible with standard classes."
  t)

(defmacro eci:def-eci (name (&rest supers) &rest args &key
                       comment &allow-other-keys)
  "Define an ECI class."
  `(export-eci
    (defparameter ,name
      (defclass ,name (,@supers)
        () ; no slots, for now
        (:metaclass eci:eci)
        ,@(when comment `((:documentation ,comment)))
        ,@(loop for (key value) on (sans args :comment) by #'cddr
                collect `(,key ,value)))
      ,comment)))

(defmacro eci:def-role (name (&rest supers) &rest args)
  "Define an ECI role."
  `(eci:def-eci ,name ,(or supers '(eci::role)) ,@args))

(defmacro eci:def-instance (name &rest args)
  "Define an ECI instance."
  (check-type name symbol "an instance name")
  `(export-eci
    (defparameter ,name `(a ,',name ,,@(quote-every-other-one args)))))

(defmacro eci:def-instances (class instance-names)
  "Define a set of ECI instances."
  (check-type class symbol "an ECI name")
  (check-type instance-names list "a list of instance names")
  `(progn
     ,@(loop for name in instance-names
             collect `(eci:def-instance ,name :isa ,class))))

(defun load-ecis (filename &key (verbose *load-verbose*) (print *load-print*)
                  (if-does-not-exist :error) (external-format :default))
  "Read ECI definitions from a file and evaluate them.
Automatically creates missing packages encountered during reading."
  (with-open-file (file filename
                   :direction :input
                   :if-does-not-exist if-does-not-exist
                   :external-format external-format)
    (unless file (return-from load-ecis nil))
    (when verbose (format t "~&; loading ECIs from ~s~%" filename))
    (flet ((maybe-print (object)
             (when print (format t "~&; ~s~%" object))
             object))
      (loop with *package* = *eci-package*
            with *read-eval* = nil
            for position = 0 then (file-position file)
            for form = (loop
                         (handler-case (return (read file))
                           (end-of-file ()
                             (loop-finish))
                           (package-error (error)
                             (make-package (package-error-package error))
                             (unless (file-position file position)
                               (error error)))))
            do (with-simple-restart (continue "Skip this form.")
                 (maybe-print (eval form)))
            finally (return t)))))
