;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Distinguished words & relations.

(defparameter *prepositions*
  '(:about :at :by :for :from :in :like :of :on :to :with)
  "Prepositions.")

(defmethod parse-ψ-term :around ((term cons) &rest args)
  "Syntactic sugar for linguistic relations: e.g., given
\(DEFCAT DETERMINER (LINGUISTIC)) & (DEFCAT THE (DETERMINER)),
\(THE FOO) parses as (FOO :DETERMINER THE)."
  (let* ((head (car term))
         (head-cat (and (symbolp head)
                        (eq head (find-symbol (symbol-name head) :krunch))
                        (find-category head nil))))
    (if (and head-cat
             (ignore-error category-not-found (subcatp head-cat 'linguistic))
             (cadr term) (not (keywordp (cadr term))))
        (apply #'parse-ψ-term
               (rewrite `(,@(ensure-list (cadr term))
                          ,(feature-name head-cat) ,head
                          ,@(cddr term)))
               args)
        (call-next-method))))

;;; Grammatical features.

(defparameter *grammatical-features* *prepositions*
  "Features that convey grammatical information.")

(defmacro define-grammatical-feature (feature (phrase &rest args)
                                      &rest methods/options)
  "Define a generic function that returns a grammatical feature of PHRASE."
  (multiple-value-bind (methods options)
      (loop for method/option in methods/options
            if (typep method/option '(cons (eql :method)))
              collect method/option into methods
            else
              collect method/option into options
            finally (return (values methods options)))
    (flet ((method (specializer &rest body)
             (unless (ignore-error type-error
                       (find (ensure-list specializer) methods
                             :key #'cdaadr
                             :test #'equal))
               (if args
                   `((:method ((,phrase ,specializer) &rest args)
                       (declare (ignorable args))
                       ,@body))
                   `((:method ((,phrase ,specializer))
                       ,@body))))))
      (let ((feature-of (intern (format nil "~A-OF" feature)))
            (feature-name (or (cadr (assoc :feature-name options))
                              (feature-name feature))))
        `(progn
           (pushnew ,feature-name *grammatical-features*)
           (defgeneric ,feature-of (,phrase ,@args)
             ,@(method t
                 (if (cadr (assoc :type-name options))
                     `(constant-type-name (feature ,feature-name ,phrase))
                     `(feature ,feature-name ,phrase)))
             ,@methods))))))

(define-grammatical-feature gender (phrase)
  (:type-name t)
  (:method :around (phrase)
    (let ((gender (call-next-method)))
      (case gender
        ((nil) :neuter)
        (:male :masculine)
        (:female :feminine)
        (otherwise gender))))
  (:method ((phrase pronoun))
    (pronoun-gender phrase)))

(define-grammatical-feature number (phrase)
  (:type-name t)
  (:method :around (phrase)
    (or (call-next-method)
        (let ((subject (subject-of phrase)))
          (unless (and subject (eq subject phrase))
            (number-of subject)))))
  (:method ((phrase null))
    :singular)
  (:method ((phrase pronoun))
    (pronoun-number phrase))
  (:method ((phrase number))
    (case phrase
      (1 :singular)
      (t :plural))))

(define-grammatical-feature person (phrase)
  (:type-name t)
  (:method :around (phrase)
    (or (call-next-method) :third))
  (:method ((phrase pronoun))
    (pronoun-person phrase)))

(define-grammatical-feature modal (phrase &key)
  (:type-name t)
  (:method :around (phrase &key tense)
    (or (call-next-method)
        (case tense
          (:future :will)))))

(define-grammatical-feature determiner (phrase))
(define-grammatical-feature name (phrase))
(define-grammatical-feature origin (phrase) (:feature-name :of))
(define-grammatical-feature time (phrase))
(define-grammatical-feature location (phrase))
(define-grammatical-feature purpose (phrase))
(define-grammatical-feature manner (phrase))
(define-grammatical-feature mood (phrase) (:type-name t))
(define-grammatical-feature negation (phrase))
(define-grammatical-feature tense (phrase) (:type-name t))
(define-grammatical-feature aspect (phrase) (:type-name t))
(define-grammatical-feature voice (phrase) (:type-name t))

;;; Anaphora & pronominalization.

(defgeneric realized-in (context)
  (:documentation "Return a list of things realized in a context.")
  (:method ((context null))))

(defgeneric phrase-structure (context)
  (:documentation "Return the current phrase structure of a context.")
  (:method ((context null))))

(defclass realization-context ()
  ((realized :accessor realized-in :initarg :realized)
   (structure :accessor phrase-structure :initarg :structure))
  (:default-initargs :realized nil :structure nil))

(defun make-context (&rest initargs)
  (apply #'make-instance 'realization-context initargs))

(defgeneric augment-context (context thing)
  (:method ((context null) (thing null)))
  (:method ((context null) thing)
    (make-context :structure (list thing)))
  (:method ((context realization-context) thing)
    (make-context :realized (realized-in context)
                  :structure (cons thing (phrase-structure context)))))

(defmacro with-augmented-context ((context thing) &body body &aux
                                  (prev-context (make-symbol "PREV-CONTEXT")))
  `(let* ((,prev-context ,context)
          (,context (augment-context ,prev-context ,thing)))
     (unwind-protect (progn ,@body)
       (when (and ,prev-context (realized-in ,context))
         (setf (realized-in ,prev-context)
               (union (realized-in ,prev-context)
                      (realized-in ,context)))))))

(defgeneric find-in-context (thing context &key key test test-not)
  (:method ((thing null) context &key &allow-other-keys)
    (declare (ignore context)))
  (:method (thing (context null) &key &allow-other-keys)
    (declare (ignore thing)))
  (:method ((thing tag) (context realization-context) &rest args)
    (apply #'find thing (realized-in context) args))
  (:method ((thing term) (context realization-context) &rest args)
    (apply #'find-in-context (term-tag thing) context args)))

(defun pronominalize (phrase &key case context subject object
                      (number (number-of phrase))
                      (gender (gender-of phrase)))
  (or (and (null (subterms phrase))
           (pronoun (realize nil (head phrase))
                    :case case
                    :number number))
      (and subject object (eq subject object)
           (personal-pronoun :case :reflexive
                             :number number
                             :gender gender))
      (and (or subject object)
           context (find-in-context phrase context)
           (personal-pronoun :case case
                             :number number
                             :gender gender))
      (and context (car (phrase-structure context))
           (multiple-value-bind (prep prep-object)
               (get-features (car (phrase-structure context)) *prepositions*)
             (declare (ignore prep))
             (and (eq phrase prep-object)
                  (personal-pronoun :case case
                                    :number number
                                    :gender (gender-of prep-object)))))))

(defun maybe-pronominalize (phrase &rest args &key
                            (pronominalize t) &allow-other-keys)
  (or (and phrase pronominalize (apply #'pronominalize phrase args))
      phrase))

(defmacro define-pronominalized-feature (feature)
  (let ((feature-name (feature-name feature)))
    `(define-grammatical-feature ,feature (phrase &key context dummy pronominalize)
       (:method ((phrase term) &rest args)
         (apply #'maybe-pronominalize (feature ,feature-name phrase) args)))))

(define-pronominalized-feature subject)
(define-pronominalized-feature object)
(define-pronominalized-feature indirect-object)
(define-pronominalized-feature possessive)
(define-pronominalized-feature comp) ; predicative complement

;;; Public interface.

(defparameter *rewrite* t
  "Default for the :REWRITE keyword argument that SAY passes to REALIZE.
Defaults to T, but bound to NIL, so we rewrite only top-level forms.")

(defun say (thing &rest args &key
            (context (make-context))
            (rewrite *rewrite*) &allow-other-keys &aux
            (*rewrite* nil))
  "Say something."
  (apply #'realize t thing :context context :rewrite rewrite args))

(defun ask (thing &rest args &key (mood 'question) (mark #\?))
  "Ask something."
  (apply #'sentence thing :mood mood :mark mark args))

(defun exclaim (thing &rest args &key (mark #\!))
  "Exclaim something!"
  (apply #'sentence thing :mark mark args))

(defun sentence (thing &rest args &key (mark #\.) &allow-other-keys)
  "Utter a capitalized, punctuated sentence."
  (capitalize (punctuate (apply #'say thing args) mark)))

;;; Realizations.

(defgeneric realize (morphology thing &rest args)
  (:argument-precedence-order thing morphology)
  (:method (morphology (thing null) &key))
  (:method (morphology (thing (eql ⊥)) &key))
  (:method ((morphology (eql t)) thing &rest args)
    (apply #'realize nil (apply #'phrase t thing args) args))
  (:method ((morphology null) thing &rest args)
    (apply #'realize (lambda (realization &rest args)
                       (declare (ignore args))
                       realization)
           thing args))
  (:method :around ((morphology function) thing &rest args
                    &key mark &allow-other-keys)
    (let ((realization (call-next-method)))
      (etypecase realization
        (null)
        (string (apply morphology realization args))
        (atom (apply #'realize morphology realization args))
        ((cons keyword) realization)
        (cons
         (punctuate
          (format nil "~{~a~^ ~}"
                  (combine-words
                   (loop for word in realization
                         when word
                         collect (apply #'realize morphology word args))))
          mark)))))
  (:method :after (morphology (thing tag) &key context)
    (when context
      (pushnew thing (realized-in context)))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun ensure-&key (lambda-list)
    "Ensure that LAMBDA-LIST specifies either &REST or &KEY."
    (cond ((intersection lambda-list '(&rest &key)) lambda-list)
          ((find '&aux lambda-list)
           (let ((aux (position '&aux lambda-list)))
             `(,@(subseq lambda-list 0 aux) &key ,@(subseq lambda-list aux))))
          (t (append lambda-list '(&key))))))

(defmacro define-realization (args &body body)
  `(defmethod realize ((morphology function) ,@(ensure-&key args))
     ,@body))

(define-realization ((thing string)) thing)
(define-realization ((thing (eql t))) "ok")
(define-realization ((thing symbol))
  (string-downcase (substitute #\Space #\- (symbol-name thing))))

(define-realization ((thing class)) (class-name thing))
(define-realization ((thing number)) (format nil "~r" thing))
(define-realization ((thing tag)) (tag-term thing))
(define-realization ((thing term) &rest args)
  (if (subterms thing)
      (apply #'realize t thing args)
      (head thing)))

(define-realization ((thing category))
  (if (itypep thing 'individual)
      (make-individual thing)
      (name thing)))

(define-realization ((thing (eql cat:individual)))
  "For small (verbless) clauses."
  nil)

(define-realization ((thing individual))
  "Realize ordinary individuals by (class) name."
  (or (ignore-error unbound-slot (name thing))
      (class-name (class-of thing))))

(define-realization ((pronoun pronoun) &key case number)
  (pronoun pronoun :case case :number number))

;;; Phrases.

(defclass phrase (term) ())

(defun make-phrase (class &rest initargs)
  (assert (subtypep class 'phrase) (class) "Not a phrase class.")
  (apply #'make-instance class initargs))

(defgeneric phrase (class thing &key &allow-other-keys)
  (:method (class thing &key) thing)
  (:method (class (thing null) &key))
  (:method (class (thing cons) &rest args &key
            (rewrite t) (tag-cycles t) (normalize t) &aux
            (thing (if rewrite (rewrite thing) thing))
            (thing (if tag-cycles (tag-cycles thing) thing))
            (thing (if normalize (normalize thing) thing)))
    (apply #'phrase class thing args))
  (:method (class (tag tag) &rest args)
    (apply #'phrase class (tag-term tag) args))
  (:method (class (term term) &rest args &aux
            (class (etypecase class
                     ((eql t) (apply #'compute-phrase-type term args))
                     ((or symbol class) class))))
    (change-class term class))
  (:method (class (term phrase) &key)
    (assert (typep term class) (class term) "Wrong phrase class.")
    term))

(defgeneric head (term)
  (:method ((term list))
    (car term))
  (:method (term)
    (or (name-of term)
        (term-type term))))

(defun adjuncts (phrase &rest args)
  "Realize prepositional adjuncts to a phrase."
  (loop for prep in *prepositions*
        as object = (feature prep phrase)
        when object
          collect (realize #'prep prep)
          and collect (apply #'say object
                             :case :objective
                             :object object
                             args)))

;; Specialized phrases.

(defvar *phrase-types* '()
  "A list of specialized phrase type names. Each name should have
a corresponding EQL-specialized method on PHRASE-TYPE-P.")

(defgeneric phrase-type-p (thing phrase-type &key context &allow-other-keys)
  (:documentation "Determine if THING should be realized as PHRASE-TYPE."))

(defgeneric compute-phrase-type (thing &key &allow-other-keys)
  (:documentation "Select a phrase type based on the head and context.")
  (:method ((thing phrase) &key) (class-of thing))
  (:method ((thing tag) &rest args)
    (apply #'compute-phrase-type (tag-term thing) args))
  (:method ((thing term) &rest args)
    (loop for phrase-type in *phrase-types*
          when (apply #'phrase-type-p thing phrase-type args)
            return phrase-type)))

(defgeneric auxiliary (phrase &key &allow-other-keys)
  (:documentation "Choose auxiliaries for a verb phrase.")
  (:method-combination append :most-specific-last))

(defmacro define-phrase-type (phrase-type (&rest supers) &rest options)
  "Define a phrase class, optionally with a global variable
and some specialized methods."
  (let ((auxiliary (cdr (assoc :auxiliary options)))
        (documentation (cadr (assoc :documentation options)))
        (number (cdr (assoc :number options)))
        (object (cdr (assoc :object options)))
        (parameter (cdr (assoc :parameter options)))
        (predicate (cdr (assoc :predicate options)))
        (realization (cdr (assoc :realization options)))
        (subject (cdr (assoc :subject options))))
    `(progn
       (defclass ,phrase-type (,@supers)
         () ; no slots, for now
         ,@(when documentation `((:documentation ,documentation))))
       (pushnew ',phrase-type *phrase-types*)
       ,@(when auxiliary `((defmethod auxiliary ,@auxiliary)))
       ,@(when number `((defmethod number-of ,@number)))
       ,@(when object `((defmethod object-of ,@object)))
       ,@(when subject `((defmethod subject-of ,@subject)))
       ,@(when parameter `((defparameter ,@parameter)))
       ,@(when realization `((define-realization ,@realization)))
       ,@(when predicate
           (destructuring-bind ((thing-arg phrase-type-arg . args) . body)
               predicate
             `((defmethod phrase-type-p (,thing-arg
                                         (,phrase-type-arg (eql ',phrase-type))
                                         ,@(ensure-&key args))
                 ,@body)))))))

;; Noun phrases.

(defun adjectives (phrase &rest args)
  "Realize adjectival premodifiers to a noun phrase. Note that
non-grammatical features are interpreted adjectivially by default."
  (loop for (feature modifier) on (subterms phrase) by #'cddr
        unless (find feature *grammatical-features*)
          collect (apply #'realize #'adjective
                         (term-sans-values modifier (name phrase))
                         :gerundive (and (not (tense-of modifier))
                                         (itypep modifier 'event))
                         args)))

(define-phrase-type noun-phrase (phrase)
  (:documentation "A phrase with a noun as its head.")
  (:parameter *noun-features*
    '(:determiner :possessive)
    "Features that convey grammatical information in a noun phrase.")
  (:predicate (thing phrase-type)
    "The default phrase type."
    t)
  (:realization ((phrase noun-phrase) &key context subject object
                 (case :nominative)
                 (number (number-of phrase)))
    (or (pronominalize phrase
                       :subject subject
                       :object object
                       :context context
                       :case case
                       :number number)
        (with-augmented-context (context phrase)
          `(,(say (negation-of phrase))
            ,(realize #'determiner (determiner-of phrase)
                      :case case
                      :number number)
            ,(say (possessive-of phrase)
                  :subject subject
                  :object object
                  :case :genitive
                  :context context)
            ,@(adjectives phrase)
            ,(realize #'noun (head phrase)
                      :case case
                      :number number)
            ,@(when (origin-of phrase)
                `("of" ,(say (origin-of phrase) :context context)))
            ,(say (location-of phrase) :context context)
            ,(say (time-of phrase) :context context)
            ,(say (purpose-of phrase) :context context)
            ,(say (manner-of phrase) :context context)
            ,@(adjuncts (term-sans phrase :of)
                        :subject subject
                        :object object
                        :context context))))))

(defvar *aggregate-length* *print-length*
  "Controls how many elements of an aggregate phrase are realized.")

(define-phrase-type aggregate-phrase (aggregate-term noun-phrase)
  (:documentation "An aggregate defined by its members.")
  (:predicate (thing phrase-type)
    (aggregate-term-p thing))
  (:number ((phrase aggregate-term))
    (case (length (members phrase))
      (1 :singular)
      (t :plural)))
  (:realization ((phrase aggregate-phrase) &key
                 (ellipsis :et-al.) ; "and N others"
                 (length *aggregate-length*) &aux
                 (n (length (members phrase))))
    (apply #'format nil
           "~#[none~;~A~;~A and ~A~:;~@{~#[~;and ~]~A~^, ~}~]"
           (mapcar #'say
                   (if (and length (> n length))
                       (tail-cons
                        (case ellipsis
                          (:et-al. (format nil "~D others" (- n length)))
                          (otherwise ellipsis))
                        (subseq (members phrase) 0 length))
                       (members phrase))))))

(defvar *href-target* "_blank"
  "Default target for hyperlinks.")

(define-phrase-type hyperlink (noun-phrase)
  (:documentation "A hyperlinked phrase.")
  (:predicate (thing phrase-type)
    (feature :href thing))
  (:realization ((phrase hyperlink) &key (target *href-target*))
    (format nil "<a href=\"~A\"~@[ target=\"~A\"~]>~A</a>"
            (say (feature :href phrase)) target
            (say (term-sans phrase :href)))))

;; Preposition phrases.

(define-phrase-type preposition-phrase (phrase)
  (:documentation "A preposition with an optional complement.")
  (:predicate (thing phrase-type)
    (or (find (name (head thing)) *prepositions* :test #'string-equal)
        (itypep thing 'relative-location)
        (itypep thing 'relative-time)))
  (:realization ((phrase preposition-phrase) &rest args &key context
                 (object (object-of phrase)))
    (with-augmented-context (context phrase)
      `(,(say (negation-of phrase))
        ,(realize #'prep (head phrase))
        ,(apply #'say object :case :objective :object object args)
        ,@(adjuncts phrase :object object :context context)))))

;; Clauses.

(defparameter *adverbial-features*
  '(:qualifier)
  "Features whose values should be realized adverbially.")

(defun adverbs (phrase &rest args)
  "Realize adverbial premodifiers to a verb phrase."
  (loop for (feature modifier) on (subterms phrase) by #'cddr
        when (find feature *adverbial-features*)
          collect (apply #'realize #'adverb modifier args)))

(defparameter *verb-features*
  '(:subject :tense :modal :aspect :voice)
  "Features that convey grammatical information in a verb phrase.")

(defun verb-phrase-p (phrase)
  "Return true if PHRASE is headed by a verb."
  (and (not (get-features phrase *noun-features*))
       (or (get-features phrase *verb-features*)
           (itypep phrase 'event))))

(defun impersonalp (phrase)
  "Return true if PHRASE is an impersonal verb phrase."
  (and (verb-phrase-p phrase)
       (not (subject-of phrase))))

(defun make-dummy-subject (comp)
  "Make a dummy subject for an intransitive verb from its complement."
  (when comp
    (make-pronoun :case `(:nominative ,(if (impersonalp comp) "it" "there"))
                  :number (number-of comp))))

(defmacro one-use (variable)
  "Return the value of VARIABLE and set it to NIL."
  (check-type variable symbol "a variable")
  `(prog1 ,variable (setq ,variable nil)))

(define-phrase-type clause (phrase)
  (:documentation "A phrase with a subject & predicate.
This phrase type is never instantiated directly. But its
subtypes all call down to its REALIZE method and reorder
or augment the plist that it returns.")
  (:predicate (thing phrase-type))
  (:auxiliary append ((phrase clause) &key aspect context)
    (ecase aspect
      ((:neutral nil))
      (:perfect '("have"))
      (:progressive
       (and (not (itypep (car (phrase-structure context)) 'be))
            '("be")))
      (:perfect+progressive '("have" "been"))))
  (:subject ((phrase clause) &key context pronominalize
             (dummy (typecase (car (phrase-structure context))
                      (relative-clause nil)
                      (otherwise t))))
    (declare (ignore pronominalize))
    (or (call-next-method)
        (and dummy (make-dummy-subject (comp-of phrase)))))
  (:realization ((phrase clause) &key context number person &aux
                 (tense (or (tense-of phrase)
                            (typecase (cadr (phrase-structure context))
                              (relative-clause :past))))
                 (aspect (aspect-of phrase))
                 (voice (voice-of phrase))
                 (modal (modal-of phrase :tense tense))
                 (negation (negation-of phrase))
                 (head (head phrase))
                 (subject (subject-of phrase :context context))
                 (number (or number (number-of subject)))
                 (person (or person (person-of subject)))
                 (object (object-of phrase :context context))
                 (indirect-object (indirect-object-of phrase :context context))
                 (comp (comp-of phrase :context context))
                 (time (time-of phrase))
                 (location (location-of phrase))
                 (purpose (purpose-of phrase))
                 (manner (manner-of phrase))
                 (auxiliary (or (auxiliary phrase
                                           :tense tense
                                           :aspect aspect
                                           :voice voice
                                           :modal modal
                                           :negation negation
                                           :context context)
                                (and modal ; TRIPS parses do as modality
                                     (string-equal modal "do")
                                     (list (one-use modal))))))
    (with-visited-table (dejavu)
      (unless (dejavu phrase)
        (setf (dejavu phrase) t)
        (with-augmented-context (context phrase)
          `(:auxiliaries ,(when auxiliary
                            `(,@(mapcar (lambda (aux)
                                          (realize #'verb aux
                                                   :tense (one-use tense)
                                                   :number number
                                                   :person person))
                                        auxiliary)
                                ,(one-use negation)))
                         :modals ,(when modal
                                    `(,(realize #'verb modal)
                                       ,(one-use negation)))
                         :negation ,negation
                         :adverbs ,(adverbs phrase)
                         :verb ,(realize #'verb head
                                         :number number
                                         :person person
                                         :tense tense
                                         :aspect aspect
                                         :voice voice)
                         :subject ,(say subject
                                        :case :nominative
                                        :context context)
                         :object ,(say object
                                       :case :objective
                                       :subject subject
                                       :object object
                                       :context context)
                         :comp ,(say comp
                                     :subject subject
                                     :object object
                                     :context context)
                         :indirect-object ,(say indirect-object
                                                :case :objective
                                                :subject subject
                                                :object indirect-object
                                                :context context)
                         :time ,(say time :context context)
                         :location ,(say location :context context)
                         :purpose ,(say purpose :context context)
                         :manner ,(realize #'adverb manner :context context)
                         :adjuncts ,(adjuncts phrase
                                              :subject subject
                                              :object object
                                              :context context)))))))

(define-phrase-type s-v-o (clause)
  (:documentation "The prototypical word order in English.")
  (:predicate (thing phrase-type)
    (verb-phrase-p thing))
  (:auxiliary :around ((phrase s-v-o) &key modal negation)
    (or (call-next-method)
        (and negation (not modal) '("do"))))
  (:realization ((phrase s-v-o))
    (destructuring-bind (&key subject verb object comp
                         indirect-object time location purpose manner
                         auxiliaries modals negation adverbs adjuncts)
        (call-next-method)
      `(,subject ,@modals ,@auxiliaries ,negation
        ,@adverbs ,verb ,indirect-object ,object ,comp
        ,location ,manner ,time ,purpose ,@adjuncts))))

(define-phrase-type s-be-comp (clause)
  (:documentation "Copular phrases.")
  (:predicate (thing phrase-type)
    "To be, or not to be, that is the question."
    (itypep thing 'be))
  (:realization ((phrase s-be-comp) &key context)
    (destructuring-bind (&key subject verb object comp
                         indirect-object time location purpose manner
                         auxiliaries modals negation adverbs adjuncts)
        (call-next-method)
      (declare (ignore object indirect-object))
      (when (typep (car (phrase-structure context)) 'question)
        (rotatef subject verb))
      `(,time ,subject ,@modals ,@auxiliaries
        ,@adverbs ,verb ,negation ,comp
        ,location ,manner ,time ,purpose ,@adjuncts))))

(define-phrase-type s-to-v-o (clause)
  (:documentation "A phrase subordinate to a control verb.")
  (:predicate (thing phrase-type &key context)
    (and (phrase-type-p thing 's-v-o :context context)
         (itypep (car (phrase-structure context)) 'control)))
  (:subject ((phrase s-to-v-o) &key context dummy pronominalize)
    "Drop the subject if it's the same as the one upstairs."
    (declare (ignore dummy pronominalize))
    (let ((subject (call-next-method)))
      (and (not (eq subject (subject-of (car (phrase-structure context)))))
           subject)))
  (:realization ((phrase s-to-v-o))
    "Insert the particle \"to\" after the subject."
    (destructuring-bind (&key subject verb object comp
                         indirect-object time location purpose manner
                         auxiliaries modals negation adverbs adjuncts)
        (call-next-method)
      `(,subject "to" ,@modals ,@auxiliaries ,negation
        ,@adverbs ,verb ,indirect-object ,object ,comp
        ,location ,manner ,time ,purpose ,@adjuncts))))

(defun interrogative-p (phrase)
  (or (itypep phrase 'interrogative)
      (and (determiner-of phrase)
           (interrogative-p (determiner-of phrase)))))

(defun interrogative-subject-p (phrase)
  (interrogative-p (subject-of phrase :pronominalize nil)))

(defun interrogative-object-p (phrase)
  (interrogative-p (object-of phrase :pronominalize nil)))

(defun interrogative-purpose-p (phrase)
  (interrogative-p (purpose-of phrase)))

(defun interrogative-manner-p (phrase)
  (interrogative-p (manner-of phrase)))

(defun interrogative-location-p (phrase)
  (interrogative-p (location-of phrase)))

(defun interrogative-time-p (phrase)
  (interrogative-p (time-of phrase)))

(define-phrase-type question (clause)
  (:documentation "Questions?")
  (:predicate (thing phrase-type &key (mood (mood-of thing)))
    "Is this a question?"
    (case (constant-type-name mood)
      (:question t)
      (otherwise (or (interrogative-subject-p thing)  ; who
                     (interrogative-object-p thing)   ; what, which
                     (interrogative-purpose-p thing)  ; why
                     (interrogative-manner-p thing)   ; how
                     (interrogative-location-p thing) ; where
                     (interrogative-time-p thing))))) ; when
  (:auxiliary :around ((phrase question) &key modal)
    "Does it need do-support?"
    (or (call-next-method)
        (and (not modal)
             (not (itypep phrase 'be))
             (subject-of phrase)
             (not (itypep (subject-of phrase) 'be))
             (not (interrogative-subject-p phrase))
             '("do"))))
  (:realization ((phrase question))
    "Front any interrogative and auxiliary."
    (destructuring-bind (&key subject verb object comp
                         indirect-object time location purpose manner
                         auxiliaries modals negation adverbs adjuncts)
        (call-next-method)
      (let ((interrogative
             (cond ((interrogative-subject-p phrase) (one-use subject))
                   ((interrogative-object-p phrase) (one-use object))
                   ((interrogative-purpose-p phrase) (one-use purpose))
                   ((interrogative-manner-p phrase) (one-use manner))
                   ((interrogative-location-p phrase) (one-use location))
                   ((interrogative-time-p phrase) (one-use time))))
            (front (or (one-use modals)
                       (one-use auxiliaries)
                       (and (itypep phrase 'be)
                            (list (one-use verb))))))
        `(,interrogative
          ,@front ,negation ,subject ,@modals ,@auxiliaries
          ,@adverbs ,verb ,indirect-object ,object ,comp
          ,location ,manner ,time ,purpose ,@adjuncts)))))

;; Compound clauses.

(define-phrase-type answer (phrase)
  (:documentation "Answers to questions.")
  (:parameter *responses* '(:response))
  (:predicate (thing phrase-type)
    (get-features thing *responses*))
  (:realization ((phrase answer) &rest args &key context)
    (with-augmented-context (context phrase)
      (multiple-value-bind (feature response) (get-features phrase *responses*)
        `(,(format nil "~A," (apply #'say response :context context args))
          ,(apply #'say (term-sans phrase feature) :context context args))))))

(define-phrase-type conjunction (phrase)
  (:documentation "One phrase and another, joined together.")
  (:parameter *conjunctions* '(#\- #\/ :and :but :nor :or :so :yet))
  (:predicate (thing phrase-type)
    (get-features thing *conjunctions*))
  (:realization ((phrase conjunction) &rest args &key context)
    (with-augmented-context (context phrase)
      (multiple-value-bind (conjunction conjunct)
          (get-features phrase *conjunctions*)
        (let ((a (apply #'say (term-sans phrase conjunction) :context context args))
              (b (apply #'say conjunct :context context args)))
          (if (characterp conjunction)
              `(,(concatenate 'string a (string conjunction) b))
              `(,a ,conjunction ,b)))))))

(define-phrase-type relative-clause (phrase)
  (:documentation "A clause that modifies the meaning of a noun phrase.")
  (:parameter *relative-pronouns* '(:that :when :which :who))
  (:predicate (thing phrase-type)
    (get-features thing *relative-pronouns*))
  (:realization ((phrase relative-clause) &rest args &key context)
    (with-augmented-context (context phrase)
      (multiple-value-bind (pronoun predicate)
          (get-features phrase *relative-pronouns*)
        `(,(apply #'say (term-sans phrase pronoun) :context context args)
          ,pronoun ,(apply #'say (term-sans-values predicate (name phrase))
                           :number (number-of phrase)
                           :person (person-of phrase)
                           :context context args))))))
