;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Use CL-DOT to generate category and ψ-term graphs.

(require :cl-dot)

;; The fontcolor node attribute seems to be missing. Probably just a bug.
(pushnew '(:fontcolor cl-dot::text) cl-dot::*node-attributes* :test #'equal)

(defun make-graph (object &key (direction :up)
                   (format "png")
                   (attributes
                    (ecase direction
                      (:up '(:rankdir "BT"))
                      (:down)))
                   (output-file
                    (make-pathname :name (format nil "~(~a-~a-~d~)"
                                                 (name object) direction
                                                 (random most-positive-fixnum))
                                   :type (string-downcase format)
                                   :directory '(:absolute "tmp")
                                   :case :local)))
  (cl-dot:dot-graph (cl-dot:generate-graph-from-roots direction (list object)
                                                      attributes)
                    (namestring output-file)
                    :format format)
  output-file)

;;; Category/class hierarchy graphs.

(defmethod cl-dot:graph-object-node (graph (object class))
  (declare (ignore graph))
  (make-instance 'cl-dot:node
                 :attributes `(:label ,(format nil "~a" (class-name object))
                               :fontcolor :blue)))

(defmethod cl-dot:graph-object-points-to ((graph (eql :up)) (object class))
  (parents object *category-lattice*))

(defmethod cl-dot:graph-object-points-to ((graph (eql :down)) (object class))
  (children object *category-lattice*))

(defun graph-categories-above (category &rest args
                               &key (direction :up) &allow-other-keys)
  (apply #'make-graph category :direction direction args))

(defun graph-categories-below (category &rest args
                               &key (direction :down) &allow-other-keys)
  (apply #'make-graph category :direction direction args))

;;; ψ-term graphs.

(defmethod cl-dot:graph-object-node (graph (object tag))
  (declare (ignore graph))
  (make-instance 'cl-dot:node
                 :attributes `(:label ,(format nil "!~a" (name object))
                               :shape :box
                               :color :red
                               :fontcolor :red)))

(defmethod cl-dot:graph-object-points-to (graph (object tag))
  (declare (ignore graph))
  (list (make-instance 'cl-dot:attributed
                       :attributes `(:color :red)
                       :object (tag-term object))))

(defmethod cl-dot:graph-object-node (graph (object term))
  (declare (ignore graph))
  (make-instance 'cl-dot:node
                 :attributes `(:label ,(format nil "~a" (name (term-type object)))
                               :fontcolor :blue)))

(defmethod cl-dot:graph-object-points-to (graph (object term))
  (declare (ignore graph))
  (loop for (feature subterm) on (subterms object) by #'cddr
        as edge-label = (format nil " ~a " (name feature))
        collect (make-instance 'cl-dot:attributed
                               :attributes `(:label ,edge-label
                                             :fontcolor "#009900")
                               :object subterm)))

(defun graph-ψ-term (term &rest args &key (direction :down) &allow-other-keys)
  (apply #'make-graph term :direction direction args))
