;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(defsystem :krunch
  :depends-on (:cl-ppcre) ; for regex rules
  :components ((:file "packages")
               (:file "utilities")
               (:file "morphology")
               (:file "names")
               (:file "lattice")
               (:file "lisp-lattice")
               (:file "bindings")
               (:file "categories")
               (:file "individuals")
               (:file "terms")
               (:file "rewrite")
               (:file "regex-rules")
               (:file "compose")
               (:file "ecis")
               (:file "ontologies")
               (:file "generator"))
  :in-order-to ((test-op (test-op :krunch/tests))))

(defsystem :krunch/tests
  :depends-on (:krunch)
  :components ((:file "test/rt")
               (:file "test/utilities")
               (:file "test/krunch"))
  :perform (test-op (o c) (symbol-call :rt :do-tests)))

(defsystem :krunch/sparser
  :depends-on (:krunch :sparser)
  :components ((:file "sparser")))

(defsystem :krunch/spire
  :depends-on (:krunch :spire)
  :components ((:file "spire")))

(defsystem :krunch/clic
  :depends-on (:krunch/spire)
  :components ((:file "trips-lf")
               (:file "clic-lattice")))

(defsystem :krunch/trips-bob
  :depends-on (:trips/bob-parser :krunch)
  :components ((:file "trips-lf")
               (:file "trips-lattice"))
  :in-order-to ((test-op (test-op :krunch/trips-bob-tests))))

(defsystem :krunch/trips-bob-tests
  :depends-on (:krunch/trips-bob :krunch/tests)
  :components ((:file "test/krunch-trips-bob"))
  :perform (test-op (o c) (symbol-call :rt :do-tests)))

(defsystem :krunch/viewer
  :depends-on (:krunch :cl-dot)
  :components ((:file "viewer")))

(setf (logical-pathname-translations "krunch")
      `(("**;*.lisp.*" ,(merge-pathnames "**/*.lisp"
                         (system-relative-pathname :krunch ""))))
      (logical-pathname-translations "clic")
      `(("**;*.lisp.*" ,(merge-pathnames "**/*.lisp"
                         (system-relative-pathname :clic "")))))

