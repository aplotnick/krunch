;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH; -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Term lattice hooks.

(defclass trips-lattice (lattice)
  ())

(defmethod children ((x symbol) (lattice trips-lattice))
  (if (ontp x)
      (om::get-children x)
      (call-next-method)))

(defmethod parents ((x symbol) (lattice trips-lattice))
  "The TRIPS ontology is single-inheritance only."
  (if (ontp x)
      (list (or (om::get-parent x) (lattice-top lattice)))
      (call-next-method)))

(defclass trips-term-lattice (trips-lattice term-lattice)
  ())

(defmethod normalize-ψ-term-type ((raw-type symbol) (lattice trips-term-lattice))
  "Let TRIPS concepts and words represent themselves."
  (if (or (ontp raw-type) (wp raw-type))
      (register-type raw-type lattice)
      (call-next-method)))

(defmethod normalize-features ((term term) (lattice trips-term-lattice))
  "Do not normalize features in the TRIPS lattice."
  term)

;;; Generation hooks.

(defun get-word-form (thing &key number (person :third) tense aspect voice
                      &allow-other-keys)
  (first
   (lxm::get-word-form thing
                       (case voice
                         (:passive :pastpart)
                         (otherwise (case tense
                                      (:present :pres)
                                      (:past :past)
                                      (otherwise
                                       (case aspect
                                         (:progressive :ing)
                                         (otherwise
                                          (case number
                                            (:singular :sing)
                                            (:plural :plur)
                                            (otherwise :bare))))))))
                       :agr (case number
                              (:singular
                               (ecase person
                                 (:first 'w::1s)
                                 (:second 'w::2s)
                                 (:third 'w::3s)))
                              (:plural
                               (ecase person
                                 (:first 'w::1p)
                                 (:second 'w::2p)
                                 (:third 'w::3p)))))))

(defmethod realize :around ((morphology function) (thing symbol) &rest args)
  (declare (optimize debug))
  (if (wp thing)
      (let ((word-form (apply #'get-word-form thing args)))
        (if word-form
            (string-downcase word-form)
            (call-next-method)))
      (call-next-method)))
