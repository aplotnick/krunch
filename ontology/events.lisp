;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defcat location (individual) "Spatial regions.")
(defcat time (individual) "Temporal regions.")
(defcat event (time location) "Things that happen.")
(defcat be (event) "Things that are.")

;; Prepositional features.
(define-feature :location 'location 'location)
(define-feature :time 'event 'time)
(define-feature :when 'event 'time)

;; Amounts of time.
(defcat second (time))
(defcat minute (time))
(defcat hour (time))
(defcat day (time))
(defcat week (time))
(defcat fortnight (time))
(defcat month (time))
(defcat year (time))
(defcat century (time))

;; Quantificational times (Lewis 1975).
(defcat quantificational-time (determiner time))
(defcat always (quantificational-time))
(defcat ever (quantificational-time))
(defcat frequently (quantificational-time))
(defcat infrequently (negation quantificational-time))
(defcat never (negation quantificational-time))
(defcat occasionally (quantificational-time))
(defcat often (quantificational-time))
(defcat rarely (negation quantificational-time))
(defcat sometimes (quantificational-time))
(defcat usually (quantificational-time))

;; Relative times.
(defcat relative-time (time))
(defcat after (relative-time))
(defcat before (relative-time))
(defcat during (relative-time))
(defcat now (relative-time))
(defcat soon (relative-time))

(define-rule (?x :after ?t . ?rest) (?x :time (after :object ?t) . ?rest))
(define-rule (?x :before ?t . ?rest) (?x :time (before :object ?t) . ?rest))
(define-rule (?x :during ?t . ?rest) (?x :time (during :object ?t) . ?rest))

;; Relative locations.
(defcat relative-location (location))
(defcat at (relative-location))
(defcat in (relative-location))
(defcat on (relative-location))
(defcat over (relative-location))
(defcat here (relative-location))
(defcat there (relative-location))

(define-rule (?x :at ?l . ?rest) (?x :location (at :object ?l) . ?rest))
(define-rule (?x :in ?l . ?rest) (?x :location (in :object ?l) . ?rest))
(define-rule (?x :on ?l . ?rest) (?x :location (on :object ?l) . ?rest))
(define-rule (?x :over ?l . ?rest) (?x :location (over :object ?l) . ?rest))
