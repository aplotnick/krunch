;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;; Attributes.
(defcat attribute (individual))
(defcat color (attribute))
(defcat red (color))
(defcat green (color))
(defcat blue (color))
(defcat size (attribute))
(defcat big (size))
(defcat small (size))
(defcat ordinal (attribute))
(defcat first (ordinal))
(defcat second (ordinal))
(defcat third (ordinal))
(defcat force (attribute))
(defcat gentle (attribute))
(defcat rough (attribute))

(define-feature :color 'individual 'color)
(define-feature :force 'individual 'force)
(define-feature :order 'individual 'ordinal)
(define-feature :size 'individual 'size)

;; Directions.
(defcat direction (location))
(defcat away (direction))
(defcat bottom (direction))
(defcat down (direction))
(defcat end (direction))
(defcat left (direction))
(defcat middle (direction))
(defcat right (direction))
(defcat together (direction))
(defcat top-of (direction) :name "top") ; cf. ⊤
(defcat up (direction))

;; Relative locations.
(defcat across (relative-location))
(defcat around (relative-location))
(defcat between (relative-location))
(defcat next-to (relative-location))
(defcat on-top (relative-location))
(defcat toward (relative-location))

;; Objects.
(defcat one (individual)) ; e.g., "another one"
(defcat physobj (location))
(defcat block (physobj))
(defcat row (physobj))
(defcat shelf (physobj))
(defcat stack (physobj))
(defcat staircase (physobj))
(defcat step (physobj))
(defcat table (physobj))

(define-feature :side 'block 'direction)

;; Events.
(defcat add (event))
(defcat build (event))
(defcat let-us (event))
(defcat make (event))
(defcat move (event))
(defcat pick (event))
(defcat pick-up (event))
(defcat push (event))
(defcat put (event))
(defcat put-down (put))
