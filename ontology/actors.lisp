;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defcat actor (location))
(defcat he (actor))
(defcat it (actor))
(defcat me (actor))
(defcat she (actor))
(defcat they (actor))
(defcat us (actor))
(defcat we (actor))
(defcat you (actor))

(define-rule sys me)
(define-rule user you)
