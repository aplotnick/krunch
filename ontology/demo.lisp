;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;; Attributes.
(defcat attribute (individual))

(defcat color (attribute))
(defcat black (color))
(defcat blue (color))
(defcat brown (color))
(defcat green (color))
(defcat red (color))
(defcat white (color))

(defcat size (attribute))
(defcat big (size))
(defcat little (size))

(defcat lazy (attribute))
(defcat quick (attribute))

(define-feature :color 'individual 'color)
(define-feature :size 'individual 'size)

;; Objects.
(defcat info (individual))
(defcat physobj (actor))
(defcat book (physobj info))
(defcat flower (physobj))
(defcat manner (individual))
(defcat mat (physobj))
(defcat milk (physobj))

(define-feature :about 'info 'individual)

;; Places.
(defcat basement (physobj))
(defcat house (physobj))
(defcat home (physobj))

;; Events.
(defcat beat (event))
(defcat buy (event))
(defcat chase (event))
(defcat drink (event))
(defcat eat (event))
(defcat go (event))
(defcat grow (event))
(defcat impress (event))
(defcat jump (event))
(defcat laugh (event))
(defcat lick (event))
(defcat like (event))
(defcat move (event))
(defcat own (event))
(defcat rain (event))
(defcat read (event))
(defcat sell (event))
(defcat shave (event))
(defcat take (event))

(define-morphology "buy" :ed "bought" :s "buys")
(define-morphology "drink" :ed "drank" :en "drunk")
(define-irregular-verb "drink"
  ("drink" :tense :present :person :first :number *))
(define-morphology "eat" :ed "ate" :en "eaten")
(define-morphology "go" :ed "went" :en "gone" :s "go")
(define-irregular-verb "go"
  ("goes" :tense :present :person :third :number :singular))
(define-morphology "grow" :ed "grew" :en "grown")
(define-morphology "read" :ed "read")
(define-morphology "take" :ed "took" :en "taken")
