;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (loop for package in '(:kb :ont)
        unless (find-package package)
        do (make-package package :use '())))

(unionf *aggregates* '(kb::seq-fn))

;; Transformation rules.

(define-rule :db-link :href)
(define-rule :quan :determiner)
(define-rule ont::universal all)
(define-rule kb::+ +)

(define-rule (?head :spec ?spec . ?rest) (?head . ?rest)) ; drop spec by default
(define-rule (?head :spec ont::indef-set :quantity ?size . ?rest)
             (?head :determiner ?size :number plural . ?rest))
(define-rule (?head :spec ont::indef-set :quantity nil . ?rest)
             (?head :determiner any :number plural . ?rest))
(define-rule (?head :spec ont::wh-term-set . ?rest) (?head :number plural . ?rest))
(define-rule (?head :name ?name :name-of ?name-of . ?rest) (?head :name ?name . ?rest))
(define-rule (?head :pro ?pro :proform ?pro . ?rest) (?head :determiner ?pro . ?rest))
(define-rule (?head :subject (kb::seq-fn) . ?rest) (?head :subject any :negation not . ?rest))
(define-rule (?head :object (kb::seq-fn) . ?rest) (?head :object any :negation not . ?rest))

(define-rule (* * :name ?name :assoc-with ?assoc) (?name :mod ?assoc))
(define-rule (* * :instance-of ?class :assoc-with ?assoc) (?class :mod ?assoc))

(define-rule (?head ?var :drum ((:drum (kb::term :matches ((kb::match :matched ?matched))) . *)) . ?rest)
             (?head ?var :name ?matched . ?rest))

(define-rule (kb::a * :compact-answer ?answer) ?answer)
(define-rule (kb::a * :isa ?type . ?rest) (?type . ?rest))
(define-rule (kb::a * :isa * :lex ?lex . ?rest) (?lex . ?rest))

(define-rule (kb::a * :isa ont::sequence :elements ?elements) ?elements)
(define-rule (kb::a * :isa ont::have :lex ?lex :theme ?theme :ground ?ground . ?rest)
             (?lex :subject ?theme :comp ?ground . ?rest))
(define-rule (kb::a * :isa ont::same-as :lex ?lex :theme ?theme :ground ?ground . ?rest)
             (?lex :subject ?theme :comp ?ground . ?rest))

(define-rule (kb::a * :isa kb::learn :agent ?agent :topic ?topic . ?rest)
             (kb::a * :isa kb::learn :subject ?agent :object ?topic . ?rest))
(define-rule (kb::a * :isa kb::part-of :theme ?theme :ground ?ground :tense ?tense . ?rest)
             (kb::a * :isa kb::part-of :subject (be :subject ?theme :tense ?tense) :object ?ground . ?rest))
(define-rule (kb::a * :isa kb::share-property :lex ?lex :theme ?theme :ground ?ground . ?rest)
             (?lex :subject ?theme :object ?ground . ?rest))

(define-rule (kb::a ?var :agent ?agent :ground ?agent . ?rest)
             (kb::a ?var :agent ?agent . ?rest))
(define-rule (kb::a ?var :patient ?patient :theme ?patient . ?rest)
             (kb::a ?var :patient ?patient . ?rest))
(define-rule (kb::a ?var :location (kb::a * :lex ?lex :ground ?ground) . ?rest)
             (kb::a ?var :location (?lex :object ?ground) . ?rest))
(define-rule (kb::a ?var :mod (kb::a * :lex w::for :ground ?ground) . ?rest)
             (kb::a ?var :for ?ground :determiner a . ?rest))
(define-rule (kb::a ?var :base ?base :assoc-with ?base . ?rest)
             (kb::a ?var :assoc-with ?base . ?rest))

; Default top-level tense for system queries.
(define-rule (kb::a * :isa kb::confirm :query (?query ?var . ?rest))
             (?query ?var :tense present . ?rest))
(define-rule (kb::a * :isa kb::find :condition (?condition ?var . ?rest))
             (?condition ?var :tense present . ?rest))

; Rewrite bogus words & names from TRIPS.
(define-rerule "\\bC-?FOS\\b" "c-FOS")
(define-rerule "(.*)-GENE$" "the \\1 gene")
(define-rerule "^MIR-(.*)-([0-9])-([A-Z])" "miR-\\1-\\2\\3")
(define-rerule "^MIR-" "miR-")
(define-rerule "-?PUNC-MINUS-?" "-")

; Drop superfluous plural on "transcription factors".
; These sometimes come in as :NAME, other times :LEX.
(define-rerule "^TRANSCRIPTION-FACTORS?$" "transcription factor")
(define-rule (kb::a ?var :isa kb::transcription-factor :lex w::transcription-factors . ?rest)
             (kb::a ?var :isa kb::transcription-factor :lex w::transcription-factor . ?rest))

; Drop superfluous (GENE :ASSOC-WITH GENE-PROTEIN).
(define-rule (ont::gene :name ?name :assoc-with (ont::gene-protein :name ?name) . ?rest)
             (ont::gene :name ?name . ?rest))

;; Co-composition rules.

(define-corule
  ((kb::a * :query (?query ?var . ?rest))
   (kb::a * :compact-answer kb::true))
  (?query ?var :response yes :tense present . ?rest))

(define-corule
  ((kb::a * :query (?query ?var . ?rest))
   (kb::a * :compact-answer kb::false))
  (?query ?var :response no :negation not :tense present . ?rest))

(define-corule
  ((kb::a *
          :isa kb::find
          :condition (?condition ?var :agent ?agent :patient ?patient . ?rest)
          :theme ?agent)
   (kb::a * :compact-answer ?answer))
  (?condition ?var :agent ?answer :patient ?patient :tense present :voice passive . ?rest))

(define-corule
  ((kb::a *
          :isa kb::find
          :condition (?condition ?var :agent ?agent :patient ?patient . ?rest)
          :theme ?patient)
   (kb::a * :compact-answer ?answer))
  (?condition ?var :agent ?agent :patient ?answer :tense present . ?rest))

(define-corule
  ((kb::a *
          :isa kb::find
          :condition (?condition ?var :theme ?theme :ground ?ground . ?rest)
          :theme ?ground)
   (kb::a * :compact-answer ?answer))
  (?condition ?var :theme ?theme :ground ?answer :tense present . ?rest))

(define-corule
  ((kb::ask-if :query (?question ?var . ?rest))
   (kb::a * :compact-answer kb::true))
  (?question ?var :response yes . ?rest))

(define-corule
  ((kb::ask-if :query (?question ?var . ?rest))
   (kb::a * :compact-answer kb::false))
  (?question ?var :response no :negation not . ?rest))

(define-corule
  ((kb::ask-wh :what ?what :query (?query ?var :agent ?what . ?rest))
   (kb::a * :compact-answer ?answer))
  (?query ?var :agent ?answer . ?rest))

(define-corule
  ((kb::ask-wh :what ?what :query (?query ?var :affected ?what . ?rest))
   (kb::a * :compact-answer ?answer))
  (?query ?var :affected ?answer . ?rest))
