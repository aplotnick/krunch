;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defcat animal (actor))
(defcat pet (animal))
(defcat aardvark (animal))
(defcat bird (animal))
(defcat canary (pet bird))
(defcat ostrich (bird))
(defcat carnivore (animal))
(defcat canid (carnivore))
(defcat cat (carnivore))
(defcat dog (canid pet))
(defcat poodle (dog))
(defcat deer (animal))
(defcat donkey (animal))
(defcat fox (canid))
(defcat fluffy (cat) :name "Fluffy")
(defcat mouse (animal))

(define-morphology "deer" :ed "deer" :s "deer")
(define-morphology "mouse" :s "mice")
