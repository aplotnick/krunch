;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defcat name ()
  ((first :value-restriction string)
   (last :value-restriction string)))

(defcat person (individual)
  ((id :value-restriction name)
   (age :value-restriction real)
   (gender :value-restriction gender)))

(define-morphology "person" :s "people")

(defcat barber (person))
(defcat farmer (person))
(defcat friend (person))
(defcat student (person))
(defcat employee (person))
(defcat staff (employee))
(defcat workstudy (student staff))

(defcat married-person (person)
  ((spouse :value-restriction married-person)))

(defcat man (person)
  ((gender :value-restriction male :initform cat:male)))

(defcat aaron (man) :name "Aaron")
(defcat peter (man) :name "Peter")
(defcat simon (man) :name "Simon")

(defcat woman (person)
  ((gender :value-restriction female :initform cat:female)))

(define-feature :id 'person 'name)
(define-feature :first 'name 'string)
(define-feature :last 'name 'string)
(define-feature :address 'person 'location)
(define-feature :spouse 'married-person 'married-person)
(define-feature :age 'person 'integer)
