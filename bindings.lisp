;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defmethod name ((object standard-slot-definition))
  (slot-definition-name object))

(defgeneric value-restriction (slot)
  (:method (slot)
    t)
  (:method ((slot standard-slot-definition))
    (slot-definition-type slot)))

(defclass value-restricted-slot-definition (standard-slot-definition)
  ((value-restriction :accessor value-restriction :initarg :value-restriction))
  (:default-initargs :value-restriction t))

(defclass value-restricted-direct-slot-definition
    (value-restricted-slot-definition standard-direct-slot-definition)
  ())

(defclass value-restricted-effective-slot-definition
    (value-restricted-slot-definition standard-effective-slot-definition)
  ((bindings :accessor slot-bindings :initarg :bindings))
  (:default-initargs :bindings (make-hash-table :test 'equal)))

(defmethod shared-initialize ;
    ((instance value-restricted-slot-definition) slot-names &rest initargs &key
     (value-restriction t))
  (apply #'call-next-method instance slot-names
         :value-restriction value-restriction
         initargs))

(defclass bindings (standard-class)
  ()
  (:documentation "Metaclass for lattice-indexed (slot value) bindings."))

(defmethod compute-effective-slot-definition
    ((class bindings) name direct-slot-definitions)
  (let ((slot (call-next-method)))
    (typecase slot
      (value-restricted-effective-slot-definition
       (setf (value-restriction slot)
             `(and ,@(mapcar #'value-restriction direct-slot-definitions)))))
    slot))

(defmethod direct-slot-definition-class
    ((class bindings) &key (value-restriction t) &allow-other-keys)
  (case value-restriction
    ((t) (call-next-method))
    (t (find-class 'value-restricted-direct-slot-definition))))

(defmethod effective-slot-definition-class
    ((class bindings) &key (value-restriction t) &allow-other-keys)
  (case value-restriction
    ((t) (call-next-method))
    (t (find-class 'value-restricted-effective-slot-definition))))

#+sbcl ; non-standard but necessary
(defmethod sb-pcl::compute-effective-slot-definition-initargs :around
    ((class bindings) direct-slot-definitions)
  "Pass the :VALUE-RESTRICTION option from the direct slot definitions
to the effective ones."
  (let ((slot (find-if (lambda (slot)
                         (typep slot 'value-restricted-slot-definition))
                       direct-slot-definitions)))
    (if slot
        (list* :value-restriction (value-restriction slot)
               (call-next-method))
        (call-next-method))))

(define-condition value-restriction-error (type-error)
  ()
  (:documentation "A slot value restriction was not satisfied."))

(defmethod (setf slot-value-using-class)
    (new-value
     (class bindings) object
     (slot value-restricted-effective-slot-definition))
  (if (itypep new-value (value-restriction slot))
      (call-next-method)
      (error 'value-restriction-error
             :datum new-value
             :expected-type (value-restriction slot))))
