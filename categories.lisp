;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;; The category metaclass must exist at compile-time-too (ANSI CL §3.2.3),
;; and so must the support routines used by the defining macro DEFCAT.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (let (#+sbcl (sb-ext:*on-package-variance* '(:warn t)))
   (handler-bind ((warning #'muffle-warning))
     (defpackage :cat
       (:use)
       (:import-from :krunch :named-object)
       (:export :top :named-object)
       (:documentation "KRUNCH categories."))))

  (defvar *category-package*
    (or (find-package :cat)
        (error "Can't find the category package."))
    "The category package.")

  (defun catsym (symbol)
    (reintern symbol *category-package*))

  (defvar *categories*
    '()
    "All of the categories that are currently defined.")

  (defvar *default-category-lattice-type*
    'bottom-up-binary-encoding
    "The type of lattice to use for the category hierarchy.")

  (defvar *category-lattice*
    (make-instance *default-category-lattice-type*)
    "The category lattice.")

  (defclass category (bindings)
    ((instances :accessor instances :initarg :instances))
    (:default-initargs :instances '())
    (:documentation "Base metaclass for all categories."))

  (defmethod shared-initialize :after ((instance category) slots &key)
    (declare (ignore slots))
    (pushnew instance *categories*)
    (register-type instance *category-lattice*))

  (defmethod validate-superclass ((class category) (superclass standard-class))
    "Declare the category metaclass to be compatible with standard classes."
    t)

  (define-condition category-not-found (error)
    ((name :reader name :initarg :name))
    (:report (lambda (error stream)
               (format stream "There is no category named ~a." (name error)))))

  (defun category-not-found (name &optional (errorp t))
    "Maybe signal that a category is not found."
    (when errorp
      (error 'category-not-found :name name)))

  (defgeneric category-name (name &optional errorp)
    (:documentation "Resolve a category name designator.")
    (:method (name &optional (errorp t))
      (category-not-found name errorp))
    (:method ((name (eql t)) &optional errorp)
      (declare (ignore errorp))
      t)
    (:method ((name category) &optional errorp)
      (declare (ignore errorp))
      (name name))
    (:method ((name symbol) &optional (errorp t))
      (cond ((null (symbol-package name)) name)
            (t (multiple-value-bind (symbol status)
                   (find-symbol (symbol-name name) *category-package*)
                 (cond (symbol
                        (when errorp
                          (assert (eql status :external)
                                  (name status)
                                  "~a should be external in the category package."
                                  name))
                        symbol)
                       (t (category-not-found name errorp))))))))

  (defgeneric find-category (name &optional errorp)
    (:documentation "Look up a category by name.")
    (:method ((name category) &optional errorp)
      (declare (ignore errorp))
      name)
    (:method ((name cons) &optional (errorp t))
      (case (car name)
        ((and or not)
         (flet ((find-category (name) (find-category name errorp)))
           `(,(car name) ,@(mapcar #'find-category (cdr name)))))
        (t (category-not-found name errorp))))
    (:method ((name symbol) &optional (errorp t))
      (handler-case (find-class (category-name name errorp) errorp)
        (error () (category-not-found name errorp))))
    (:method (name &optional (errorp t))
      (category-not-found name errorp))))

(defmethod print-object ((object category) stream)
  (let ((*package* *category-package*))
    (print-unreadable-object (object stream :type nil :identity nil)
      (princ (name object) stream))))

(defmethod children ((x symbol) lattice &aux (category (find-category x nil)))
  "Assume X names a category."
  (if category
      (children category lattice)
      (call-next-method)))

(defmethod parents ((x symbol) lattice &aux (category (find-category x nil)))
  "Assume X names a category."
  (if category
      (parents category lattice)
      (call-next-method)))

(defgeneric feature-name (object)
  (:documentation "Return a feature name for OBJECT.")
  (:method (object)
    (constant-type-name object))
  (:method ((object category))
    "Go one level up the category hierarchy."
    (constant-type-name (first (class-direct-superclasses object)))))

(defgeneric catspec (catspec)
  (:documentation "Canonicalize a category specifier for ITYPEP.")
  (:method ((catspec category))
    catspec)
  (:method ((catspec symbol))
    (or (find-category catspec nil)
        (and (eq (symbol-package catspec) (find-package "COMMON-LISP"))
             catspec)))
  (:method ((catspec null)))
  (:method ((catspec cons))
    (etypecase catspec
      ((cons (member or :or) *) `(or ,@(mapcar #'catspec (cdr catspec))))
      ((cons (member and :and) *) `(and ,@(mapcar #'catspec (cdr catspec))))
      ((cons (member not :not) (cons * null)) `(not ,(catspec (cadr catspec)))))))

(defgeneric itypep (individual catspec)
  (:documentation
   "Is an individual/category an instance/subcategory of a category/individual?")
  (:method (individual catspec)
    (typep individual (catspec catspec)))
  (:method ((individual null) catspec)
    (null catspec))
  (:method ((individual symbol) catspec)
    (ignore-error lattice-encoding-error (itypep (catspec individual) catspec)))
  (:method ((individual category) catspec)
    (subcatp individual (catspec catspec))))

(defun subcatp (x y &optional (lattice *category-lattice*))
  "Is X a subcategory of Y?"
  (lower-bound-p (find-category x) (find-category y) lattice))

(defun least-specific-subcat (x y &optional (lattice *category-lattice*))
  "Find the least specific (i.e., most general) sub-category of X and Y."
  (glb (find-category x) (find-category y) lattice))

(defun most-specific-supercat (x y &optional (lattice *category-lattice*))
  "Find the most specific (i.e., least general) super-category of X and Y."
  (lub (find-category x) (find-category y) lattice))

(defun make-category-documentation (name)
  (+s (string-capitalize (substitute #\Space #\- (string name)))))

(defmacro defcat (name supercats &rest slots/options &aux
                  (name (catsym name))
                  (documentation
                   (and slots/options
                        (stringp (car slots/options))
                        (pop slots/options)))
                  (cat-options
                   (and slots/options
                        (keywordp (car slots/options))
                        (do* ((options ())
                              (key (pop slots/options) (pop slots/options))
                              (value (pop slots/options) (pop slots/options)))
                             ((listp key) (nreverse options))
                          (push key options)
                          (push value options))))
                  (default-name
                   (getf cat-options :name))
                  (default-initargs
                   (when default-name `(:name ,default-name)))
                  (feature-name
                   (getf cat-options :feature-name))
                  (supercats
                   (mapcar (lambda (cat)
                             (or (find-category (catsym cat) nil)
                                 (find-class cat nil)
                                 cat))
                           (or supercats
                               (case name
                                 (cat:top '()) ; avoid infinite regress
                                 (t (if default-name
                                        '(top named-object)
                                        '(top)))))))
                  (slots
                   (mapcar (lambda (slot &aux (slot (ensure-list slot)))
                             (unless (getf (cdr slot) :initarg)
                               (setq slot `(,@slot
                                            :initarg ,(intern
                                                       (symbol-name (car slot))
                                                       :keyword))))
                             slot)
                           (car slots/options)))
                  (class-options
                   (if (assoc :metaclass (cdr slots/options))
                       (cdr slots/options)
                       `(,@(cdr slots/options) (:metaclass category))))
                  (class-options
                   (if (assoc :default-initargs class-options)
                       class-options
                       `(,@class-options (:default-initargs ,@default-initargs))))
                  (documentation
                   (or (cadr (assoc :documentation class-options))
                       (cadar (push `(:documentation
                                      ,(or documentation
                                           (make-category-documentation name)))
                                    class-options)))))
  "Define a category, and set a special variable with the same name to it."
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (defparameter ,name
       (defclass ,name ,supercats ,slots ,@class-options)
       ,documentation)
     (when (eq (symbol-package ',name) *category-package*)
       (export ',name *category-package*))
     ,@(when feature-name
         `((defmethod feature-name ((object (eql ,name))) ,feature-name)))
     (find-class ',name)))

(defcat top () "Everything.")
