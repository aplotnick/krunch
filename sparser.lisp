;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

;;; KRUNCH interface to Sparser.

(in-package :krunch)

(unionf *aggregates* (list category::collection))
