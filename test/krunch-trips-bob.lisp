;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package :rt))

(deftest (trips says book was red)
  (with-ontology trips
    (say (lf "the book was red")))
  "the book was red")

(deftest (trips says book was read)
  (with-ontology trips
    (say (lf "the book was read")))
  "the book was read")

(deftest (trips says three people read book)
  (with-ontology trips
    (values (say (lf "three people read the book"))
            (say (lf "three people are reading the book"))
            (say (lf "three people have read the books"))))
  "three people read the book"
  "three people are reading the book"
  "three people have read the books")

(deftest (trips says the ball is red)
  (with-ontology trips
    (values (say (lf "the ball is red"))
            (say (lf "the ball was red"))
            (say (lf "the ball will be red"))
            (say (lf "the ball shall be red"))))
  "the ball is red"
  "the ball was red"
  "the ball will be red"
  "the ball shall be red")

(deftest (trips says mek phosphorylates erk)
  (with-ontology trips
    (values (say (lf "MEK phosphorylates ERK."))
            (say (lf "ERK is phosphorylated by MEK."))))
  "mek phosphorylates erk"
  "erk is phosphorylated by mek")

(deftest (trips says find drug that targets kras)
  (with-ontology trips
    (say (lf "Find a drug that targets KRAS.")))
  "you find a drug that targets kras")

(deftest (trips says found drug like that yesterday)
  (with-ontology trips
    (say (lf "I found a drug like that yesterday.")))
  "I found a drug like that yesterday")

(deftest (trips says find drugs that target kras)
  (with-ontology trips
    (say (lf "Find drugs that target KRAS.")))
  "you find drugs that target kras")

(deftest (trips says couldn’t find any drugs like that)
  (with-ontology trips
    (say (lf "I couldn't find any drugs like that.")))
  "I couldn't find any drugs like that")

(deftest (trips ask what drugs target braf)
  (with-ontology trips
    (ask (lf "What drugs target BRAF?")))
  "What drugs target braf?")

(deftest (trips ask if there are any drugs that target braf)
  (with-ontology trips
    (ask (lf "Are there any drugs that target BRAF?")))
  "Are there any drugs that target braf?")

(deftest (trips says vemurafenib targets braf)
  (with-ontology trips
    (say (lf "Vemurafenib targets BRAF.")))
  "vemurafenib targets braf")

(deftest (trips ask what vemurafenib’s target is)
  (with-ontology trips
    (ask (lf "What is the target of Vemurafenib?")))
  "What is the target of vemurafenib?")

(deftest (trips says don’t know the target of vemurafenib)
  (with-ontology trips
    (say (lf "I don't know the target of Vemurafenib.")))
  "I don't know the target of vemurafenib")

(deftest (trips says nominal target of vemurafenib is braf)
  (with-ontology trips
    (say (lf "The nominal target of Vemurafenib is BRAF.")))
  "the nominal target of vemurafenib is braf")
