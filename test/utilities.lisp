;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

;;; Lisp utility tests.

(in-package :krunch)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package :rt))

;;; Conditions.

(deftest ignore-error
  (let ((x (list* :a :b :c)))
    (values (handler-case (ignore-error nil (member :c x))
              (type-error () t))
            (ignore-error type-error (member :c x))))
  t
  nil)

;;; Symbols.

(deftest reintern
  (reintern 'a :keyword)
  :a)

(deftest (reintern uninterned)
  (reintern '#1=#:a :keyword)
  #1#)

;;; Conses.

(deftest (ensure-list atom)
  (ensure-list t)
  (t))

(deftest (ensure-list null)
  (ensure-list nil)
  nil)

(deftest (ensure-list cons)
  (ensure-list (cons :a :b))
  (:a . :b))

(deftest (ensure-list list)
  (ensure-list (list :a :b :c))
  (:a :b :c))

(deftest list-length*
  (values (list-length* nil)
          (list-length* :a)
          (list-length* '(:a :b))
          (list-length* '(:a . :b)))
  0
  1
  2
  2)

(deftest recons
  (values (recons nil :a :b)
          (let ((c (cons :a :b)))
            (eql c (recons c :a :b))))
  (:a . :b)
  t)

(deftest tail-cons
  (tail-cons :x (list :a :b :c))
  (:a :b :c :x))

(deftest (tail-cons nil)
  (tail-cons :x nil)
  (:x))

(deftest nconcf
  (let ((x (list :a :b :c)))
    (nconcf x '(:x)))
  (:a :b :c :x))

;; Property lists.

(deftest reverse-plist
  (reverse-plist '(:a 1 :b 2 :c 3))
  (:c 3 :b 2 :a 1))

(deftest sans
  (apply (lambda (&rest args) (assert (null args)) t)
         (sans '(:foo foo :bar bar) :foo :bar))
  t)

(deftest quote-every-other-one
  (let ((x '(:a 1 :b 2 :c 3)))
    (values (quote-every-other-one x :odd)
            (quote-every-other-one x :even)))
  (:a '1 :b '2 :c '3)
  (':a 1 ':b 2 ':c 3))

;;; Sequences.

(deftest choose
  (loop with n = 100
        with rs1 = (make-random-state nil)
        with rs2 = (make-random-state rs1)
        with l1 = (loop repeat n collect (random n rs1))
        with l2 = (loop repeat n collect (random n rs2))
        initially (assert (equal l1 l2))
        repeat n
        as i1 = (choose l1 rs1)
        as i2 = (choose l2 rs2)
        always (and (eql i1 i2)
                    (member i1 l1)
                    (member i2 l2)))
  t)

;;; Hash tables.

(defun make-test-hash (&key (initial-contents '(:a 1 :b 2 :c 3)))
  (loop with hash-table = (make-hash-table)
        for (key val) on initial-contents by #'cddr
        do (setf (gethash key hash-table) val)
        finally (return hash-table)))

(deftest copy-hash-table
  (let* ((table (make-test-hash))
         (copy (copy-hash-table table)))
    (values (eql table copy)
            (equalp table copy)))
  nil
  t)
