;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package :rt)
  (setq *readtable* *ψ-term-readtable*))

;;; Category lattice tests.

(deftest glb
  (with-categories ((a ())
                    (b (a))
                    (c (a))
                    (d (b c)))
    (with-lattice-operators t
      (values (eq (glb a b) b)
              (eq (glb a c) c)
              (eq (glb b c) d))))
  t
  t
  t)

(deftest lub
  (with-categories ((a ())
                    (b (a))
                    (c (a)))
    (with-lattice-operators t
      (eq (lub b c) a)))
  t)

(deftest identities
  (with-categories ()
    (with-lattice-operators t
      (values-list (mapcar (complement #'bottom)
                           (list (glb t t)
                                 (glb t nil)
                                 (glb nil t)
                                 (glb nil nil)
                                 (lub t t)
                                 (lub t nil)
                                 (lub nil t)
                                 (lub nil nil))))))
  t
  nil
  nil
  nil
  t
  t
  t
  nil)

(defclass bottom-up-lisp-lattice (lisp-lattice bottom-up-binary-encoding)
  ())

(deftest lisp-lattice
  (with-lattice-operators (make-instance 'bottom-up-lisp-lattice)
    (handler-bind ((lattice-encoding-error #'continue))
      (values (ubp (find-class 'number) 1)
              (lbp 1 (find-class 'number))
              (glb (find-class 'fixnum) 1)
              (glb (find-class 'integer) 1)
              (glb (find-class 'integer) 1.0)
              (glb (find-class 'float) 1.0)
              (glb (find-class 'real) 1.0)
              (glb (find-class 'symbol) :foo)
              (glb (find-class 'string) "foo"))))
  t
  t
  1
  1
  nil
  1.0
  1.0
  :foo
  "foo")

(deftest find-category
  (values (eq (find-category 'top) cat:top)
          (find-category nil nil))
  t
  nil)

(deftest (category documentation)
  (with-categories ((foo () "Foo!"))
    (values (documentation cat:top 'type)
            (documentation 'cat:top 'variable)
            (documentation foo 'type)
            (documentation (name foo) 'variable)))
  "Everything."
  "Everything."
  "Foo!"
  "Foo!")

(deftest subcatp
  (with-categories ((foo ())
                    (bar (foo)))
    (with-lattice-operators t
      (values (subcatp foo bar)
              (subcatp bar foo))))
  nil
  t)

(deftest itypep
  (with-categories ((foo ())
                    (bar (foo)))
    (values (itypep foo foo)
            (itypep (foo) t)
            (itypep (foo) foo)
            (not (itypep (foo) nil))
            (not (itypep (foo) bar))
            (not (itypep (foo) (gensym)))
            (not (itypep (gensym) foo))
            (itypep (bar) foo)
            (itypep (bar) bar)
            (itypep (foo) `(or ,foo ,bar))
            (not (itypep (foo) `(or ,bar)))
            (itypep (foo) `(and t ,foo))
            (not (itypep (foo) `(and ,foo ,bar)))
            (not (itypep (foo) `(not ,foo)))
            (itypep (foo) `(not ,bar))))
  t t t t t t t t t t t t t t t)

(deftest (itypep subcat)
  (with-categories ((foo ())
                    (bar (foo)))
    (values (itypep foo bar)
            (itypep bar foo)
            (itypep bar `(and ,foo ,bar))))
  nil
  t
  t)

(deftest make-individual
  (with-categories ((foo (individual)))
    (let ((foo1 (make-individual foo :name 'foo1))
          (foo2 (make-individual foo :name 'foo2)))
      (values (eq foo1 foo2)
              (eq (category-of foo1) (category-of foo2)))))
  nil
  t)

(deftest delete-individual
  (with-categories ((foo (individual)))
    (let ((foo* (make-individual foo)))
      (values (eq foo* (find foo* (instances foo)))
              (find (delete-individual foo*) (instances foo)))))
  t
  nil)

(deftest individual-named
  (with-categories ((foo (individual)))
    (let* ((name (gensym))
           (foo* (make-individual foo :name name)))
      (values (eq foo* (individual-named foo* foo))
              (eq foo* (individual-named name foo)))))
  t
  t)

(deftest find-individual
  (with-categories ((foo (individual))
                    (baz (foo) (zip)))
    (let* ((name (gensym))
           (foo* (make-individual foo :name name))
           (baz* (make-individual baz :zip 'quux)))
      (values (eq foo* (find-individual foo*))
              (eq foo* (find-individual name))
              (eq foo* (find-individual `(name ,name)))
              (eq foo* (find-individual `(,foo (name ,name))))
              (eq baz* (find-individual `(zip quux))))))
  t
  t
  t
  t
  t)

;; Figure 1 of Aït-Kaci et al. (1989)
(deftest (Aït-Kaci fig1)
  (with-ontology demo
    (eq (least-specific-subcat 'student 'employee)
        (find-category 'workstudy)))
  t)

;; Figure 4 of Aït-Kaci et al. (1989)
(deftest (Aït-Kaci fig4)
  (with-categories ((f ())
                    (e (f))
                    (d ())
                    (c (d e))
                    (b (d f))
                    (a (b c)))
    (with-lattice-operators t
      (eq (glb d e) c)))
  t)

;; Figure 3 of Aït-Kaci & Amir (2015)
(deftest (Aït-Kaci&Amir fig4)
  (with-categories ((k ())
                    (l ())
                    (h (k))
                    (i (k l))
                    (j (k l))
                    (f (h i j))
                    (g (i j))
                    (c (f))
                    (d (f g))
                    (e (g))
                    (a (c d))
                    (b (d e)))
    (with-lattice-operators t
      (values (eq (glb f g) d)
              (set-difference (glb i j) (list f g)))))
  t
  nil)

;; Figure 5 of Aït-Kaci & Amir (2015)
(deftest (Aït-Kaci&Amir fig5)
  (with-ontology demo
    (values (eq (least-specific-subcat 'pet 'bird)
                (find-category 'canary))
            (eq (most-specific-supercat 'dog 'canary)
                (find-category 'pet))))
  t
  t)

;; Figure 3 of van Bommel & Beck (2000)
(deftest (von-Bommel&Beck fig3)
  (with-categories ((a ())
                    (b (a))
                    (c (a))
                    (d (a))
                    (e (b))
                    (f (c))
                    (g (d))
                    (h (d))
                    (i (e))
                    (j (f))
                    (k (g h))
                    (l (g h))
                    (m (i j))
                    (n (k l)))
    (with-lattice-operators t
      (values (eq (glb i j) m)
              (set-difference (glb g h) (list k l))
              (eq (glb e f) m))))
  t
  nil
  t)

(deftest read&parse-ψ-term
  (let* ((*package* (find-package "KRUNCH"))
         (*readtable* *ψ-term-readtable*)
         (a (parse-ψ-term (read-from-string "ψ")))
         (b (parse-ψ-term (read-from-string "!b : b")))
         (c (parse-ψ-term (read-from-string "!c : number")))
         (d (parse-ψ-term (read-from-string "!d : (d :a t :b !b : b :c 3)"))))
    (values (null (symbol-package (name a))) ; uninterned
            (eq (term-type a) 't)
            (eq (name b) 'b)
            (eq (term-type b) 'b)
            (eq (term-type c) 'number)
            (eq (term-type d) 'd)
            (eq (feature :a d) ⊤)
            (eq (term-type (feature :b d)) 'b)
            (eq (term-type (feature :c d)) 3)))
  t
  t
  t
  t
  t
  t
  t
  t
  t)

(defun make-john ()
  "Instantiate the example from HOOT figure 7."
  (normalize '!P : (person :id (name :first "John" :last !S : "Doe")
                           :address !A : location
                           :age 42
                           :spouse (person :id (name :first "Jane" :last !S)
                                           :address !A : location
                                           :spouse !P))))

(deftest (hoot fig7)
  (with-ontology demo
    (let* ((john (make-john))
           (jane (feature :spouse john)))
      (values (eq (term-type john) (find-category 'married-person))
              (eq (term-type jane) (find-category 'married-person))
              (eq (term-type (feature :address john))
                  (find-category 'location))
              (eq (feature :address john)
                  (feature :address jane))
              (eq (feature :last (feature :id john))
                  (feature :last (feature :id jane))))))
  t
  t
  t
  t
  t)

(deftest (normalize duplicate features)
  (with-ontology demo
    (let ((john (normalize '(name :first "John" :first "Jane")))
          (jane (normalize '(name :first "Jane" :first "John"))))
      (values (term-type (feature :first john))
              (term-type (feature :first jane)))))
  "John"
  "Jane")

;;; Term rewriting tests.

(deftest term-sans
  (with-ontology demo
    (let* ((john (make-john))
           (anon (term-sans (feature :id john) :first :last)))
      (values (constant-type-name anon)
              (null (subterms anon)))))
  :name
  t)

(deftest term-sans-values
  (with-ontology demo
    (let* ((john (make-john))
           (jane (feature :spouse john)))
      (feature :spouse (term-sans-values john jane))))
  nil)

(deftest bind*
  (macrolet ((no-match-p (&body body) `(null (catch 'no-match ,@body))))
    (values (null (bind* 'a 'a))
            (no-match-p (bind* 'a 'b))
            (null (bind* '* 'a))
            (null (bind* '(a b) '(a b)))
            (no-match-p (bind* '(a b) '(a c)))
            (equal (bind* '?x 'a) '((?x . a)))
            (equal (bind* '(:a b . ?x) '(:x y :a b :c d))
                   '((?x :x y :c d)))))
  t
  t
  t
  t
  t
  t
  t)

(deftest keys
  (values (keys '(a :a a :b nil))
          (keys '(a :a a :b nil :c (:d . :e)) t))
  (:b :a)
  (:c :a))

(deftest rule-lessp
  (let ((a (parse-rule '((a) t)))
        (b (parse-rule '((a b) t)))
        (c (parse-rule '((a *) t)))
        (d (parse-rule '((a . *) t))))
    (equal (sort-rules (list a b c d))
           (list b c d a)))
  t)

(deftest (rewrite static)
  (with-rewrite-rules ((a b)
                       ((a c) (d c))
                       ((d c) e)
                       (e f))
    (values (equal (rewrite nil) nil)
            (equal (rewrite t) t)
            (equal (rewrite 'a) 'b)
            (equal (rewrite 'b) 'b)
            (equal (rewrite '(a b)) '(b b))
            (equal (rewrite '(a c)) 'f)))
  t
  t
  t
  t
  t
  t)

(deftest (rewrite variable)
  (with-rewrite-rules ((?x ?x)
                       ((?x x ?y) (?y y ?x)))
    (values (rewrite 'a)
            (rewrite '(a x b))))
  a
  (b y a))

(deftest (rewrite variable eql)
  (with-rewrite-rules (((?x ?x) ?x))
    (values (rewrite '(x x))
            (rewrite '(x y))))
  x
  (x y))

(deftest (rewrite variable equal)
  (with-rewrite-rules (((?a (?a)) (?a)))
    (values (rewrite '(a (a)))
            (rewrite '("a" ("a")))))
  (a)
  ("a"))

(deftest (rewrite dotted)
  (with-rewrite-rules (((?x ?y . ?z) (?y)))
    (values (rewrite '(a))
            (rewrite '(a b))
            (rewrite '(a b c))))
  (a)
  (b)
  (b))

(deftest (rewrite nested)
  (with-rewrite-rules (((a b) (c)))
    (rewrite '(a (b (a b)))))
  (a (b (c))))

(defparameter *test-cycle*
  '(a #1=(b (a b) #1#) #1#)
  "A circular term for the (REWRITE CYCLE) test.")

(deftest (rewrite cycle)
  (with-rewrite-rules (((a b) (c)))
    ;; We print the result to a string because RT compares
    ;; test results with EQUALP, which can't handle cycles.
    ;; And we keep the term in a special so that RT doesn't
    ;; throw the printer into a loop if the test fails.
    (let ((*package* (find-package :krunch))
          (*print-case* :downcase)
          (*print-circle* t)
          (*print-length* nil)
          (*print-level* nil)
          (*print-pretty* nil))
      (prin1-to-string (rewrite *test-cycle*))))
  "(a #1=(b (c) #1#) #1#)")

(deftest (rewrite plist)
  (with-rewrite-rules (((:foo bar . ?rest) (:foo baz . ?rest)))
    (values (rewrite '(:foo bar))
            (rewrite '(:foo bar :bar baz))
            (rewrite '(:bar baz :foo bar))))
  (:foo baz)
  (:foo baz :bar baz)
  (:foo baz :bar baz))

(deftest (rewrite plist and indicators)
  (with-rewrite-rules ((:foo :bar)
                       ((:foo foo) foo))
    (values (rewrite '(:foo foo))
            (rewrite '(:foo bar :bar baz))))
  foo
  (:bar bar :bar baz))

(deftest (rewrite plist default)
  (with-rewrite-rules (((:foo foo :bar nil . ?rest)
                        (:foo foo :bar bar . ?rest)))
    (values (rewrite '(:foo foo :bar quux))
            (rewrite '(:foo foo :baz baz))))
  (:foo foo :bar quux)
  (:foo foo :bar bar :baz baz))

(deftest (rewrite plist location)
  (with-rewrite-rules (((?x :at ?at . ?rest)
                        (?x :location (at :object ?at) . ?rest)))
    (values (rewrite '(darmok :at nil))
            (rewrite '(darmok :at tanagra))))
  (darmok :location (at :object nil))
  (darmok :location (at :object tanagra)))

(deftest (rewrite regex)
  (with-rewrite-rules ()
    (define-rerule "a(.)c" "c\\1a")
    (values (rewrite "aba")
            (rewrite "abc")
            (rewrite "axc")))
  "aba"
  "cba"
  "cxa")

;;; Co-composition tests.

(deftest (compose ⊤ ⊥)
  (values (eq (compose ⊥ ⊥) ⊥)
          (eq (compose ⊤ ⊥) ⊥)
          (eq (compose ⊥ ⊤) ⊤)
          (eq (compose ⊤ ⊤) ⊤))
  t
  t
  t
  t)

(deftest (compose corules)
  (with-ontology blocks
    (with-corules ((((?x :color ?color) (?x :size ?size))
                    (?x :size ?size :color ?color))
                   (((?x . ?rest) (table . ?table))
                    (?x :location (on :object (table . ?table)) . ?rest)))
      (values (compose '(block :color red) '(block :size big))
              (compose '(block :color red) '(table :size big)))))
  (block :size big :color red)
  (block :location (on :object (table :size big)) :color red))

;;; Generation tests.

(deftest suffixes
  (flet ((morph (word) (list (+ed word) (+ing word) (+s word))))
    (values (morph "fry")
            (morph "guy")
            (morph "kiss")
            (morph "model")
            (morph "quiz")
            (morph "suffix")
            (morph "Survey")
            (morph "TRY")))
  ("fried" "frying" "fries")
  ("guyed" "guying" "guys")
  ("kissed" "kissing" "kisses")
  ("modelled" "modeling" "models")
  ("quizzed" "quizzing" "quizzes")
  ("suffixed" "suffixing" "suffixes")
  ("Surveyed" "Surveying" "Surveys")
  ("TRIED" "TRYING" "TRIES"))

(deftest combine-words
  (combine-words '("do" "not" "is" "not" "will" "not" "let" "us"))
  ("don't" "isn't" "won't" "let's"))

(deftest (say aardvark)
  (with-ontology demo
    (values (say '(aardvark))
            (say '(a aardvark))))
  "aardvark"
  "an aardvark")

(deftest (say aaron)
  (with-ontology demo
    (values (say '(aaron))
            (say '(a aaron))))
  "Aaron"
  "an Aaron")

(deftest (say aaron and peter)
  (with-ontology demo
    (values (say '(both (aaron :and peter)))
            (say '(either (aaron :or peter)))
            (say '(not (aaron :or peter)))
            (say '(aaron :but (not peter)))))
  "both Aaron and Peter"
  "either Aaron or Peter"
  "neither Aaron nor Peter"
  "Aaron but not Peter")

(deftest (say cat)
  (with-ontology demo
    (values (say '(cat))
            (say '(cat :number plural))
            (say '(a cat))
            (say '(some cat))
            (say '(some (cat :number plural)))
            (say '(that cat))
            (say '(that (cat :number plural)))))
  "cat"
  "cats"
  "a cat"
  "some cat"
  "some cats"
  "that cat"
  "those cats")

(deftest (say quick brown fox)
  (with-ontology demo
    (say '(jump :tense present
                :subject (the (fox :mod quick :color brown))
                :over (the (dog :mod lazy)))))
  "the quick brown fox jumps over the lazy dog")

(deftest (say barber shaves self)
  (with-ontology demo
    (say '(shave :tense present
                 :subject !B : (the (barber :gender male))
                 :object !B)))
  "the barber shaves himself")

(deftest (say cat licks self)
  (with-ontology demo
    (values (say '(lick :tense present
                        :subject !C : (the cat)
                        :object !C))
            (say '(lick :tense present
                        :subject !D : (the (cat :gender male))
                        :object !D))
            (say '(lick :tense present
                        :subject !E : (the (cat :gender female))
                        :object !E))))
  "the cat licks itself"
  "the cat licks himself"
  "the cat licks herself")

(deftest (say cats lick themselves)
  (with-ontology demo
    (say '(lick :subject !C : (cat :number plural)
                :object !C)))
  "cats lick themselves")

(deftest (say peter bought self book)
  (with-ontology demo
    (say '(buy :tense past
               :subject !P : (peter :gender male)
               :object (a book)
               :indirect-object !P)))
  "Peter bought himself a book")

(deftest (say peter bought book about simon)
  (with-ontology demo
    (say '(buy :tense past
               :subject peter
               :object (a (book :about simon)))))
  "Peter bought a book about Simon")

(deftest (say peter bought simon book)
  (with-ontology demo
    (say '(buy :tense past
               :subject peter
               :object (a book)
               :indirect-object simon)))
  "Peter bought Simon a book")

(deftest (ask why peter bought simon book)
  (with-ontology demo
    (ask '(buy :tense past
               :subject peter
               :object (a book)
               :indirect-object simon
               :purpose why)))
  "Why did Peter buy Simon a book?")

(deftest (say simon bought book about self)
  (with-ontology demo
    (say '(buy :tense past
               :subject !S : (simon :gender male)
               :object (a (book :about !S)))))
  "Simon bought a book about himself")

(deftest (say simon wants barber to shave self)
  (with-ontology demo
    (say '(want :tense present
                :subject !S : (simon :gender male)
                :object (shave :subject !B : (the (barber :gender male))
                               :object !B))))
  "Simon wants the barber to shave himself")

(deftest (say simon wants barber to shave him)
  (with-ontology demo
    (say '(want :tense present
                :subject !S : (simon :gender male)
                :object (shave :subject !B : (the (barber :gender male))
                               :object !S))))
  "Simon wants the barber to shave him")

(deftest (say simon likes his cat)
  (with-ontology demo
    (say '(like :tense present
                :subject !S : (simon :gender male)
                :object (cat :possessive !S))))
  "Simon likes his cat")

(deftest (say simon likes peter’s cat)
  (with-ontology demo
    (say '(like :tense present
                :subject simon
                :object (cat :possessive peter))))
  "Simon likes Peter's cat")

(deftest (say i bought book and read it)
  (with-ontology demo
    (say '(buy :tense past
               :subject me
               :object !B : (a book)
               :and (read :tense past
                          :subject me
                          :object !B))))
  "I bought a book and I read it")

(deftest (say donkey anaphora)
  (with-ontology demo
    (say '(beat :tense present
                :subject (every (farmer :who (own :tense present
                                                  :object !D : (a donkey))))
                :object !D)))
  "every farmer who owns a donkey beats it")

(deftest (say drink)
  (with-ontology demo
    (values (say '(drink :subject me :tense present))
            (say '(drink :subject me :tense past))
            (say '(drink :subject me :tense present :aspect perfect))
            (say '(drink :subject he :tense present))
            (say '(drink :subject he :tense past))
            (say '(drink :subject he :tense present :aspect perfect))
            (say '(drink :subject they :tense present))
            (say '(drink :subject they :tense past))
            (say '(drink :subject they :tense present :aspect perfect))))
  "I drink"
  "I drank"
  "I've drunk"
  "he drinks"
  "he drank"
  "he has drunk"
  "they drink"
  "they drank"
  "they have drunk")

(deftest (say drink my milk)
  (with-ontology demo
    (values (say '(drink :subject me :object (milk :possessive me)))
            (say '(drink :subject me :object (milk :possessive you)))
            (say '(drink :subject you :object (milk :possessive you)))
            (say '(drink :subject we :object (milk :possessive we)))
            (say '(drink :subject we :object (milk :possessive we)
                         :aspect perfect))))
  "I drink my milk"
  "I drink your milk"
  "you drink your milk"
  "we drink our milk"
  "we have drunk our milk")

(defun say-with-tense&aspect (phrase &key
                              (tense '(present
                                       past
                                       future))
                              (aspect '(neutral
                                        perfect
                                        progressive
                                        perfect+progressive)))
  "Realize a phrase with all possible tense/aspect combinations."
  (values-list (loop for tense in tense
                     append (loop for aspect in aspect
                                  collect (say `(,@phrase :tense ,tense
                                                          :aspect ,aspect))))))

(deftest (say i go home)
  (with-ontology demo
    (say-with-tense&aspect '(go :subject me :object home)))
  "I go home"
  "I've gone home"
  "I'm going home"
  "I've been going home"
  "I went home"
  "I'd gone home"
  "I was going home"
  "I'd been going home"
  "I'll go home"
  "I'll have gone home"
  "I'll be going home"
  "I'll have been going home")

(deftest (say he goes home)
  (with-ontology demo
    (say-with-tense&aspect '(go :subject he :object home)))
  "he goes home"
  "he has gone home"
  "he is going home"
  "he has been going home"
  "he went home"
  "he had gone home"
  "he was going home"
  "he had been going home"
  "he will go home"
  "he will have gone home"
  "he will be going home"
  "he will have been going home")

(deftest (say want milk)
  (with-ontology demo
    (values (say '(want :subject me :object milk))
            (say '(want :subject me :object (some milk)))
            (say '(want :subject me :object (be :comp (some milk))))))
  "I want milk"
  "I want some milk"
  "I want there to be some milk")

(deftest (say want to be home)
  (with-ontology demo
    (say '(want :subject !M : me
                :object (be :subject !M : me
                            :comp home))))
  "I want to be home")

(deftest (say want to go home)
  (with-ontology demo
    (values (say '(want :subject !M : me
                        :object (go :subject !M : me
                                    :object home)))
            (say '(want :subject me
                        :object (go :subject you
                                    :object home)))
            (say '(not (want :subject me
                             :object (go :subject you
                                         :object home))))
            (say '(want :subject me
                        :object (not (go :subject you
                                         :object home))))))
  "I want to go home"
  "I want you to go home"
  "I don't want you to go home"
  "I want you to not go home")

(defun say-with-negation (phrase &rest args)
  "Realize a phrase and its negation."
  (values (apply #'say phrase args)
          (apply #'say `(not ,phrase) args)))

(deftest (say drink milk)
  (with-ontology demo
    (say-with-negation '(drink :subject me :object milk)))
  "I drink milk"
  "I don't drink milk")

(deftest (say drank milk)
  (with-ontology demo
    (say-with-negation '(drink :subject me :object milk :tense past)))
  "I drank milk"
  "I didn't drink milk")

(deftest (say will drink milk)
  (with-ontology demo
    (say-with-negation '(drink :subject me :object milk :tense future)))
  "I'll drink milk"
  "I won't drink milk")

(deftest (say have drunk milk)
  (with-ontology demo
    (say-with-negation '(drink :subject me :object milk :aspect perfect)))
  "I've drunk milk"
  "I've not drunk milk")

(deftest (say cat is on mat)
  (with-ontology demo
    (say-with-negation '(be :subject (the cat)
                            :location (on :object (the mat))
                            :tense present)))
  "the cat is on the mat"
  "the cat isn't on the mat")

(deftest (say cat may be on mat)
  (with-ontology demo
    (say-with-negation '(may (be :subject (the cat)
                                 :location (on :object (the mat))))))
  "the cat may be on the mat"
  "the cat may not be on the mat")

(deftest (say fluffy chases mice)
  (with-ontology demo
    (say-with-negation '(chase :tense present
                               :subject fluffy
                               :object (mouse :number plural))))
  "Fluffy chases mice"
  "Fluffy doesn't chase mice")

(deftest (say fluffy is chasing mouse)
  (with-ontology demo
    (say-with-negation '(chase :tense present
                               :aspect progressive
                               :subject fluffy
                               :object (a (mouse :size little))
                               :in (the basement))))
  "Fluffy is chasing a little mouse in the basement"
  "Fluffy isn't chasing a little mouse in the basement")

(deftest (say fluffy chased mouse)
  (with-ontology demo
    (say-with-tense&aspect '(chase :subject fluffy :object (a mouse))))
  "Fluffy chases a mouse"
  "Fluffy has chased a mouse"
  "Fluffy is chasing a mouse"
  "Fluffy has been chasing a mouse"
  "Fluffy chased a mouse"
  "Fluffy had chased a mouse"
  "Fluffy was chasing a mouse"
  "Fluffy had been chasing a mouse"
  "Fluffy will chase a mouse"
  "Fluffy will have chased a mouse"
  "Fluffy will be chasing a mouse"
  "Fluffy will have been chasing a mouse")

(deftest (say fluffy ate mouse)
  (with-ontology demo
    (say-with-tense&aspect '(eat :subject fluffy :object (a mouse))))
  "Fluffy eats a mouse"
  "Fluffy has eaten a mouse"
  "Fluffy is eating a mouse"
  "Fluffy has been eating a mouse"
  "Fluffy ate a mouse"
  "Fluffy had eaten a mouse"
  "Fluffy was eating a mouse"
  "Fluffy had been eating a mouse"
  "Fluffy will eat a mouse"
  "Fluffy will have eaten a mouse"
  "Fluffy will be eating a mouse"
  "Fluffy will have been eating a mouse")

(defun ask-with-tense (phrase &key (tense '(present past future)))
  "Realize a question with various tenses."
  (values-list
   (loop for tense in tense
         collect (ask `(,@phrase :tense ,tense)))))

(deftest (ask if fluffy is eating)
  (with-ontology demo
    (ask-with-tense '(be :subject fluffy :comp (eat :aspect progressive))))
  "Is Fluffy eating?"
  "Was Fluffy eating?"
  "Will Fluffy be eating?")

(deftest (ask if fluffy is not eating)
  (with-ontology demo
    (ask-with-tense '(not (be :subject fluffy :comp (eat :aspect progressive)))))
  "Isn't Fluffy eating?"
  "Wasn't Fluffy eating?"
  "Won't Fluffy be eating?")

(deftest (ask what fluffy might eat)
  (with-ontology demo
    (ask '(eat :subject fluffy :object what :modal might)))
  "What might Fluffy eat?")

(deftest (ask what fluffy ate)
  (with-ontology demo
    (ask-with-tense '(eat :subject fluffy :object what)))
  "What does Fluffy eat?"
  "What did Fluffy eat?"
  "What will Fluffy eat?")

(deftest (ask when fluffy ate)
  (with-ontology demo
    (ask-with-tense '(eat :subject fluffy :time when)))
  "When does Fluffy eat?"
  "When did Fluffy eat?"
  "When will Fluffy eat?")

(deftest (ask when fluffy will be eating)
  (with-ontology demo
    (ask '(be :subject fluffy
              :comp (eat :aspect progressive)
              :time when
              :tense future)))
  "When will Fluffy be eating?")

(deftest (ask which mouse fluffy ate)
  (with-ontology demo
    (ask '(eat :subject fluffy
               :object (mouse :determiner which)
               :tense past)))
  "Which mouse did Fluffy eat?")

(defun say-with-passive (phrase)
  (values-list (loop for voice in '(active passive)
                     collect (say `(,@phrase :voice ,voice)))))

(deftest (say mouse was eaten by fluffy)
  (with-ontology demo
    (say-with-passive '(eat :subject fluffy :object (a mouse) :tense past)))
  "Fluffy ate a mouse"
  "a mouse was eaten by Fluffy")

(deftest (say they were impressed by him)
  (with-ontology demo
    (say-with-passive '(impress :subject he
                                :object they
                                :tense past)))
  "he impressed them"
  "they were impressed by him")

(deftest (say they were impressed by his manner)
  (with-ontology demo
    (say-with-passive '(impress :subject (manner :possessive he)
                                :object they
                                :tense past)))
  "his manner impressed them"
  "they were impressed by his manner")

(deftest (say his friends laughed at him)
  (with-ontology demo
    (say-with-passive '(laugh :subject (friend :possessive he :number plural)
                              :at he
                              :tense past)))
  "his friends laughed at him"
  "he was laughed at by his friends")

(deftest (say it is raining)
  (with-ontology demo
    (say '(be :comp (rain :aspect progressive)
              :tense present)))
  "it's raining")

(deftest (say little flower grew)
  (with-ontology demo
    (values (say '(grow :subject (a (flower :size little)) :tense past))
            (say '(grow :comp (a (flower :size little)) :tense past))))
  "a little flower grew"
  "there grew a little flower")

;; Blocks.

(deftest (say big red block)
  (with-ontology blocks
    (values (say '(block :size big :color red))
            (say '(block :size big :color red :number plural))))
  "big red block"
  "big red blocks")

(deftest (say block on table)
  (with-ontology blocks
    (say '(block :location (on :object (the table)))))
  "block on the table")

(deftest (say big red block that is on table)
  (with-ontology blocks
    (values (say '(the (block :size big :color red
                              :that (be :on (the table)
                                        :tense present))))
            (say '(the (block :size big :color red
                              :that (be :on (the table)
                                        :tense past))))
            (say '(the (block :size big :color red
                              :which (be :on (the table)
                                         :tense future))))
            (say '(the (block :size big :color red
                              :which (be :on (the table)
                                         :modal shall
                                         :tense future))))))
  "the big red block that is on the table"
  "the big red block that was on the table"
  "the big red block which will be on the table"
  "the big red block which shall be on the table")

(deftest (say block name)
  (with-ontology blocks
    (values (say '(block :name "B1"))
            (say '(block :name "the Adidas block"))))
  "B1"
  "the Adidas block")

(deftest (say block is red)
  (with-ontology blocks
    (values (say '(be :subject (the block)
                      :comp red
                      :tense present))
            (say '(be :subject (the block)
                      :comp red
                      :tense past))
            (say '(be :subject (the (block :number plural))
                      :comp red
                      :tense present))
            (say '(be :subject (the (block :number plural))
                      :comp red
                      :tense past))))
  "the block is red"
  "the block was red"
  "the blocks are red"
  "the blocks were red")

(deftest (ask if block is red)
  (with-ontology blocks
    (ask '(be :subject (the block)
              :comp red
              :tense present)))
  "Is the block red?")

(deftest (say yes the block is red)
  (with-ontology blocks
    (sentence '(be :subject (the block)
                   :comp red
                   :tense present
                   :response yes)))
  "Yes, the block is red.")

(deftest (say no the block is not red)
  (with-ontology blocks
    (sentence '(be :subject (the block)
                   :comp red
                   :tense present
                   :response no)))
  "No, the block isn't red.")

(deftest (ask if block is ever red)
  (with-ontology blocks
    (ask '(be :subject (the block)
              :comp (red :qualifier ever)
              :tense present)))
  "Is the block ever red?")

(deftest (say no the block is never red)
  (with-ontology blocks
    (sentence '(be :subject (the block)
                   :comp red
                   :tense present
                   :response no
                   :negation never)))
  "No, the block is never red.")

(deftest (ask what never)
  (with-ontology blocks
    (ask '(t :subject what :negation never)))
  "What never?")

(deftest (say no never)
  (with-ontology blocks
    (sentence '(t :response no :negation never)))
  "No, never.")

(deftest (ask if there is a big red block)
  (with-ontology blocks
    (ask '(be :comp (a (block :size big :color red))
              :tense present)))
  "Is there a big red block?")

(deftest (say there is a big red block)
  (with-ontology blocks
    (sentence '(be :comp (a (block :size big :color red))
                   :tense present)))
  "There is a big red block.")

(deftest (ask if there are any big red blocks)
  (with-ontology blocks
    (ask '(be :comp (any (block :size big :color red :number plural))
              :tense present)))
  "Are there any big red blocks?")

(deftest (say there are some big red blocks)
  (with-ontology blocks
    (sentence '(be :comp (some (block :size big :color red :number plural))
                   :tense present)))
  "There are some big red blocks.")

(deftest (say pick up big red block)
  (with-ontology blocks
    (sentence '(pick-up :object (a (block :size big :color red)))))
  "Pick up a big red block.")

(deftest (say now put it down)
  (with-ontology blocks
    (sentence '(put :object it :location down :qualifier now)))
  "Now put it down.")

(deftest (ask which block)
  (with-ontology blocks
    (ask '(t :subject (which block))))
  "Which block?")

(deftest (ask where to put it)
  (with-ontology blocks
    (ask '(where (put :subject me :object it :modal should))))
  "Where should I put it?")

(deftest (ask how to put it down)
  (with-ontology blocks
    (ask '(how (put :subject me :object it :location down :modal shall))))
  "How shall I put it down?")

(deftest (ask put it down gently)
  (with-ontology blocks
    (ask '(put :subject me
               :object it
               :location down
               :modal shall
               :manner gentle)))
  "Shall I put it down gently?")

(deftest (say i will put it down gently now)
  (with-ontology blocks
    (sentence '(put :subject me
                    :object it
                    :location down
                    :modal will
                    :manner gentle
                    :time now)))
  "I'll put it down gently now.")

(deftest (say move block from shelf to table)
  (with-ontology blocks
    (sentence '(move :object (a block)
                     :from (the shelf)
                     :to (the table))))
  "Move a block from the shelf to the table.")

(deftest (say add another one)
  (with-ontology blocks
    (sentence '(add :object (another one))))
  "Add another one.")

(deftest (say push them together)
  (with-ontology blocks
    (sentence '(push :object they
                     :location together)))
  "Push them together.")

(deftest (say put red block on table)
  (with-ontology blocks
    (sentence '(put :object (a (block :color red))
                    :location (on :object (the table)))))
  "Put a red block on the table.")

(deftest (say put another block next to it)
  (with-ontology blocks
    (sentence '(put :object (another block)
                    :location (next-to :object it))))
  "Put another block next to it.")

(deftest (say now add red block next to that block)
  (with-ontology blocks
    (sentence '(add :object (a (block :color red))
                    :location (next-to :object (that block))
                    :qualifier now)))
  "Now add a red block next to that block.")

(deftest (say now put green block on first block)
  (with-ontology blocks
    (sentence '(put :object (a (block :color green))
                    :location (on-top :of (the (block :order first)))
                    :qualifier now)))
  "Now put a green block on top of the first block.")

(deftest (say put red block on bottom middle green block)
  (with-ontology blocks
    (sentence '(put :object (a (block :color red))
                    :location (on :object (the (block :side bottom
                                                      :mod middle
                                                      :color green))))))
  "Put a red block on the bottom middle green block.")

(deftest (say make row of two green blocks)
  (with-ontology blocks
    (sentence '(make :object (a (row :of (block :quantity 2
                                                :color green
                                                :number plural))))))
  "Make a row of two green blocks.")

(deftest (say put red block at end)
  (with-ontology blocks
    (sentence '(put :object (a (block :color red))
                    :at (the end))))
  "Put a red block at the end.")

(deftest (say put another at end of row on left)
  (with-ontology blocks
    (sentence '(put :object (another (block :color green))
                    :on (the (block :color green
                                    :at (the (end :of (the row)
                                                  :on (the left))))))))
  "Put another green block on the green block at the end of the row on the left.")

(deftest (say now put block on it)
  (with-ontology blocks
    (sentence '(put :object (a block)
                    :location (on-top :of it)
                    :qualifier now)))
  "Now put a block on top of it.")

(deftest (say top block should be red)
  (with-ontology blocks
    (sentence '(should (be :subject (the (block :side top-of))
                           :comp red))))
  "The top block should be red.")

(deftest (say put row on table)
  (with-ontology blocks
    (sentence '(put :object (a (row :of (block :quantity 3
                                               :color blue
                                               :number plural)))
                    :on (the table))))
  "Put a row of three blue blocks on the table.")

(deftest (say put stack on table)
  (with-ontology blocks
    (sentence '(put :object (a (stack :of (block :quantity 2
                                                 :color red
                                                 :number plural)))
                    :on (the table))))
  "Put a stack of two red blocks on the table.")

(deftest (say let us build three step staircase)
  (with-ontology blocks
    (sentence
     '(let-us :object (build :object (a (staircase :quantity 3 :mod step))))))
  "Let us build a three step staircase.")

(deftest (say not enough blocks)
  (with-ontology blocks
    (sentence '(not (have :subject we
                          :object (enough (block :number plural))
                          :tense present))))
  "We don't have enough blocks.")

;; Biology.

(deftest (say mek phosphorylates erk)
  (with-ontology biology
    (say-with-passive '(phosphorylate :subject mek :object erk :tense present)))
  "MEK phosphorylates ERK"
  "ERK is phosphorylated by MEK")

(deftest (say erk that mek phosphorylated)
  (with-ontology biology
    (say '(erk :that (phosphorylate :subject mek :tense past))))
  "ERK that MEK phosphorylated")

(deftest (say braf that is phosphorylated by mek)
  (with-ontology biology
    (say '(braf :that (be :comp (phosphorylate :by mek)
                          :tense present))))
  "BRAF that is phosphorylated by MEK")

(deftest (say braf that is phosphorylated on serine and tyrosine)
  (with-ontology biology
    (say '(braf :that (be :comp (phosphorylate :on (serine :and tyrosine))
                          :tense present))))
  "BRAF that is phosphorylated on serine and tyrosine")

(deftest (say amount of phosphorylated braf)
  (with-ontology biology
    (say '(amount :of (braf :that (be :comp phosphorylate :tense present)))))
  "amount of BRAF that is phosphorylated")

(deftest (say concentration braf-nras complex)
  (with-ontology biology
    (say '(concentration :of (complex :mod (braf #\- nras)))))
  "concentration of BRAF-NRAS complex")

(deftest (ask if complex is sustained)
  (with-ontology biology
    (ask '(sustain :object (the (concentration :of (complex :mod (braf #\- nras))))
                   :after (hour :quantity 2 :number plural)
                   :tense present
                   :voice passive)))
  "Is the concentration of BRAF-NRAS complex sustained after two hours?")

(deftest (say egfr binds egf)
  (with-ontology biology
    (sentence '(bind :subject (the (egfr :mod receptor-tyrosine-kinase))
                     :object (the (egf :mod growth-factor-ligand))
                     :tense present)))
  "The receptor tyrosine kinase EGFR binds the growth factor ligand EGF.")

(deftest (say total amount of braf)
  (with-ontology biology
    (values (sentence '(double :object (the (amount :mod total :of braf))))
            (sentence '(set :object (the (amount :mod total :of braf))
                            :to zero))))
  "Double the total amount of BRAF."
  "Set the total amount of BRAF to zero.")

(deftest (say assume no nras in system)
  (with-ontology biology
    (sentence '(assume :object (not (exist :subject nras
                                           :in (the system)
                                           :tense present)))))
  "Assume NRAS doesn't exist in the system.")

(deftest (say decrease binding rate)
  (with-ontology biology
    (sentence '(decrease :object (the (rate :mod bind :of (nras :and braf))))))
  "Decrease the binding rate of NRAS and BRAF.")

(deftest (say quite certain it is transient in time)
  (with-ontology biology
    (sentence '(be :subject me
                   :comp (certain :qualifier quite
                                  :object (be :subject it
                                              :comp (transient :in time)
                                              :tense present))
                   :tense present)))
  "I'm quite certain it's transient in time.")

(deftest (ask what genes srf regulates)
  (with-ontology biology
    (ask '(regulate :subject srf
                    :object (gene :determiner what :number plural)
                    :tense present)))
  "What genes does SRF regulate?")

(deftest (ask which pathways involve srf)
  (with-ontology biology
    (ask '(involve :subject (pathway :determiner which :number plural)
                   :object srf)))
  "Which pathways involve SRF?")

(deftest (say find drug that targets kras)
  (with-ontology biology
    (values (sentence '(find :object (a (drug :that (target :object kras
                                                            :tense present)))))
            (sentence '(could (not (find :subject me
                                         :object (a (drug :that (target :object kras
                                                                        :tense present)))))))
            (sentence '(could (not (find :subject me
                                         :object (any (drug :that (target :object kras)
                                                            :number plural))))))))
  "Find a drug that targets KRAS."
  "I couldn't find a drug that targets KRAS."
  "I couldn't find any drugs that target KRAS.")

(deftest (say find drugs that target kras)
  (with-ontology biology
    (sentence '(find :object (drug :that (target :object kras :tense present)
                                   :number plural))))
  "Find drugs that target KRAS.")

(deftest (say 88% of patients)
  (with-ontology biology
    (sentence '(have :subject (patient :part "88%" :number plural)
                     :object (a (mutation :in !KRAS : kras
                                          :that (make :object bio-active
                                                      :indirect-object !KRAS : kras
                                                      :tense present))))))
  "88% of patients have a mutation in KRAS that makes it active.")

(deftest (say mutated kras can cause lung cancer)
  (with-ontology biology
    (sentence '(cause :subject (kras :mod (mutate :tense past))
                      :object lung-cancer
                      :modal can)))
  "Mutated KRAS can cause lung cancer.")

(deftest (say mutations in braf)
  (with-ontology biology
    (sentence '(associate :subject (development :mod cancer)
                          :with (mutation :in (the (gene :mod braf))
                                          :number plural)
                          :tense present
                          :voice passive)))
  "Mutations in the BRAF gene are associated with cancer development.")

(deftest (say effect of raf caax on aspp2)
  (with-ontology biology
    (sentence '(compare :subject (that :of (hras :site !V12 : (location :name "V12")
                                                 :or (kras :site !V12)))
                        :to (the (effect :of (caax :mod raf)
                                         :on (aspp2 :and p53)))
                        :tense past
                        :voice passive)))
  "The effect of Raf CAAX on ASPP2 and p53 was compared to that of HRAS V12 or KRAS V12.")

(deftest (say want to find treatment for pancreatic cancer)
  (with-ontology biology
    (sentence '(want :subject me
                     :object (find :object (a (treatment :for pancreatic-cancer))))))
  "I want to find a treatment for pancreatic cancer.")

(deftest (ask what drug i could use)
  (with-ontology biology
    (ask '(use :subject me
               :object (drug :determiner what)
               :modal could)))
  "What drug could I use?")

(deftest (ask what drug should i use for pancreatic cancer)
  (with-ontology biology
    (ask '(use :subject me
               :object (drug :determiner what)
               :for pancreatic-cancer
               :modal should)))
  "What drug should I use for pancreatic cancer?")

(deftest (ask what proteins might lead to pancreatic cancer)
  (with-ontology biology
    (ask '(lead :object (protein :determiner what :number plural)
                :to (the (development :of pancreatic-cancer))
                :modal might)))
  "What proteins might lead to the development of pancreatic cancer?")

(deftest (ask what proteins plx-4720 targets)
  (with-ontology biology
    (ask '(target :subject (molecule :name "PLX-4720")
                  :object (protein :determiner what :number plural)
                  :tense present)))
  "What proteins does PLX-4720 target?")

(deftest (ask what are targets of plx-4720)
  (with-ontology biology
    (ask '(be :subject (what :number plural)
              :comp (the (target :of (molecule :name "PLX-4720")
                                 :number plural))
              :tense present)))
  "What are the targets of PLX-4720?")

(deftest (ask what is target of selumetinib)
  (with-ontology biology
    (ask '(be :subject what
              :comp (the (target :of (drug :name "Selumetinib")))
              :tense present)))
  "What is the target of Selumetinib?")

(deftest (ask what proteins vemurafenib targets)
  (with-ontology biology
    (ask '(target :subject (drug :name "Vemurafenib")
                  :object (protein :determiner what :number plural)
                  :tense present)))
  "What proteins does Vemurafenib target?")

(deftest (ask what selumetinib’s targets are)
  (with-ontology biology
    (ask '(be :subject (target :possessive (drug :name "Selumetinib")
                               :number plural)
              :object what
              :tense present)))
  "What are Selumetinib's targets?")

(deftest (ask if selumetinib inhibits map2k1)
  (with-ontology biology
    (ask '(inhibit :subject (drug :name "Selumetinib")
                   :object (protein :name "MAP2K1")
                   :tense present)))
  "Does Selumetinib inhibit MAP2K1?")

(deftest (ask what drugs inhibit map2k1)
  (with-ontology biology
    (ask '(inhibit :subject (drug :determiner what :number plural)
                   :object (protein :name "MAP2K1"))))
  "What drugs inhibit MAP2K1?")

(deftest (ask if there are any drugs targeting kras)
  (with-ontology biology
    (ask '(be :comp (target :subject (any (drug :number plural))
                            :object kras
                            :aspect progressive)
              :tense present)))
  "Are there any drugs targeting KRAS?")

(deftest (ask what drug targets braf)
  (with-ontology biology
    (ask-with-tense '(target :subject (drug :determiner what)
                             :object braf)))
  "What drug targets BRAF?"
  "What drug targeted BRAF?"
  "What drug will target BRAF?")

(deftest (ask what drugs target braf)
  (with-ontology biology
    (ask-with-tense '(target :subject (drug :determiner what
                                            :number plural)
                             :object braf)))
  "What drugs target BRAF?"
  "What drugs targeted BRAF?"
  "What drugs will target BRAF?")

(deftest (ask if there are any drugs inhibiting mek)
  (with-ontology biology
    (ask '(be :comp (inhibit :subject (any (drug :number plural))
                             :object mek
                             :aspect progressive)
              :tense present)))
  "Are there any drugs inhibiting MEK?")

(deftest (ask if there are drugs that treat pancreatic cancer)
  (with-ontology biology
    (ask '(be :comp (drug :that (treat :object pancreatic-cancer)
                          :number plural)
              :tense present)))
  "Are there drugs that treat pancreatic cancer?")

(deftest (ask if vemurafenib inhibits braf)
  (with-ontology biology
    (ask '(inhibit :subject (drug :name "Vemurafenib")
                   :object braf
                   :tense present)))
  "Does Vemurafenib inhibit BRAF?")

(deftest (ask if selumetinib is inhibitor of mek1)
  (with-ontology biology
    (ask '(be :subject (drug :name "Selumetinib")
              :comp (a (inhibitor :of (mek :name "MEK1")))
              :tense present)))
  "Is Selumetinib an inhibitor of MEK1?")

(deftest (ask if mek2 is inhibited by Selumetinib)
  (with-ontology biology
    (ask '(inhibit :subject (drug :name "Selumetinib")
                   :object (mek :name "MEK2")
                   :tense present
                   :voice passive)))
  "Is MEK2 inhibited by Selumetinib?")

(deftest (ask if vemurafenib is an inhibitor for braf)
  (with-ontology biology
    (ask '(be :subject (drug :name "Vemurafenib")
              :comp (a (inhibitor :for braf))
              :tense present)))
  "Is Vemurafenib an inhibitor for BRAF?")

(deftest (ask if vemurafenib targets braf)
  (with-ontology biology
    (ask '(target :subject (drug :name "Vemurafenib")
                  :object braf
                  :tense present)))
  "Does Vemurafenib target BRAF?")

(deftest (ask if there are any drugs for braf)
  (with-ontology biology
    (ask '(be :comp (any (drug :number plural :for braf))
              :tense present)))
  "Are there any drugs for BRAF?")

(deftest (ask if you know any drugs for braf)
  (with-ontology biology
    (ask '(know :subject you
                :object (any (drug :number plural :for braf))
                :tense present)))
  "Do you know any drugs for BRAF?")

(deftest (ask what inhibits braf)
  (with-ontology biology
    (ask '(inhibit :subject what
                   :object braf
                   :tense present)))
  "What inhibits BRAF?")

(deftest (ask if there are any inhibitors for jak1)
  (with-ontology biology
    (ask '(be :comp (any (inhibitor :number plural
                                    :for (protein :name "JAK1")))
              :tense present)))
  "Are there any inhibitors for JAK1?")

(deftest (ask what are some jak1 inhibitors)
  (with-ontology biology
    (ask '(be :subject (what :number plural)
              :comp (some (inhibitor :number plural
                                     :mod (protein :name "JAK1")))
              :tense present)))
  "What are some JAK1 inhibitors?")

(deftest (ask if stat3 is one of the regulators of c-fos)
  (with-ontology biology
    (ask '(be :subject (gene :name "STAT3")
              :comp (regulator :part one
                               :determiner the
                               :number plural
                               :of (the (gene :mod "c-FOS")))
              :tense present)))
  "Is STAT3 one of the regulators of the c-FOS gene?")
