;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; Variables are bound to S-expressions, with special handling for plists.

(declaim (inline plistp wildcardp variablep bind))

(defun plistp (object)
  "Return true if OBJECT is a plist, e.g., (:KEY VALUE ...) where KEY
is any keyword except :* which TRIPS uses in its LF syntax. Does not
check the whole list, just the first two elements."
  (typep object '(cons (and keyword (not (eql :*))) cons)))

(defun wildcardp (object)
  "Return true if OBJECT is the wildcard symbol *"
  (eq object '*))

(defun variablep (object)
  "Return true if OBJECT is a symbol whose name starts with ?"
  (and object (symbolp object) (char= (char (symbol-name object) 0) #\?)))

(defun bind (variable value &optional bindings)
  "Bind VARIABLE to VALUE in the alist BINDINGS."
  (declare (inline acons))
  (acons variable value bindings))

(defun variable-value (variable bindings &optional (errorp t))
  "Return the value of VARIABLE in BINDINGS."
  (let ((binding (assoc variable bindings :test #'eq)))
    (if binding
        (cdr binding)
        (and errorp (error 'unbound-variable :name variable)))))

(defun bind* (pattern value &optional bindings)
  "Augment and return BINDINGS by matching PATTERN (e.g., the LHS of a rule)
against VALUE (e.g., a term). If there is no match, throw BINDINGS to the
NO-MATCH tag."
  (declare (optimize speed))
  (flet ((no-match () (throw 'no-match bindings)))
    (cond ((wildcardp pattern) bindings)
          ((variablep pattern)
           (let ((old-value (variable-value pattern bindings nil)))
             (if old-value
                 (if (flet ((deref (x)
                              (loop while (variablep x)
                                    do (setq x (variable-value x bindings))
                                    finally (return x))))
                       (equal (deref value) (deref old-value)))
                     bindings
                     (no-match))
                 (bind pattern value bindings))))
          ((and (null pattern) (plistp value)) bindings) ; allow other keys
          ((and (plistp pattern) (plistp value))
           (bind* (cddr pattern)
                  (sans value (car pattern))
                  (bind* (cadr pattern)
                         (let ((tail (ignore-error type-error
                                       (member (car pattern) value :test #'eq))))
                           (if (or tail (null (cadr pattern)))
                               (cadr tail)
                               (no-match)))
                         bindings)))
          ((and (consp pattern) (consp value))
           (bind* (cdr pattern)
                  (cdr value)
                  (bind* (car pattern) (car value) bindings)))
          ((equal pattern value) bindings)
          (t (no-match)))))

;; Terms may have cycles, so we must take care not to revisit subterms
;; that have already been visited. We must also take care to update shared
;; references properly; i.e., when we rewrite a subterm, we must also
;; rewrite all other subterms that pointed to the original subterm.
;; One way to do this is to smash the subterm in place. Another is to
;; bind a temporary variable and replace all references to the shared
;; subterm with the temporary, and that's what we do here.

(defun make-visited-table (table)
  "Initialize or return a hash table that is the value of a dynamic variable."
  (if (boundp table)
      (symbol-value table)
      (make-hash-table :test #'eq)))

(defmacro with-visited-table ((table) &body body)
  "Bind a special variable to a visited table, and bind local
accessors with the same name."
  (check-type table symbol "a variable name")
  `(let ((,table (make-visited-table ',table)))
     (declare (special ,table) (hash-table ,table))
     (flet ((,table (object)
              (gethash object ,table))
            ((setf ,table) (new-object object)
              (setf (gethash object ,table) new-object)))
       ,@body)))

(defun make-temp ()
  (gensym "?"))

(defun tempp (symbol)
  (and (variablep symbol)
       (not (symbol-package symbol))))

(defgeneric decycle (x &optional bindings)
  (:documentation "Replace cycles in the tree X with temporary variables,
and return the new tree and an alist of bindings for the temporaries.
The result may share structure with X.")
  (:method :around ((x cons) &optional bindings &aux y)
    "Carefully replace shared structure with temporary variable bindings."
    (let ((binding (rassoc x bindings :test #'eq)))
      (if (and binding (tempp (car binding)))
          (values (car binding) bindings)
          (with-visited-table (visited)
            (multiple-value-bind (temp visited-p) (visited x)
              (cond ((and (not temp) visited-p)
                     (let ((temp (make-temp)))
                       (values (setf (visited x) temp)
                               (bind temp x bindings))))
                    (t (setf (visited x) nil
                             (values y bindings) (call-next-method)
                             temp (visited x))
                       (if (eq x y)
                           (values x bindings)
                           (values y (if temp (bind temp y bindings) bindings))))))))))
  (:method ((x cons) &optional bindings &aux a d)
    "Recursively decycle a cons cell, sharing structure when possible."
    (setf (values a bindings) (decycle (car x) bindings)
          (values d bindings) (decycle (cdr x) bindings))
    (values (recons x a d) bindings))
  (:method (x &optional bindings)
    "The default method, a noop."
    (values x bindings)))

(defgeneric recycle (x &optional bindings)
  (:documentation "Destructively replace references in the tree X
to the variables in BINDINGS with their values.")
  (:method ((x cons) &optional bindings)
    (rplaca x (recycle (car x) bindings))
    (rplacd x (recycle (cdr x) bindings)))
  (:method (x &optional bindings)
    (or (cdr (assoc x bindings)) x)))

;;; Rewrite rules transform S-expressions that represent terms.

;; Rules consist of a pair of lists: a left-hand side (LHS) and
;; a right-hand side (RHS). If the LHS matches a term under BIND*
;; then the RHS is applied with the new bindings. Arbitrary trees
;; are permitted on both sides, and plists are handled specially,
;; ignoring order. An explicit null value for a keyword on the LHS
;; means that the key must not appear in the term.

;; We keep a cache of all of the keywords in the LHS for FAIL-FAST:
;; if keys in the LHS don't occur anywhere in a term, then the rule
;; can't possibly match, and we can avoid checking the subterms. A
;; complementary strategy is to check the cars of all the subterms;
;; that works fine, but doesn't make as much difference as checking
;; the keywords.

(defclass rewrite-rule ()
  ((lhs :reader lhs :initarg :lhs)
   (rhs :reader rhs :initarg :rhs)
   (keys :accessor rule-keys :initarg :keys))
  (:default-initargs :lhs nil :rhs nil :keys nil))

(defun keys (tree &optional ignore-null-values)
  "Collect the keywords that occur in TREE,
optionally eliding keys with null values."
  (declare (optimize speed))
  (labels ((keys (tree keys)
             (cond ((atom tree) keys)
                   ((plistp tree)
                    (keys (cddr tree)
                          (if (or (cadr tree) (not ignore-null-values))
                              (keys (cadr tree)
                                    (pushnew (car tree) keys :test #'eq))
                              keys)))
                   ((consp tree)
                    (keys (cdr tree)
                          (keys (car tree) keys))))))
    (keys tree nil)))

(defmethod shared-initialize :after ((rule rewrite-rule) slot-names &key)
  (declare (ignore slot-names))
  (setf (rule-keys rule) (keys (lhs rule) t)))

(defmethod print-object ((rule rewrite-rule) stream)
  (print-unreadable-object (rule stream :type nil :identity nil)
    (format stream "~s → ~s" (lhs rule) (rhs rule))))

(defgeneric rule-equal (x y)
  (:method ((x rewrite-rule) (y rewrite-rule))
    (and (equal (lhs x) (lhs y))
         (equal (rhs x) (rhs y)))))

(defgeneric rule-lessp (x y)
  (:method ((x rewrite-rule) (y rewrite-rule))
    (loop for (x . xs) on (ensure-list (lhs x))
          and (y . ys) on (ensure-list (lhs y))
          thereis (or (and (wildcardp x) (not (wildcardp y)))
                      (and (variablep x) (not (variablep y)))
                      (and (null x) (not (null y)))
                      (and (null xs) (not (null ys)))
                      (and (atom xs) (not (atom ys)))))))

(defun sort-rules (rules)
  (sort (copy-list rules) (complement #'rule-lessp)))

(defparameter *null-rule* (make-instance 'rewrite-rule))
(defgeneric parse-rule (rule-exp &optional rule-class &rest initargs)
  (:method (rule-exp &optional rule-class &rest initargs)
    (declare (ignore rule-exp rule-class initargs))
    (error "Rule syntax is (LHS RHS)."))
  (:method ((rule-exp null) &optional rule-class &rest initargs)
    (declare (ignore rule-class initargs))
    *null-rule*)
  (:method ((rule-exp cons) &optional (rule-class 'rewrite-rule) &rest initargs)
    (destructuring-bind (lhs rhs) rule-exp
      (apply #'make-instance rule-class :lhs lhs :rhs rhs initargs))))

(defparameter *rules* '())
(defmacro with-rewrite-rules ((&rest rules) &body body)
  `(let ((*rules* (mapcar #'parse-rule ',rules)))
     ,@body))

;; Emacs: (put 'define-rule 'lisp-indent-function 0)
(defmacro define-rule (&rest rule
                       &aux (rule (case (length rule)
                                    (1 (car rule))
                                    (t (parse-rule rule)))))
  "Define a rewrite rule.
The syntax is flexible to accomidate two kinds of RULE lists:
lists of the form (LHS RHS), and instances of REWRITE-RULE.
The latter will never be specified directly, but will come
from other rule-defining macros that call down to this one.
This is a macro only to avoid quoting the arguments."
  (check-type rule rewrite-rule)
  `(progn
     (pushnew ,rule *rules* :test #'rule-equal)
     ,rule))

(defgeneric apply-rule (rule term bindings)
  (:documentation
   "Use RULE to rewrite TERM, and return the new value and augmented BINDINGS.")
  (:method ((rule rewrite-rule) term bindings)
    "Augment BINDINGS by matching the LHS of RULE to TERM, then apply the RHS."
    (catch 'no-match
      (return-from apply-rule
        (apply-rule (rhs rule) term (bind* (lhs rule) term bindings))))

    ;; We only get here if the LHS of RULE did not match TERM.
    ;; So either stop, or recursively try applying RULE to the
    ;; subterms of TERM.
    (values (if (or (atom term)
                    (and (consp (lhs rule))
                         (loop for (car . cdr) on term
                               always (atom car))))
                term
                (flet ((apply-rule (rule term &aux value)
                         (multiple-value-setq (value bindings)
                           (apply-rule rule term bindings))))
                  (recons term
                          (apply-rule rule (car term))
                          (apply-rule rule (cdr term)))))
            bindings))
  (:method :around ((rule rewrite-rule) (term cons) bindings)
    "Update BINDINGS for new values."
    (multiple-value-bind (value bindings) (call-next-method rule term bindings)
      (unless (eq value term)
        (let ((binding (rassoc term bindings :test #'eq)))
          (when binding (rplacd binding value))))
      (values value bindings)))
  (:method ((rule cons) (term list) bindings &aux value)
    (values (if (and (plistp rule) (plistp term))
                (list* (car rule)
                       (multiple-value-setq (value bindings)
                         (apply-rule (cadr rule) (getf term (car rule)) bindings))
                       (multiple-value-setq (value bindings)
                         (apply-rule (cddr rule) (sans term (car rule)) bindings)))
                (cons (multiple-value-setq (value bindings)
                        (apply-rule (car rule) (car term) bindings))
                      (multiple-value-setq (value bindings)
                        (apply-rule (cdr rule) (cdr term) bindings))))
            bindings))
  (:method (rule term bindings)
    (cond ((wildcardp rule) (values term bindings))
          ((variablep rule) (values (variable-value rule bindings) bindings))
          (t (values rule bindings)))))

(defun fail-fast (rule term)
  "Use the cached keys in RULE to quickly determine if TERM can't match."
  (and (rule-keys rule)
       (not (subsetp (rule-keys rule) (keys term)))))

(defun rewrite-1 (term &optional (rules *rules*) &aux changed-p)
  (multiple-value-bind (term bindings) (decycle term)
    (loop for rule in rules
          while (or (fail-fast rule term)
                    (equal term (setq term (apply-rule rule term bindings)))
                    (not (setq changed-p t)))
          finally (return (values (recycle term bindings) changed-p)))))

(defun rewrite (term &optional (rules *rules*) &aux changed-p)
  (setq rules (sort-rules rules))
  (loop do (multiple-value-setq (term changed-p) (rewrite-1 term rules))
        while changed-p
        finally (return term)))
