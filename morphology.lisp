;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

(in-package :krunch)

;;; English morphology.

(defvar *morphology* (make-hash-table :test #'equal)
  "Irregular word morphologies. Keys are words,
values are plists of the form (SUFFIX FORM ..).")

(defun morphology (word &key suffix (key #'string) (test #'string=))
  "Look up irregular morphology for WORD."
  (let ((morphology (gethash word *morphology*)))
    (if suffix
        (loop for (suffix* form) on morphology by #'cddr
              when (funcall test suffix (funcall key suffix*))
              return form)
        morphology)))

(defun (setf morphology) (morphology word)
  "Set irregular morphology for WORD."
  (check-type morphology list)
  (check-type word string)
  (setf (gethash word *morphology*) (mapcar #'string-downcase morphology)))

(defmacro define-morphology (word &rest morphology)
  "Define irregular morphology for WORD."
  `(setf (morphology ,word) ',morphology))

(define-morphology "be" :ed "was" :en "been" :ing "being" :s "is")
(define-morphology "do" :ed "did" :en "done" :s "do")
(define-morphology "have" :ed "had" :en "have" :s "have")

(defvar *vowels* "aeiou")
(defvar *double-final-letters* "bcdfgklmnprstz"
  "Letters to double by default if they occur at the end of a word.")

(defun maybe-double-final-letter (word &key (double *double-final-letters*) &aux
                                  (n (length word))
                                  (final (char word (1- n))))
  (declare (type string word))
  (if (and double
           (> n 2) ; long enough to double
           (char/= final (char word (- n 2))) ; not already doubled
           (find final double :test #'char=)) ; doublable
      (concatenate 'string word (string final))
      word))

(defmacro with-lower-case (string &rest body)
  "If the variable STRING is bound to an all uppercase string,
execute BODY with it bound to the corresponding lowercase string
and convert the result back to uppercase."
  (check-type string symbol "a variable")
  (let ((upper-case-p (copy-symbol 'upper-case-p))
        (result (copy-symbol 'result)))
    `(let* ((,upper-case-p (loop for char across ,string
                                 always (if (both-case-p char)
                                            (upper-case-p char)
                                            t)))
            (,string (if ,upper-case-p
                         (string-downcase ,string)
                         ,string))
            (,result (progn ,@body)))
       (declare (type string ,string ,result))
       (if ,upper-case-p
           (string-upcase ,result)
           ,result))))

(defun suffix (word suffix &rest irregulars &key
               (double *double-final-letters*)
               (except-after-vowel '("y"))
               (vowels *vowels*) &allow-other-keys)
  "Attach SUFFIX to WORD with word- and suffix-specific special cases."
  (declare (type string word suffix))
  (with-lower-case word
    (or (morphology word :suffix suffix)
        (not (setq word (maybe-double-final-letter word :double double)))
        (loop with n = (length word)
              for (final suffix) on irregulars by #'cddr
              as m = (etypecase final
                       (keyword 0) ; a keyword arg, not an irregular suffix
                       (character (setq suffix (string suffix)) 1)
                       (string (length final)))
              when (and (> n m) (string-equal word final :start1 (- n m)))
              unless (and (find final except-after-vowel :test #'string=)
                          (find (char word (- n m 1)) vowels :test #'char=))
              return (concatenate 'string (subseq word 0 (- n m)) suffix))
        (concatenate 'string word suffix))))

(defun +ed (word) (suffix word "ed" #\c "ked" #\e "ed" #\y "ied" :double "lz"))
(defun +en (word) (or (morphology word :suffix "en") (+ed word)))
(defun +ing (word) (suffix word "ing" #\e "ing" "ee" "eing" :double "z"))
(defun +ly (word) (suffix word "ly" "le" "ly" :double nil))
(defun +s (word) (suffix word "s" #\s "ses" #\x "xes" #\y "ies" #\z "zes" :double "z"))

(defgeneric adjective (word &key)
  (:documentation "Morphologically specialize an adjective.")
  (:method ((word string) &key gerundive)
    (if gerundive
        (+ing word)
        word)))

(defgeneric adverb (word &key)
  (:documentation "Morphologically specialize an adverb.")
  (:method ((word string) &key)
    (+ly word)))

(defgeneric prep (word &key)
  (:documentation "Morphologically specialize a preposition.")
  (:method ((word string) &key)
    word))

(defgeneric noun (word &key number)
  (:documentation "Morphologically specialize a noun.")
  (:method ((word string) &key case (number :singular))
    (case case
      (:genitive (suffix word "'s" :double nil))
      (otherwise
       (ecase number
         ((:singular nil) word)
         (:plural (+s word)))))))

(defvar *irregulars* (make-hash-table :test #'equal)
  "Irregularly inflected verbs. Keys are words,
values are lists of the form (IRREGULAR FEATURE VALUE ..).
The value * is taken as a wildcard that matches anything.")

(defun irregular (verb &key adverb tense person number)
  "Return the first irregular verb form that matches the given features."
  (let ((irregulars (gethash verb *irregulars*)))
    (loop for (irregular . features) in irregulars
          when (and adverb (getf features :adverb))
            return irregular
          when (and (or tense person number)
                    (destructuring-bind (&key
                                         ((:tense irregular-tense))
                                         ((:person irregular-person))
                                         ((:number irregular-number))
                                         &allow-other-keys)
                        features
                      (flet ((eq* (x y) (or (eq x '*) (eq y '*) (eq x y))))
                        (and (eq* tense irregular-tense)
                             (eq* person irregular-person)
                             (eq* number irregular-number)))))
            return irregular)))

(defun (setf irregular) (irregulars verb)
  "Set irregular verb forms."
  (check-type irregulars list)
  (check-type verb string)
  (assert (notany (lambda (irregular)
                    (check-type irregular (cons string list)))
                 irregulars)
          (irregulars)
          "Invalid irregulars for verb ~s." verb)
  (setf (gethash verb *irregulars*) irregulars))

(defmacro define-irregular-verb (verb &rest irregulars)
  "Define VERB as irregular."
  `(setf (irregular ,verb) ',irregulars))

(define-irregular-verb "be"
  ("am" :tense :present :person :first :number :singular)
  ("are" :tense :present :person :second :number :singular)
  ("are" :tense :present :person * :number :plural)
  ("were" :tense :past :person :second :number :singular)
  ("were" :tense :past :person :third :number :plural))

(define-irregular-verb "do"
  ("does" :tense :present :person :third :number :singular))

(define-irregular-verb "have"
  ("has" :tense :present :person :third :number :singular))

(defgeneric verb (word &key &allow-other-keys)
  (:documentation "Morphologically specialize a verb.")
  (:method ((word string) &key number person tense aspect voice)
    (or (irregular word :number number :person person :tense tense)
        (ecase voice
          (:passive (+en word))
          ((:active nil)
           (ecase aspect
             ((:progressive :perfect+progressive) (+ing word))
             (:perfect (+en word))
             ((:neutral nil)
              (ecase tense
                (:past (+ed word))
                (:present (case number
                            (:plural word)
                            (otherwise (+s word))))
                ((:future nil) word)))))))))

(defun irregular-adverb (word)
  (irregular word :adverb t))

(defun (setf irregular-adverb) (value adverb)
  (setf (irregular adverb) `((,adverb :adverb ,value))))

(defmacro define-irregular-adverb (adverb)
  `(setf (irregular-adverb ,adverb) t))

(define-irregular-adverb "how")
(define-irregular-adverb "now")
(define-irregular-adverb "quite")
(define-irregular-adverb "soon")

(defmethod adverb :around ((word string) &key)
  (or (irregular-adverb word)
      (call-next-method)))

;;; Combinations & contractions.

(defvar *combinations* (make-hash-table :test #'equal)
  "Combinations of words. Keys are lists of words, values are words.")

(defun combination (&rest words)
  "Look up a combination of words."
  (gethash words *combinations*))

(defun (setf combination) (combination &rest words)
  "Set a combination of words."
  (check-type combination string)
  (setf (gethash words *combinations*) combination))

(defmacro define-combination (combination &rest words)
  "Define a combination of words."
  `(setf (combination ,@words) ,combination))

(defun combine-words (words)
  "Combine adjacent words."
  (nreverse ; combine right-to-left
   (loop with combination = nil
         for (word prev) on (reverse words)
         if combination
           do (setq combination nil)
         else if (and (string-equal prev "a")
                      (find (char word 0) "aeiou" :test #'char-equal))
           collect word and collect (setq combination "an")
         else if (setq combination (combination prev word))
           collect combination
         else
           collect word)))

(define-combination "can't" "can" "not")
(define-combination "couldn't" "could" "not")
(define-combination "didn't" "did" "not")
(define-combination "doesn't" "does" "not")
(define-combination "don't" "do" "not")
(define-combination "I'd" "I" "had")
(define-combination "I'd" "I" "would")
(define-combination "I'll" "I" "will")
(define-combination "I'm" "I" "am")
(define-combination "I've" "I" "have")
(define-combination "isn't" "is" "not")
(define-combination "it's" "it" "is")
(define-combination "let's" "let" "us")
(define-combination "shouldn't" "should" "not")
(define-combination "wasn't" "was" "not")
(define-combination "won't" "will" "not")
(define-combination "wouldn't" "would" "not")

;;; Pronouns.

(deftype pronoun-case ()
  '(member :nominative :objective :dative :genitive :reflexive))

(defstruct pronoun person number gender case)

(defmethod print-object ((object pronoun) stream)
  (print-unreadable-object (object stream :type t :identity nil)
    (prin1 (nth-value 1 (get-properties (pronoun-case object)
                                        '(:nominative :objective)))
           stream)))

(defmethod describe-object ((object pronoun) stream)
  (format stream "~&~s is a ~(~a-person ~a ~a~) pronoun with case~
                  ~%~:<~@{~s ~s~^~:@_~}~:>.~%"
          object
          (pronoun-person object)
          (pronoun-number object)
          (pronoun-gender object)
          (pronoun-case object)))

(defvar *pronouns* (make-hash-table :test #'equal)
  "Pronouns indexed by (usually) nominative case.")

(defun determiner (word &rest args)
  (and word (or (apply #'pronoun word args) word)))

(defgeneric pronoun (word &key case number errorp)
  (:documentation "Morphologically specialize a pronoun.")
  (:method ((word null) &key case number errorp)
    (declare (ignore case number errorp)))
  (:method ((word string) &rest args)
    (multiple-value-bind (pronoun present-p) (gethash word *pronouns*)
      (and present-p (apply #'pronoun pronoun args))))
  (:method ((pronoun pronoun) &rest args &key case
            (number :singular) (errorp t) &aux
            (word (if case
                      (or (getf (pronoun-case pronoun) case)
                          (and errorp
                               (error "Invalid case ~s for ~s." case pronoun)))
                      pronoun)))
    (cond ((eq (pronoun-number pronoun) number) word)
          ((eq number :plural)
           (let ((plural (morphology word :suffix "s")))
             (if (and plural (string/= word plural))
                 (apply #'pronoun plural args)
                 word)))
          (t word))))

(defgeneric (setf pronoun) (pronoun word)
  (:method ((pronoun pronoun) (word string))
   (setf (gethash word *pronouns*) pronoun)))

(defmacro define-pronoun (word &rest args)
  `(setf (pronoun ,word) (make-pronoun ,@args)))

(define-pronoun "me"
  :person :first
  :number :singular
  :gender :neuter
  :case '(:nominative "I"
          :objective "me"
          :genitive "my"
          :reflexive "myself"))

(define-morphology "I" :s "we")
(define-morphology "me" :s "we")

(define-pronoun "we"
  :person :first
  :number :plural
  :gender :neuter
  :case '(:nominative "we"
          :objective "us"
          :genitive "our"
          :reflexive "ourselves"))

(define-pronoun "you"
  :person :second
  :number :singular
  :gender :neuter
  :case '(:nominative "you"
          :objective "you"
          :genitive "your"
          :reflexive "yourself"))

(define-morphology "you" :s "you")

(define-pronoun "y'all"
  :person :second
  :number :plural
  :gender :neuter
  :case '(:nominative "y'all"
          :objective "y'all"
          :genitive "y'all's"
          :reflexive "y'all's selves"))

(define-pronoun "he"
  :person :third
  :number :singular
  :gender :masculine
  :case '(:nominative "he"
          :objective "him"
          :genitive "his"
          :reflexive "himself"))

(define-pronoun "she"
  :person :third
  :number :singular
  :gender :feminine
  :case '(:nominative "she"
          :objective "her"
          :genitive "her"
          :reflexive "herself"))

(define-morphology "he" :s "they")
(define-morphology "she" :s "they")

(define-pronoun "they"
  :person :third
  :number :plural
  :gender :neuter
  :case '(:nominative "they"
          :objective "them"
          :genitive "their"
          :reflexive "themselves"))

(define-pronoun "it"
  :person :third
  :number :singular
  :gender :neuter
  :case '(:nominative "it"
          :objective "it"
          :genitive "its"
          :reflexive "itself"))

(define-pronoun "that"
  :person :third
  :number :singular
  :gender :neuter
  :case '(:nominative "that"
          :objective "that"))

(define-morphology "that" :s "those")

(define-pronoun "those"
  :person :third
  :number :plural
  :gender :neuter
  :case '(:nominative "those"
          :objective "those"))

(define-pronoun "some"
  :person :third
  :number :plural
  :gender :neuter
  :case '(:nominative "some"
          :objective "some"))

(define-morphology "some" :s "some")

(define-pronoun "what"
  :person :third
  :number :singular
  :gender :neuter
  :case '(:nominative "what"
          :objective "what"))

(define-pronoun "when"
  :person :third
  :number :singular
  :gender :neuter
  :case '(:nominative "when"
          :objective "when"))

(define-pronoun "where"
  :person :third
  :number :singular
  :gender :neuter
  :case '(:nominative "where"
          :objective "where"))

(define-pronoun "who"
  :person :third
  :number :singular
  :gender :neuter
  :case '(:nominative "who"
          :objective "whom"))

(defparameter *personal-pronouns*
  `((:singular . ((:first . ,(pronoun "me"))
                  (:second . ,(pronoun "you"))
                  (:third . ((:masculine . ,(pronoun "he"))
                             (:feminine . ,(pronoun "she"))
                             (:neuter . ,(pronoun "it")) ))))
    (:plural . ((:first . ,(pronoun "we"))
                (:second . ,(pronoun "y'all"))
                (:third . ,(pronoun "they")))))
  "Personal pronouns indexed by number, person, and gender.")

(defun personal-pronoun (&key case
                         (number :singular)
                         (person :third)
                         (gender :neuter))
  (pronoun
   (flet ((pronoun (item pronouns)
            (etypecase pronouns
              (pronoun pronouns)
              (list (cdr (assoc item pronouns))))))
     (pronoun gender (pronoun person (pronoun number *personal-pronouns*))))
   :case case))

;;; Punctuation, etc.

(defvar *eos* ".!?"
  "Sentence-ending punctuation marks.")

(defgeneric punctuate (sentence &optional mark eos)
  (:documentation "Append a sentence-ending punctuation mark.")
  (:method ((sentence string) &optional (mark #\.) (eos *eos*))
    (if (or (not mark)
            (and (plusp (length sentence))
                 (find (char sentence (1- (length sentence))) eos)))
        sentence ; already punctuated
        (concatenate 'string sentence (string mark)))))

(defgeneric capitalize (sentence)
  (:documentation "Upcase the initial letter.")
  (:method ((sentence string) &aux (sentence (copy-seq sentence)))
    (when (plusp (length sentence))
      (setf (char sentence 0) (char-upcase (char sentence 0))))
    sentence))
