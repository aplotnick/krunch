;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2018 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defgeneric compose (x y &key context)
  (:documentation "Compose descriptions X and Y.")
  (:method (x (y null) &key context) (declare (ignore context)) x)
  (:method ((x null) y &key context) (declare (ignore context)) y)
  (:method (x y &rest args)
    (or (apply #'compose-via-heads x (head x) y (head y) args)
        (apply #'compose-via-rewrites x y args)
        y)))

(defgeneric compose-via-heads (x x-head y y-head &key context)
  (:method (x x-head y y-head &key context)
    (declare (ignore x x-head y y-head context))))

(defvar *corules* '() "Co-composition rules.")
(defvar *compose* (make-symbol "∘") "A unique composition symbol.")

(defclass corule (rewrite-rule) ())

(defun parse-corule (corule)
  (destructuring-bind ((x y) z) corule
    (parse-rule `((,*compose* ,x ,y) ,z) 'corule)))

(defmacro with-corules ((&rest corules) &body body)
  `(let* ((*corules* ,(if corules `(mapcar #'parse-corule ',corules) '*corules*))
          (*rules* *corules*))
     ,@body))

;; Emacs: (put 'define-corule 'lisp-indent-function 0)
(defmacro define-corule ((x y) z)
  "Define a new co-composition rule (∘ X Y) → Z."
  `(let ((*rules* nil))
     (pushnew (define-rule ,(parse-corule `((,x ,y) ,z))) *corules*
              :test #'rule-equal)))

(defgeneric compose-via-rewrites (x y &key context)
  (:method (x y &key context)
    "Rewrite the composition of X and Y using co-composition rules.
If it remains a composition after rewriting, they haven't composed."
    (declare (ignore context))
    (with-corules ()
      (let ((z (rewrite `(,*compose* ,x ,y))))
        (unless (eq (car z) *compose*)
          z)))))

(defmethod apply-rule ((rule corule) term bindings)
  "Only try to match corules against top-level conses, i.e.,
skip the recursion if the LHS of rule fails to match TERM."
  (when (consp term)
    (catch 'no-match
      (return-from apply-rule
        (apply-rule (rhs rule) term (bind* (lhs rule) term bindings)))))
  (values term bindings))
