;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

(in-package :krunch)

(defcat individual (named-object top)
  "Individuals, i.e., objects in the model.")

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; Alias the individual class for convenience in defining methods.
  (setf (find-class 'individual) cat:individual))

(defmethod catspec ((catspec individual))
  "Individuals denote themselves qua categories."
  catspec)

(defmethod itypep (individual (catspec individual))
  (subcatp individual (catspec catspec)))

(defgeneric category-of (individual)
  (:documentation "Return the category of an individual.")
  (:method (individual)
    "Categories are represented by classes."
    (class-of individual)))

(defmethod initialize-instance :after ((instance individual) &key)
  "Record this individual on its category's instance list."
  (push instance (instances (category-of instance))))

(defgeneric make-individual (category &rest initargs)
  (:documentation "Make a new individual of a given category.")
  (:method ((category symbol) &rest initargs)
    (apply #'make-instance (find-category category) initargs))
  (:method ((category category) &rest initargs)
    (apply #'make-instance category initargs)))

(defgeneric delete-individual (individual)
  (:documentation "Delete an individual.")
  (:method ((individual individual))
    (with-accessors ((individuals instances)) (category-of individual)
      (setf individuals (delete individual individuals)))
    individual))

(defgeneric individual-named (name category &key &allow-other-keys)
  (:documentation "Look up an individual by name.")
  (:method ((name individual) (category category) &key)
    (assert (eql (category-of name) category)
            (name category)
            "~a's category is ~a, not ~a." name (category-of name) category)
    name)
  (:method (name (category (eql t)) &key (categories *categories*))
    (some (lambda (cat) (individual-named name cat)) categories))
  (:method (name (category category) &rest args &key
            (key (lambda (individual)
                   (and (slot-boundp individual 'name)
                        (slot-value individual 'name)))))
    (apply #'find name (instances category)
           :key key
           args)))

(defgeneric find-individual (description &key &allow-other-keys)
  (:documentation "Find an individual from some description of it.")
  (:method ((description category) &key)
    "Categories describe themselves."
    description)
  (:method ((description individual) &key)
    "Individuals describe themselves."
    description)
  (:method ((description list) &rest args)
    "True descriptions are represented by lists. We dispatch on their cars."
    (apply #'find-individual-by (car description) description args))
  (:method (description &key)
    "By default, treat the description as a name."
    (individual-named description t)))

(defgeneric find-individual-by (head description &key)
  (:documentation "Description dispatch routine.")
  (:method ((head (eql 'name)) (description list) &key (category t))
    "Find an individual by name."
    (check-type description (cons (eql name) (cons * null)))
    (individual-named (cadr description) category))
  (:method ((head category) (description list) &key category)
    "Find an individual by its category."
    (check-type description (cons category (cons * null)))
    (assert (or (not category) (eql category head))
            (head category)
            "Category argument disagrees with description.")
    (find-individual (cadr description) :category head))
  (:method ((head symbol) (description list) &key (category t))
    "Find an individual by slot value or category name."
    (check-type description (cons symbol (cons * null)))
    (if (eql (symbol-package head) *category-package*)
        (find-individual (cadr description) :category (find-category head))
        (etypecase category
          (null)
          (category (find (cadr description) (instances category)
                          :key (lambda (individual)
                                 (and (ignore-errors (slot-boundp individual head))
                                      (slot-value individual head)))))
          ((eql t) (some (lambda (category)
                           (find-individual description :category category))
                         *categories*))))))
