;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

;;; Hassan Aït-Kaci introduced order-sorted feature structures, or ψ-terms,
;;; in his dissertation (1984), and subsequently used them as the basis of
;;; his LOGIN (1986), Le Fun (1987), LIFE (1989), and HOOT (2014) languages.
;;; This implementation is based on the "HOOT Language Specification" (2014).

(in-package :krunch)

;;; ψ-terms represent types (e.g., categories) in a lattice.

(defclass term-lattice (lisp-lattice bottom-up-binary-encoding)
  ((term-tags :accessor term-tags
              :initarg :tags
              :initform (make-hash-table))
   (feature-domains :accessor feature-domains
                    :initarg :domains
                    :initform (make-hash-table))
   (feature-ranges :accessor feature-ranges
                   :initarg :ranges
                   :initform (make-hash-table))))

(defmethod copy-lattice ((lattice term-lattice) &rest initargs &key
                         (tags (make-hash-table)) ; don't copy tags by default
                         (domains (copy-hash-table (feature-domains lattice)))
                         (ranges (copy-hash-table (feature-ranges lattice))))
  (apply #'call-next-method lattice
         :tags tags
         :domains domains
         :ranges ranges
         initargs))

(defvar *term-lattice*
  (if *category-lattice*
      (change-class *category-lattice* 'term-lattice)
      (make-instance 'term-lattice)))

(defgeneric constant-type-name (object)
  (:documentation "Return a constant that names the type of OBJECT.
Cf. CONSTANT-NAME.")
  (:method (object) (constant-name (class-of object)))
  (:method ((object class)) (constant-name object))
  (:method ((object (eql t))) t)
  (:method ((object null)))
  (:method ((object symbol))
    (cond ((constantp object) object)
          (t (constant-name (symbol-name object))))))

;;; ψ-terms.

(defclass term (named-object)
  ((name :accessor term-tag :initarg :tag)
   (type :accessor term-type :initarg :type)
   (subterms :accessor subterms
             :initarg :subterms
             :documentation "A plist keyed on features."))
  (:default-initargs :tag nil :type t :subterms '())
  (:documentation "Aït-Kaci's ψ-terms."))

(defun make-ψ-term (tag &optional (type t) &rest subterms)
  (make-instance 'term
                 :tag tag
                 :type type
                 :subterms subterms))

(defun term-sans (term &rest features)
  "Remove features of a term."
  (make-instance 'term
                 :tag (term-tag term)
                 :type (term-type term)
                 :subterms (apply #'sans (subterms term) features)))

(defun term-sans-values (term &rest values)
  "Remove features of a term with any of the given values."
  (loop for (feature subterm) on (subterms term) by #'cddr
        if (member subterm values) collect feature into features
        finally (return (if features (apply #'term-sans term features) term))))

(defmethod print-object ((term term) stream)
  (format stream "~:@<~a~^~<~@{ ~s ~s~^~_~}~:>~:>"
          (name (term-type term))
          (subterms term)))

(defmethod make-load-form ((object term) &optional environment)
  (make-load-form-saving-slots object :environment environment))

(defvar ⊤ (make-ψ-term t t))
(defvar ⊥ (make-ψ-term nil nil))

(defmethod print-object ((term (eql ⊤)) stream)
  (write-char #\⊤ stream))

(defmethod print-object ((term (eql ⊥)) stream)
  (write-char #\⊥ stream))

(defmethod (setf term-type) (new-type (term (eql ⊤)))
  (assert (eq new-type t) (new-type term) "Can't change the type of ⊤.")
  new-type)

(defmethod (setf term-type) (new-type (term (eql ⊥)))
  (assert (null new-type) (new-type term) "Can't change the type of ⊥.")
  new-type)

(defmethod constant-type-name ((object term))
  (constant-name (term-type object)))

(defmethod itypep ((individual term) catspec)
  (itypep (term-type individual) catspec))

(defparameter *dejavu* '()
  "A list of objects we've seen.")

(defmacro with-dejavu (&body forms)
  "Execute FORMS in a lexical environment containing a local function DEJAVU
of one argument that returns true if the given object has been previously seen."
  (let ((tabula-rasa (gensym "DEJAVU")))
    `(let ((,tabula-rasa (null *dejavu*)))
       (unwind-protect (flet ((dejavu (object)
                                (or (find object *dejavu*)
                                    (and (push object *dejavu*)
                                         nil))))
                         (progn ,@forms))
         (when ,tabula-rasa
           (setq *dejavu* '()))))))

;;; Tags (aka variables) name ψ-terms.

(defclass tag (named-object)
  ((term :accessor tag-term :initarg :term)
   (value :accessor tag-value :initarg :value))
  (:default-initargs :name nil :term ⊤)
  (:documentation "A name for a ψ-term."))

(defun make-tag (name &optional (term ⊤) value)
  (make-instance 'tag :name name :term term :value value))

(defmethod print-object ((object tag) stream)
  (with-dejavu
    (format stream "~:[ψ~;!~:*~a~]~@[ : ~a~]"
            (name object)
            (unless (dejavu object)
              (case (name (term-type object))
                ((t) nil)
                (t (tag-term object)))))))

(defmethod make-load-form ((object tag) &optional environment)
  (make-load-form-saving-slots object :environment environment))

(defmethod shared-initialize :after ((instance tag) slot-names &key)
  "Unbound tags refer to themselves. Aït-Kaci picked up this trick
from the WAM (Aït-Kaci 1991)."
  (declare (ignore slot-names))
  (unless (slot-boundp instance 'value)
    (setf (slot-value instance 'value) instance)))

(defgeneric tag-unboundp (tag)
  (:documentation "Return true if the tag is unbound (i.e., refers to itself).")
  (:method ((tag null)) t)
  (:method ((tag tag)) (eq tag (tag-value tag))))

(defgeneric tag-boundp (tag)
  (:documentation "Return true if the tag is bound (i.e., has a value).")
  (:method (tag) (not (tag-unboundp tag))))

(defgeneric deref (tag)
  (:documentation "Follow a value chain down to an unbound tag.")
  (:method ((tag tag))
    (do ((tag tag (tag-value tag)))
        ((tag-unboundp tag) tag))))

(deftype tag-name ()
  '(or symbol integer))

(deftype raw-tag ()
  '(cons (member tag :tag)
         (cons tag-name list)))

(defun raw-tag-p (object)
  (typep object 'raw-tag))

(defgeneric tag-p (object)
  (:method (object) (declare (ignore object)))
  (:method ((object tag)) t)
  (:method ((object cons)) (or (raw-tag-p object) (plistp object))))

(defgeneric get-tag (name tags)
  (:documentation "Find or intern NAME in TAGS.")
  (:method ((name tag) tags)
    (get-tag (name name) tags))
  (:method (name (tags term-lattice))
    (get-tag name (term-tags tags)))
  (:method (name (tags hash-table))
    (check-type name tag-name "a valid tag name")
    (or (gethash name tags)
        (setf (gethash (setq name (or name (gensym "TAG"))) tags)
              (make-tag name)))))

(defun tag-cycles (term &optional bindings)
  "Replace cycles in TERM with tag bindings & references."
  (multiple-value-bind (term bindings) (decycle term bindings)
    (if (null bindings)
        term
        (labels ((tag (term &optional nobind)
                   (cond ((variablep term)
                          (let* ((var term)
                                 (value (variable-value var bindings nil)))
                            (if (tag-p value)
                                value
                                `(tag ,var t))))
                         ((atom term)
                          term)
                         (t (let ((binding (and (not nobind)
                                                (rassoc term bindings))))
                              (if binding
                                  (destructuring-bind (var . value) binding
                                    (if (tag-p value)
                                        value
                                        `(tag ,var ,(tag value t))))
                                  (recons term
                                          (tag (car term))
                                          (tag (cdr term)))))))))
          (tag term)))))

;; Tags (and nil) implement the term protocol, too.

(defmethod term-tag ((term null)))
(defmethod term-tag ((term tag))
  term)

(defmethod (setf term-tag) (tag (term tag))
  (setf (term-tag (tag-term term)) tag))

(defmethod term-type ((term null)))
(defmethod term-type ((term tag))
  (term-type (tag-term term)))

(defmethod (setf term-type) (type (term tag))
  (setf (term-type (tag-term term)) type))

(defmethod subterms ((term null)))
(defmethod subterms ((term tag))
  (subterms (tag-term term)))

(defmethod (setf subterms) (subterms (term tag))
  (setf (subterms (tag-term term)) subterms))

(defmethod constant-type-name ((object tag))
  (constant-name (term-type object)))

(defmethod itypep ((individual tag) catspec)
  (itypep (term-type individual) catspec))

;;; Aggregate terms.

(defclass aggregate-term (term)
  ((members :accessor members :initarg :members))
  (:default-initargs :members '())
  (:documentation
"An extensional pseudo-term (e.g., a list or set) defined by its members.
Not a proper ψ-term (since no subterms are allowed) and so not normally
subject to normalization."))

(defmethod (setf subterms) (subterms (term aggregate-term))
  (cerror "Set them anyway."
          "Aggregate terms may not have subterms.")
  (call-next-method))

(defmethod print-object ((term aggregate-term) stream)
  (format stream "~:@<~a~^~<~@{ ~s~^~_~}~:>~:>"
          (name (term-type term))
          (members term)))

(defvar *aggregates*
  '(:list :set)
  "A list of heads that denote aggregates.")

(defgeneric aggregate-term-p (term)
  (:method (term) (declare (ignore term)))
  (:method ((term aggregate-term)) t)
  (:method ((term cons)) (find (car term) *aggregates*)))

;;; Features.

(define-condition missing-feature (error)
  ((feature :reader missing-feature-name :initarg :feature)
   (term :reader missing-feature-term :initarg :term))
  (:report (lambda (error stream &aux (*print-circle* nil))
             (format stream "Can't find feature ~a in term ~a."
                     (missing-feature-name error)
                     (missing-feature-term error)))))

(defgeneric feature (feature term &optional errorp)
  (:documentation "Look up a feature of a term.")
  (:method (feature term &optional errorp)
    (and errorp
         (error 'missing-feature
                :feature feature
                :term term)))
  (:method (feature (term tag) &optional errorp)
    (feature feature (tag-term term) errorp))
  (:method (feature (term term) &optional errorp)
    (or (getf (subterms term) feature)
        (and errorp
             (error 'missing-feature
                    :feature feature
                    :term term))))
  (:method (feature (term list) &optional errorp)
    (or (getf (cdr term) feature)
        (and errorp
             (error 'missing-feature
                    :feature feature
                    :term term)))))

(defgeneric (setf feature) (new-value feature term &optional errorp)
  (:documentation "Set a feature of a term.")
  (:method (new-value feature (term tag) &optional errorp)
    (setf (feature feature (tag-term term) errorp) new-value))
  (:method (new-value feature (term term) &optional errorp)
    (declare (ignore errorp))
    (setf (getf (subterms term) feature) new-value))
  (:method (new-value feature (term list) &optional errorp)
    (declare (ignore errorp))
    (setf (getf (cdr term) feature) new-value)))

(defgeneric get-features (term features)
  (:documentation "Look up any of several features at once.")
  (:method ((term tag) features)
    (get-features (tag-term term) features))
  (:method ((term term) (features list))
    (get-properties (subterms term) features)))

(defgeneric feature-domain (feature lattice)
  (:method (feature (lattice term-lattice))
    (gethash feature (feature-domains lattice))))

(defgeneric (setf feature-domain) (domain feature lattice)
  (:method (domain feature (lattice term-lattice))
    "Keep only maximal feature domains (HOOT Algorithm 7)."
    (setf (gethash feature (feature-domains lattice))
          (let ((domains (feature-domain feature lattice)))
            (if domains
                (min/max-bounds domains #'upper-bound-p lattice domain)
                (list domain))))
    domain))

(defgeneric feature-range (feature domain lattice)
  (:method (feature domain (lattice term-lattice))
    "Feature function ranges are indexed by domain, then feature."
    (gethash feature (or (gethash domain (feature-ranges lattice))
                         (progn
                           (register-type domain lattice)
                           (setf (gethash domain (feature-ranges lattice))
                                 (make-hash-table)))))))

(defgeneric (setf feature-range) (range feature domain lattice)
  (:method (range feature domain (lattice term-lattice))
    (setf (gethash feature (gethash domain (feature-ranges lattice))) range)))

(define-condition undefined-feature (error)
  ((feature :reader undefined-feature-name :initarg :feature)
   (domain :reader undefined-feature-domain :initarg :domain)
   (range :reader undefined-feature-range :initarg :range)
   (lattice :reader lattice :initarg :lattice))
  (:default-initargs :domain t :range t :lattice *term-lattice*)
  (:report (lambda (error stream &aux (*print-circle* nil))
             (format stream "The feature ~a : ~a → ~a~%~
                             is not defined in ~a."
                     (undefined-feature-name error)
                     (undefined-feature-domain error)
                     (undefined-feature-range error)
                     (lattice error)))))

(define-condition inconsistent-feature (undefined-feature)
  ((conflict :reader conflicting-feature-value :initarg :conflict))
  (:report (lambda (error stream &aux (*print-circle* nil))
             (format stream "The value ~a is inconsistent with the feature~%~
                             ~a : ~a → ~a~%~
                             in ~a."
                     (conflicting-feature-value error)
                     (undefined-feature-name error)
                     (undefined-feature-domain error)
                     (undefined-feature-range error)
                     (lattice error)))))

(defun propagate-feature (feature domain range lattice)
  "Propagate a feature declaration throughout the taxonomy (HOOT Algorithm 6)."
  (declare (optimize debug))
  (when (or (null domain) (eq domain ⊥))
    (return-from propagate-feature ⊥))
  (flet ((propagate-range (range)
           (setf (feature-range feature domain lattice) range)
           (dolist (child (children domain lattice) range)
             (propagate-feature feature child range lattice))))
    (let ((defined-range (feature-range feature domain lattice)))
      (if (not defined-range)
          (propagate-range range)
          (let ((restricted-range (or (glb defined-range range lattice)
                                      (error 'inconsistent-feature
                                             :feature feature
                                             :domain domain
                                             :range range
                                             :lattice lattice))))
            (unless (eql defined-range restricted-range)
              (propagate-range restricted-range)))))))

(defun define-feature (feature domain range &key (lattice *term-lattice*) &aux
                       (domain (normalize-ψ-term-type domain lattice))
                       (range (normalize-ψ-term-type range lattice)))
  "Declare FEATURE : DOMAIN → RANGE (HOOT Algorithm 5)."
  (setf (feature-domain feature lattice) domain)
  (propagate-feature feature domain range lattice)
  feature)

;;; ψ-term normalization.

(defun augment-eqs (term feature value &optional eqs)
  "Set or constrain a feature value."
  (let ((other (feature feature term)))
    (if other
        (push (cons value other) eqs)
        (setf (feature feature term) value)))
  eqs)

(defun solve-eqs (eqs lattice)
  "Solve the ψ-term feature constraint equations (HOOT Algorithm 3)."
  (declare (optimize debug))
  (loop while eqs
        as eq = (pop eqs)
        as lhs = (deref (car eq))
        as rhs = (deref (cdr eq))
        unless (eql lhs rhs)
        do (loop initially
                   (setf (term-type rhs) (or (glb (term-type lhs)
                                                  (term-type rhs)
                                                  lattice)
                                             (return-from solve-eqs nil))
                         (tag-value lhs) rhs)
                 for (feature value) on (subterms lhs) by #'cddr
                 do (setq eqs (augment-eqs rhs feature value eqs)))
        finally (return t)))

(define-condition inconsistent-ψ-term (error)
  ((term :reader inconsistent-ψ-term :initarg :term)
   (lattice :reader lattice :initarg :lattice))
  (:report (lambda (error stream &aux (*print-circle* nil))
             (format stream "Inconsistent ψ-term ~a~%~
                             with respect to ~a."
                     (inconsistent-ψ-term error)
                     (lattice error)))))

(defgeneric preprocess-ψ-term (raw-term &optional lattice)
  (:documentation "Prepare a ψ-term for normalization (HOOT Algorithm 2).")
  (:method (raw-term &optional (lattice *term-lattice*))
    (preprocess-ψ-term (list raw-term) lattice))
  (:method ((raw-term null) &optional lattice)
    (declare (ignore lattice)))
  (:method ((raw-term cons) &optional (lattice *term-lattice*))
    (preprocess-ψ-term (parse-ψ-term raw-term) lattice))
  (:method ((raw-term (eql ⊤)) &optional lattice)
    (declare (ignore lattice))
    raw-term)
  (:method ((raw-term (eql ⊥)) &optional lattice)
    (declare (ignore lattice))
    raw-term)
  (:method ((raw-term aggregate-term) &optional lattice)
    (declare (ignore lattice))
    raw-term)
  (:method ((raw-term tag) &optional (lattice *term-lattice*))
    (preprocess-ψ-term (tag-term raw-term) lattice))
  (:method ((raw-term term) &optional (lattice *term-lattice*))
    (declare (optimize debug))
    (with-dejavu
      (loop with eqs = '() ; feature constraint equations
            with tag = (get-tag (name raw-term) lattice)
            with term = (if (eq (tag-term tag) ⊤)
                            (setf (tag-term tag) (make-ψ-term tag))
                            (tag-term tag))
            and raw-type = (normalize-ψ-term-type (term-type raw-term) lattice)
            initially
              (assert (and tag term
                           (eq tag (name term))
                           (eq term (tag-term tag))))
              (when (dejavu tag)
                (loop-finish))
              (setf (term-type term)
                    (or (glb raw-type (term-type term) lattice)
                        (error 'inconsistent-ψ-term
                               :term raw-term
                               :lattice lattice)))
            for (feature raw-subterm) on (subterms raw-term) by #'cddr
            do (multiple-value-bind (subtag subeqs)
                   (preprocess-ψ-term raw-subterm lattice)
                 (setq eqs (augment-eqs term feature subtag
                                        (nconc subeqs eqs))))
            finally (setf (subterms term) (reverse-plist (subterms term)))
                    (return (values tag eqs))))))

(defgeneric normalize-ψ-term-type (raw-type lattice)
  (:method ((raw-type null) lattice)
    (declare (ignore lattice)))
  (:method ((raw-type symbol) lattice)
    (if (keywordp raw-type)
        raw-type
        (register-type (or (find-category raw-type nil)
                           (find-class raw-type t))
                       lattice)))
  (:method (raw-type lattice)
    (register-type raw-type lattice)))

(defgeneric normalize-features (term lattice)
  (:method ((tag tag) lattice)
    (normalize-features (tag-term tag) lattice)
    tag)
  (:method ((term aggregate-term) lattice)
    (declare (ignore lattice))
    term)
  (:method ((term term) (lattice term-lattice))
   "Normalize a ψ-term with respect to defined features (HOOT Algorithm 8)."
   (declare (optimize debug))
   (with-dejavu
     (loop with tag = (name term)
           initially
             (assert tag (term) "Can't normalize an untagged ψ-term.")
             (when (dejavu tag)
               (loop-finish))
           for (feature-name subterm) on (subterms term) by #'cddr
           as feature = (normalize-ψ-term-type feature-name lattice)
           as domain = (choose ; randomly
                        (restart-case (or (feature-domain feature lattice)
                                          (error 'undefined-feature
                                                 :feature feature
                                                 :lattice lattice))
                          (define-feature ()
                            :report (lambda (stream)
                                      (format stream "Define feature ~a : T → T."
                                              feature))
                            (list (propagate-feature feature t t lattice)))))
           as range = (feature-range feature domain lattice)
           when (and (term-type term) (term-type subterm))
           do (setf (term-type term)
                    (or (glb domain (term-type term) lattice)
                        (error 'inconsistent-feature
                               :conflict (term-type term)
                               :feature feature
                               :domain domain
                               :range range
                               :lattice lattice))
                    (term-type subterm)
                    (or (glb range (term-type subterm) lattice)
                        (error 'inconsistent-feature
                               :conflict (term-type subterm)
                               :feature feature
                               :domain domain
                               :range range
                               :lattice lattice)))
              (normalize-features subterm lattice)
           finally (return term)))))

(declaim (inline normalize))
(defun normalize (raw-type &optional (lattice *term-lattice*))
  "An alias for NORMALIZE-Ψ-TERM."
  (normalize-ψ-term raw-type lattice))

(defun normalize-ψ-term (raw-term &optional (lattice *term-lattice*))
  "Normalize a ψ-term (HOOT Algorithm 1)."
  (multiple-value-bind (tag eqs) (preprocess-ψ-term raw-term lattice)
    (when (solve-eqs eqs lattice)
      (normalize-features tag lattice))))

;;; Reading & parsing ψ-terms.

(defgeneric parse-ψ-term (term &key &allow-other-keys)
  (:documentation "Construct a raw (unnormalized) ψ-term from an S-expression.")
  (:method ((term tag) &key) term)
  (:method ((term term) &key) term)
  (:method ((term (eql t)) &rest initargs)
    (if initargs
        (apply #'make-instance 'term initargs)
        ⊤))
  (:method ((term null) &key) ⊥)
  (:method ((term cons) &rest initargs)
    (cond ((raw-tag-p term)
           (destructuring-bind (tag term) (cdr term)
             (parse-ψ-term term :name tag)))
          ((aggregate-term-p term)
           (apply #'make-instance 'aggregate-term
                  :type (car term)
                  :members (cdr term)
                  initargs))
          (t (apply #'make-instance 'term
                    :type (car term)
                    :subterms (loop for (feature subterm) on (cdr term) by #'cddr
                                    nconc (list feature (parse-ψ-term subterm)))
                    initargs))))
  (:method (term &rest initargs)
    (apply #'make-instance 'term
           :type term
           initargs)))

(defvar *ψ-term-readtable* (copy-readtable))

(defun read-tag (stream char)
  "Read a tagged ψ-term from STREAM as the reader macro for CHAR."
  (let* ((tag (case char
                (#\ψ (gensym "ψ-"))
                (t (read stream t nil t))))
         (type (case (peek-char t stream nil nil t)
                 (#\: (assert (char= #\: (read-char stream t nil t)))
                      (read stream t nil t))
                 (t t))))
    `(tag ,tag ,type)))

(set-macro-character #\! 'read-tag t *ψ-term-readtable*)
(set-macro-character #\ψ 'read-tag t *ψ-term-readtable*)

;;; Local Variables:
;;; coding: utf-8
;;; eval: (define-abbrev lisp-mode-abbrev-table "psi" "ψ")
;;; eval: (define-abbrev lisp-mode-abbrev-table "bot" "⊥")
;;; eval: (define-abbrev lisp-mode-abbrev-table "top" "⊤")
;;; abbrev-mode: t
;;; End:
