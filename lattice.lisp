;;; -*- Mode: Lisp; Syntax: Common-Lisp; Coding: utf-8; Package: KRUNCH; -*-
;;; Copyright (c) 2016 SIFT LLC. All Rights Reserved.

;;; Adapted from "A Decision  Procedure for Common Lisp's SUBTYPEP Predicate"
;;; by Henry Baker. See also "Efficient Implementation of Lattice Operations"
;;; (1989) by Aït-Kaci et al., "Classifying and Querying Very Large Taxonomies
;;; with Bit-Vector Encoding" (2015) by Aït-Kaci & Amir, and "Incremental
;;; Encoding of Multiple Inheritance Hierarchies Supporting Lattice Operations"
;;; (2000) by van Bommel & Beck.

(in-package :krunch)

;;; The lattice protocol.

(defgeneric bottom (x lattice)
  (:documentation "Is X the bottom element of the lattice?"))

(defgeneric top (x lattice)
  (:documentation "Is X the top element of the lattice?"))

(defgeneric lower-bounds (x lattice)
  (:documentation "Return a list of lower bounds of X."))

(defgeneric upper-bounds (x lattice)
  (:documentation "Return a list of upper bounds of X."))

(defgeneric lower-bound-p (x y lattice)
  (:documentation "Return true if X ⊑ Y."))

(defgeneric upper-bound-p (x y lattice)
  (:documentation "Return true if X ⊒ Y."))

(defgeneric glb (x y lattice)
  (:documentation "Return the greatest lower bound X ⊓ Y.
Also called meet, infimum, most specific subsumee, etc."))

(defgeneric lub (x y lattice)
  (:documentation "Return the least upper bound X ⊔ Y.
Also called join, supremum, least common subsumer, etc."))

(defgeneric butnot (x y lattice)
  (:documentation "Return the relative complement X ∖ Y."))

(defgeneric children (x lattice)
  (:documentation "Return the children of lattice element X.")
  (:method (x lattice)
    (declare (ignore x lattice))))

(defgeneric parents (x lattice)
  (:documentation "Return the parents of lattice element X.")
  (:method (x lattice)
    (declare (ignore x lattice))))

(defgeneric encode-all-types (lattice)
  (:documentation "Assign representations to every type in the lattice."))

(defgeneric encode-type (type lattice &optional errorp)
  (:documentation "Encode a type using the lattice representation.")
  (:method ((type (eql t)) lattice &optional errorp)
    "The symbol T designates the top lattice element."
    (encode-type (lattice-top lattice) lattice errorp)))

(defgeneric (setf encode-type) (code type lattice &optional errorp)
  (:documentation "Set the lattice representation of a type."))

(defgeneric decode-type (code lattice &optional errorp)
  (:documentation "Decode a type representation for a lattice."))

(defgeneric register-type (type lattice &key)
  (:documentation "Register a lattice type for encoding.")
  (:method (type (lattice null) &key)
    (declare (ignore type))))

(defmacro with-lattice-operators (lattice &body forms)
  "Execute FORMS with operators curried over LATTICE."
  (let ((lattice-temp (make-symbol "LATTICE")))
    `(let ((,lattice-temp ,(case lattice
                             ((t) '*category-lattice*)
                             (t lattice))))
       (flet ((bottom (x) (bottom x ,lattice-temp))
              (top (x) (top x ,lattice-temp))
              (lbs (x) (lower-bounds x ,lattice-temp))
              (ubs (x) (upper-bounds x ,lattice-temp))
              (lbp (x y) (lower-bound-p x y ,lattice-temp))
              (ubp (x y) (upper-bound-p x y ,lattice-temp))
              (glb (x y) (glb x y ,lattice-temp))
              (lub (x y) (lub x y ,lattice-temp))
              (butnot (x y) (butnot x y ,lattice-temp))
              (children (x) (children x ,lattice-temp))
              (parents (x) (parents x ,lattice-temp))
              (encode (type &optional (errorp t))
                (encode-type type ,lattice-temp errorp))
              ((setf encode) (new-code type &optional errorp)
                (setf (encode-type type ,lattice-temp errorp) new-code))
              (decode (code &optional (errorp t))
                (decode-type code ,lattice-temp errorp))
              (register (type &rest args)
                (apply #'register-type type ,lattice-temp args)))
         (declare (ignorable ,@(mapcar (lambda (fn) `#',fn)
                                       '(bottom top
                                         lbs ubs lbp ubp
                                         glb lub butnot
                                         children parents
                                         encode (setf encode)
                                         decode register))))
         ,@forms))))

;;; The primary carrier of the lattice protocol.

(defclass lattice ()
  ((top :accessor lattice-top :initarg :top)
   (bottom :accessor lattice-bottom :initarg :bottom)
   (types :accessor lattice-types :initarg :types) ; type → code
   (codes :accessor lattice-codes :initarg :codes) ; code → type
   (leaves :accessor lattice-leaves :initarg :leaves)
   (all-types :accessor all-lattice-types :initarg :all-types))
  (:default-initargs :top (find-class t)
                     :bottom nil
                     :types (make-hash-table)
                     :codes (make-hash-table)
                     :leaves '()
                     :all-types '())
  (:documentation "A type lattice."))

(defgeneric copy-lattice (lattice &rest initargs)
  (:method ((lattice lattice) &rest initargs &key
            (top (lattice-top lattice))
            (bottom (lattice-bottom lattice))
            (types (copy-hash-table (lattice-types lattice)))
            (codes (copy-hash-table (lattice-codes lattice)))
            (leaves (copy-list (lattice-leaves lattice)))
            (all-types (copy-list (all-lattice-types lattice))))
    (apply #'make-instance (class-of lattice)
           :top top
           :bottom bottom
           :types types
           :codes codes
           :leaves leaves
           :all-types all-types
           initargs)))

(defmethod bottom (x (lattice lattice))
  (eql x (lattice-bottom lattice)))

(defmethod top (x (lattice lattice))
  (eql x (lattice-top lattice)))

(defun limit-lattice-types (types lattice)
  (and types
       (let ((lattice-types (lattice-types lattice)))
         (flet ((lattice-type-p (type) (nth-value 1 (gethash type lattice-types))))
           (remove-if-not #'lattice-type-p types)))))

(defmethod children :around (x (lattice lattice))
  "The children of the top element are all of the types in the lattice.
The children of any other type is the intersection of the known types
with whatever the primary method returns."
  (if (top x lattice)
      (all-lattice-types lattice)
      (limit-lattice-types (call-next-method) lattice)))

(defmethod parents :around (x (lattice lattice))
  "The parents of the bottom element are the leaves of the lattice.
The parents of any other type are the intersection of the known types
with whatever the primary method returns."
  (if (bottom x lattice)
      (lattice-leaves lattice)
      (limit-lattice-types (call-next-method) lattice)))

(define-condition lattice-decoding-error (error)
  ((code :reader lattice-code :initarg :code)
   (lattice :reader lattice :initarg :lattice))
  (:report (lambda (error stream)
             (format stream "Unable to decode ~b for ~s." ; assume binary code
                     (lattice-code error)
                     (lattice error)))))

(defmethod decode-type (code (lattice lattice) &optional (errorp t))
  (or (gethash code (lattice-codes lattice))
      (and errorp
           (error 'lattice-decoding-error
                  :code code
                  :lattice lattice))))

(define-condition lattice-encoding-error (error)
  ((type :reader lattice-type :initarg :type)
   (lattice :reader lattice :initarg :lattice))
  (:report (lambda (error stream)
             (format stream "Unable to encode ~s for ~s."
                     (lattice-type error)
                     (lattice error)))))

(defmethod encode-type (type (lattice lattice) &optional (errorp t))
  (or (gethash type (lattice-types lattice))
      (and errorp
           (restart-case (error 'lattice-encoding-error
                                :type type
                                :lattice lattice)
             (continue ()
               :report "Register the type with the lattice and continue."
               (register-type type lattice)
               ;; Take the outermost continue restart to use the new encodings.
               (let ((continue (find 'continue (compute-restarts)
                                     :key #'restart-name
                                     :from-end t)))
                 (if continue
                     (invoke-restart continue)
                     (encode-type type lattice t))))))))

(defmethod encode-type :around (type lattice &optional errorp)
  (restart-case (call-next-method)
    (continue ()
      :report "Try again to encode the type."
      :test (lambda (c) (declare (ignore c)) errorp)
      (call-next-method))))

(defmethod (setf encode-type) (code type (lattice lattice) &optional errorp)
  (declare (ignore errorp))
  (setf (gethash code (lattice-codes lattice)) type
        (gethash type (lattice-types lattice)) code))

(defmethod register-type (type (lattice lattice) &key)
  (let ((registered-p (nth-value 1 (gethash type (lattice-types lattice)))))
    (unless registered-p
      (push type (all-lattice-types lattice))
      (setf (gethash type (lattice-types lattice)) nil
            (lattice-leaves lattice)
            (with-lattice-operators lattice
              (remove-if-not #'null (all-lattice-types lattice) :key #'children))))
    (values type registered-p)))

(defmethod children ((x class) (lattice lattice))
  (class-direct-subclasses x))

(defmethod parents ((x class) (lattice lattice))
  (class-direct-superclasses x))

;;; Plunge the lattice into a boolean one.

(defclass binary-encoding (lattice)
  ((code-bit :accessor code-bit :initarg :code-bit))
  (:default-initargs :code-bit 0)
  (:documentation "Represent lattice elements using a binary code."))

(defclass bottom-up-binary-encoding (binary-encoding)
  ()
  (:documentation "Encode lattice elements as in Aït-Kaci et al.
The bottom element is represented by zero (i.e., no bits set).
The top element is represented by negative one (i.e., all bits set)."))

(defmethod bottom ((x (eql 0)) (lattice bottom-up-binary-encoding))
  t)

(defmethod top ((x (eql -1)) (lattice bottom-up-binary-encoding))
  t)

(defmethod encode-type :around (type (lattice bottom-up-binary-encoding)
                                &optional errorp)
  (declare (ignore errorp))
  (cond ((bottom type lattice) 0)
        ((top type lattice) -1)
        (t (call-next-method))))

(defmethod encode-type ((type cons) (lattice bottom-up-binary-encoding)
                        &optional errorp)
  "Compound types are represented by logical operations over type codes."
  (declare (ignore errorp))
  (with-lattice-operators lattice
    (etypecase type
      ((cons (member or :or ∨) *) `(logior ,@(mapcar #'encode (cdr type))))
      ((cons (member and :and ∧) *) `(logand ,@(mapcar #'encode (cdr type))))
      ((cons (member eqv :eqv =) *) `(= ,@(mapcar #'encode (cdr type))))
      ((cons (member not :not ¬) (cons * null)) `(lognot ,(encode (cadr type)))))))

(defmethod decode-type ((code (eql 0)) (lattice bottom-up-binary-encoding)
                        &optional errorp)
  (declare (ignore errorp))
  (lattice-bottom lattice))

(defmethod decode-type ((code (eql -1)) (lattice bottom-up-binary-encoding)
                        &optional errorp)
  (declare (ignore errorp))
  (lattice-top lattice))

(defmethod lower-bounds (x (lattice bottom-up-binary-encoding))
  (lower-bounds (encode-type x lattice) lattice))

(defmethod lower-bounds ((x (eql -1)) (lattice bottom-up-binary-encoding))
  (all-lattice-types lattice))

(defmethod lower-bounds ((x integer) (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (remove-if-not (lambda (y)
                     (and (plusp y)
                          (logbitp (1- (integer-length y)) x)))
                   (all-lattice-types lattice)
                   :key #'encode)))

(defmethod upper-bounds (x (lattice bottom-up-binary-encoding))
  (upper-bounds (encode-type x lattice) lattice))

(defmethod upper-bounds ((x (eql -1)) (lattice bottom-up-binary-encoding))
  (list (lattice-top lattice)))

(defmethod upper-bounds ((x integer) (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (remove-if-not (lambda (y)
                     (or (not (plusp x))
                         (logbitp (1- (integer-length x)) y)))
                   (all-lattice-types lattice)
                   :key #'encode)))

(defmethod lower-bound-p (x y (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (eval (encode `(= ,x (and ,x ,y))))))

(defmethod upper-bound-p (x y (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (eval (encode `(= ,x (or ,x ,y))))))

(defun min/max-bounds (bounds bound-p lattice &rest elements &aux
                       (bounds
                        (remove-if-not
                         (lambda (b) ; it must be a bound of each element
                           (every (lambda (e) (funcall bound-p b e lattice))
                                  elements))
                         bounds)))
  "Collect the minimal/maximal upper/lower bounds of a set of lattice elements."
  (loop for b in bounds
        when (loop for z in bounds
                   never (and (not (eql b z)) ; and it must be a strict bound
                              (funcall bound-p b z lattice)))
        collect b))

(defmethod glb (x y (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (handler-case (decode (eval (encode `(and ,x ,y))))
      (lattice-decoding-error (e)
        (designate-list
         (min/max-bounds (lbs (lattice-code e)) #'lower-bound-p lattice x y))))))

(defmethod lub (x y (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (handler-case (decode (eval (encode `(or ,x ,y))))
      (lattice-decoding-error (e)
        (designate-list
         (min/max-bounds (ubs (lattice-code e)) #'upper-bound-p lattice x y))))))

(defmethod butnot (x y (lattice bottom-up-binary-encoding))
  (with-lattice-operators lattice
    (decode (eval (encode `(and ,x (not ,y)))))))

(defmethod register-type :around (type (lattice bottom-up-binary-encoding) &key
                                  (encode-all-types t))
  "The bottom-up encoding is not incremental, so by default we
re-encode the whole lattice after every new type registration."
  (multiple-value-bind (type registered-p) (call-next-method)
    (when (and (not registered-p) encode-all-types)
      (encode-all-types lattice))
    (values type registered-p)))

(defmethod encode-all-types ((lattice bottom-up-binary-encoding))
  "Figs. 7-9 in Aït-Kaci et al. (HOOT Algorithm 4)."
  (setf (code-bit lattice) 0)
  (clrhash (lattice-codes lattice))
  (with-lattice-operators lattice
    (flet ((next-layer (cochain)
             "Return the layer above a cochain."
             (remove-if (lambda (parent)
                          (some (lambda (child) (not (encode child nil)))
                                (children parent)))
                        (reduce #'union (mapcar #'parents cochain))))
           (encode-layer (cochain)
             "Assign bit representations to each element of a cochain."
             (dolist (element cochain)
               (let ((code (logior (ash 1 (code-bit lattice))
                                   (reduce #'logior
                                           (mapcar #'encode
                                                   (children element))))))
                 (setf (encode element) code))
               (incf (code-bit lattice)))))
      (do ((layer (next-layer (list (lattice-bottom lattice)))
                  (next-layer layer)))
          ((null layer) lattice)
        (encode-layer layer)))))
