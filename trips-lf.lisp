;;; -*- Mode: LISP; Syntax: Common-Lisp; Package: KRUNCH -*-
;;; Copyright (c) 2017 SIFT LLC. All Rights Reserved.

;;; We handle TRIPS logical forms as emitted by the parser,
;;;   and also CLiC logical forms as emitted by SPIRE.

(in-package :krunch)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :parser)
    (defpackage :parser (:use :cl))))

(defun lf (text &rest args &key (expand t) &allow-other-keys)
  "Parse a string and (by default) expand the utterance returned."
  (check-type text string)
  (let ((utt (parser::parse-text text)))
    (if expand
        (apply #'expand-utt utt (sans args :expand))
        utt)))

(defun ontp (object)
  "Is OBJECT a symbol in the TRIPS ontology?"
  (and (symbolp object)
       (eq (symbol-package object) (find-package :ont))))

(defun parserp (object)
  "Is OBJECT a symbol in the PARSER package?"
  (and (symbolp object)
       (eq (symbol-package object) (find-package :parser))))

(defun wp (object)
  "Is OBJECT a symbol naming a word in the TRIPS lexicon?"
  (and (symbolp object)
       (eq (symbol-package object) (find-package :w))))

(defstruct (lf-term (:type list)
                    (:constructor make-lf-term (head var type)))
  "Terms in the TRIPS LF have the format (HEAD VAR-NAME TYPE [ROLE VALUE]*)."
  head var type)

(defun roles (term)
  "Return the plist of ROLE VALUE pairs in TERM."
  (member-if #'keywordp term))

(defun role (role term)
  "Return the value of ROLE in TERM."
  (getf (roles term) role))

(defun (setf role) (new-role role term &aux (roles (roles term)))
  "Set the value of ROLE in TERM."
  (setf (getf roles role) new-role))

(set-pprint-dispatch
 '(cons (satisfies ontp) (cons * (cons * (cons keyword *))))
 (defun pprint-lf-term (stream term &aux (*print-circle* t))
   "Pretty-print the logical form TERM."
   (format stream "~:<~w ~w ~w~^ ~_~@{~w~^ ~w~^ ~_~}~:>" term)))

(set-pprint-dispatch
 '(cons (satisfies parserp))
 (defun pprint-parser-term (stream term)
   "Pretty-print a parser TERM or utterance."
   (format stream "~:<~w~^ ~_~@{~w~^ ~w~^ ~_~}~:>" term)))

(defun parser-terms (utt)
  "Given a designator for a list of TRIPS utterances,
return the parser terms and root variable name."
  (etypecase utt
    ((cons cons)
     (parser-terms (car utt)))
    ((cons (eql parser::utt) list)
     (values (getf (cdr utt) :terms)
             (getf (cdr utt) :root)))
    ((cons (eql parser::compound-communication-act) list)
     (parser-terms (getf (cdr utt) :acts)))))

(defun expand-utt (utt &rest args)
  "Return a fully expanded logical form starting from the root of UTT."
  (multiple-value-bind (terms root) (parser-terms utt)
    (apply #'expand-lf (lf-value root terms) terms args)))

(defun lf-var (term)
  "Return the variable bound to the logical form TERM."
  (etypecase term
    ((cons (eql parser::term) list)
     (getf (cdr term) :var))
    ((cons (and symbol (not keyword)) (cons (and symbol (not keyword)) list))
     (cadr term))))

(defun lf-value (var terms)
  "Return the logical form bound to VAR in TERMS."
  (check-type var symbol "a variable name")
  (check-type terms list "a list of terms")
  (let ((term (find var terms :key #'lf-var)))
    (typecase term
      ((cons (eql parser::term) list)
       (getf (cdr term) :lf))
      (t term))))

(defun expand-lf (term terms &key bindings (cycles t))
  "Expand variables referenced in TERM with respect to TERMS.
If CYCLES is true, circular references are allowed to expand."
  (declare (optimize debug))
  (labels ((bind (var value)
             (push (cons var value) bindings)
             value)
           (expand (term)
             (etypecase term
               ((and cons (satisfies aggregate-term-p))
                (values (mapcar #'expand term) bindings))
               ((or null (not symbol))
                (values term bindings))
               ((and symbol (not null))
                (let ((binding (assoc term bindings)))
                  (if binding
                      (if cycles
                          (values (cdr binding) bindings)
                          (values term bindings))
                      (let ((value (lf-value term terms)))
                        (if value
                            (setf (values term bindings)
                                  (expand-lf value terms
                                             :bindings bindings
                                             :cycles cycles))
                            (values term bindings)))))))))
    (etypecase term
      (atom (expand term))
      (cons (let ((roles (roles term)))
              (values (nconc (bind (lf-term-var term)
                                   (ldiff term roles))
                             (loop for (role value) on roles by #'cddr
                                   collect role
                                   collect (case role
                                             (:mods (mapcar #'expand value))
                                             (otherwise (expand value)))))
                      bindings))))))
